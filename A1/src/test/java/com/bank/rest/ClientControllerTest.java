package com.bank.rest;

import com.bank.model.Client;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Spy;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.http.MediaType;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.ui.ModelMap;


import java.util.ArrayList;
import java.util.List;

import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

/**
 * Created by Corina on 11.04.2016.
 */
@RunWith(MockitoJUnitRunner.class)
@WebAppConfiguration
public class ClientControllerTest {
    @Mock
    private ClientService clientService;
    @Mock
    private  AccountService accountService;
    @InjectMocks
    ClientServiceController clientController;
    @Spy
    List<Client> clients = new ArrayList<Client>();
    private MockMvc mockMvc;

    @Spy
    ModelMap model;
    public List<Client> getClientsList(){
        Client c1 = new Client();
        c1.setId(1);
        c1.setName("Client1");
        c1.setCnp("2940607125678");
        c1.setAddress("Cluj");


        Client c2 = new Client();
        c2.setId(2);
        c2.setName("Client2");
        c2.setCnp("2940607125876");
        c2.setAddress("Dej");
        clients.add(c1);
        clients.add(c2);
        return clients;
    }

    @Before
    public void prepare() throws Exception {
        this.mockMvc = MockMvcBuilders.standaloneSetup(clientController).build();
       clients=getClientsList();

    }

    @Test
    public void createClientWrongCNP() throws Exception {
        mockMvc.perform(post("/rest/client").contentType(MediaType.APPLICATION_JSON)
                .content("{\"name\":\"Client wrong\",\"cnp\":\"1660205\",\"address\":\"cluj\"}")).andExpect(status().isBadRequest());

    }

    @Test
    public void findClientsWithException() throws Exception {
        when(clientService.getAll()).thenThrow(new RuntimeException());

        mockMvc.perform(get("/rest/clients")).andExpect(status().isNotFound());

        verify(clientService, times(1)).getAll();
        verifyNoMoreInteractions(clientService);
    }

    @Test
    public void findClientsOk() throws Exception {

        mockMvc.perform(get("/rest/clients")).andExpect(status().isOk());

    }

    @Test
    public void deleteClient() throws Exception{
        mockMvc.perform(delete("/rest/clients/2").contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk());
        verify(clientService).delete(2);


    }





}
