package com.bank.dao;

import com.bank.model.Activity;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Corina on 02.04.2016.
 */
@Repository
@Transactional
public class ActivityDAOImpl extends GenericDAOImpl<Activity,Integer> implements ActivityDAO {

    @Autowired
    private SessionFactory sessionFactory;


    @Transactional(readOnly = true)
    public List<Activity> getAll() {
        List activities = new ArrayList<Activity>();
        Session session = this.sessionFactory.getCurrentSession();
        session.beginTransaction();
        session.clear();
        activities = session.createQuery("from Activity").list();
        return activities;

    }

    public List<Activity> getAllOfUser(int userId) {
        List activities = new ArrayList<Activity>();
        Session session = this.sessionFactory.getCurrentSession();
        session.beginTransaction();
        session.clear();
        activities = session.createQuery("from Activity a where a.user.id = :id")
                .setParameter("id", userId)
                .list();
        return activities;

    }

    @Override
    public List getReportActivities(int userId, String startDate, String endDate) {
        List activities = new ArrayList<Activity>();
        Session session = this.sessionFactory.getCurrentSession();
        session.beginTransaction();
        session.clear();
        activities = session.createQuery("from Activity a where a.date between :startD and :endD and a.user.id = :id ")
                .setParameter("startD",startDate)
                .setParameter("endD",endDate)
                .setParameter("id", userId)
                .list();
        return activities;
    }
}