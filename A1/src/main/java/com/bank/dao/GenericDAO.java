package com.bank.dao;

import java.util.List;

/**
 * Created by Corina on 31.03.2016.
 */
public interface GenericDAO<T,K> {

    public T create(T object);
    public void delete(int id,Class<T> type);
    public T update(T object);
    public T get(int id,Class<T> type);
}
