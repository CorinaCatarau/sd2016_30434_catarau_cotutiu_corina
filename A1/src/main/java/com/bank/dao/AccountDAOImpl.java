package com.bank.dao;

import com.bank.model.Account;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Corina on 01.04.2016.
 */
@Repository
@Transactional
public class AccountDAOImpl extends GenericDAOImpl<Account,Integer> implements AccountDAO{

    @Autowired
    private SessionFactory sessionFactory;


    @Transactional(readOnly = true)
    public List<Account> getAll() {
        List<Account> accounts = new ArrayList<Account>();
        Session session = this.sessionFactory.getCurrentSession();
        session.beginTransaction();
        session.clear();
        accounts = session.createQuery("from Account").list();
        return accounts;

    }

    @Override
    public List<Account> getAllOfClient(int clientId) {
        List<Account> accounts = new ArrayList<Account>();
        Session session = this.sessionFactory.getCurrentSession();
        session.beginTransaction();
        session.clear();
        accounts= session.createQuery(
                "from Account a where a.client.id = :id")
                .setParameter("id", clientId)
                .list();
        return accounts;

    }

    @Override
    public List<Account> getAllBillAccounts(int clientId) {
        List<Account> accounts = new ArrayList<Account>();
        Session session = this.sessionFactory.getCurrentSession();
        session.beginTransaction();
        session.clear();
        accounts= session.createQuery("from Account a where a.client.id = :id and a.billAccount = true")
                .setParameter("id", clientId)
                .list();
        return accounts;

    }


    @Override
    public boolean depositMoneyToAccount(int id, int amount) {
        Account depositAccount=this.get(id,Account.class);
        int depositAmount=depositAccount.getAmount()+amount;
        Session session=this.sessionFactory.getCurrentSession();
        session.beginTransaction();
        session.clear();
        Query query = session.createQuery("update Account set amount = :newAmount where id=:id ")
                .setParameter("newAmount",depositAmount)
                .setParameter("id",id);
        int nr=query.executeUpdate();
        session.getTransaction().commit();
        if(nr>0){
            return true;
        }
        return false;
    }

    @Override
    public boolean withdrawMoneyFromAccount(int id, int amount) {
        Account withdrawAccount=this.get(id,Account.class);
        if((amount>withdrawAccount.getAmount())||(amount<0)){
            return false;
        }else {
            int withdrawAmount = withdrawAccount.getAmount() - amount;
            Session session = this.sessionFactory.getCurrentSession();
            session.beginTransaction();
            session.clear();
            Query query = session.createQuery("update Account set amount = :newAmount"+
                                              " where id= :id ");
                   query.setParameter("newAmount", withdrawAmount);
                   query.setParameter("id", id);
            int nr = query.executeUpdate();
            session.getTransaction().commit();
            if (nr > 0) {
                return true;
            }
            return false;
        }

    }

}
