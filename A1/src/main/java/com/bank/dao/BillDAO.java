package com.bank.dao;

import com.bank.model.Bill;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * Created by Corina on 02.04.2016.
 */
@Component
public interface BillDAO extends GenericDAO<Bill,Integer> {
    public List getAll();
    public  List getAllOfClient(int clientId);


}