package com.bank.dao;

import com.bank.model.Bill;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Corina on 02.04.2016.
 */
@Repository
@Transactional
public class BillDAOImpl extends GenericDAOImpl<Bill,Integer> implements BillDAO {

    @Autowired
    private SessionFactory sessionFactory;


    @Transactional(readOnly = true)
    public List<Bill> getAll() {
        List bills = new ArrayList<Bill>();
        Session session = this.sessionFactory.getCurrentSession();
        session.beginTransaction();
        session.clear();
        bills = session.createQuery("from Bill").list();
        return bills;

    }

    public List<Bill> getAllOfClient(int clientId) {
        List bills = new ArrayList<Bill>();
        Session session = this.sessionFactory.getCurrentSession();
        session.beginTransaction();
        session.clear();
        bills = session.createQuery("from Bill b where b.client.id = :id")
                .setParameter("id", clientId)
                .list();
        return bills;

    }


}