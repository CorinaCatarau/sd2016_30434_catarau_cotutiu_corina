package com.bank.dao;

import com.bank.model.Account;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * Created by Corina on 01.04.2016.
 */
@Component
public interface AccountDAO extends GenericDAO<Account,Integer> {
    public List getAll();
    public  List getAllOfClient(int clientId);
    public boolean depositMoneyToAccount(int id,int amount);
    public boolean withdrawMoneyFromAccount(int id,int amount);
    public List getAllBillAccounts(int clientId);
}
