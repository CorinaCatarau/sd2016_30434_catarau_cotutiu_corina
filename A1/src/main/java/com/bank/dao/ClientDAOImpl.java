package com.bank.dao;

import com.bank.model.Client;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Corina on 01.04.2016.
 */
@Repository
@Transactional
public class ClientDAOImpl extends GenericDAOImpl<Client,Integer> implements ClientDAO{

    @Autowired
    private SessionFactory sessionFactory;


    @Transactional(readOnly = true)
    public List<Client> getAll() {
        List<Client> clients = new ArrayList<Client>();
        Session session = this.sessionFactory.getCurrentSession();
        session.beginTransaction();
        session.clear();
        clients = session.createQuery("from Client").list();
        return clients;

    }

    @Override
    public List<Client> getByCnp(String cnp) {
        List clients = new ArrayList<Client>();
        Session session = this.sessionFactory.getCurrentSession();
        session.beginTransaction();
        session.clear();
        clients = session.createQuery("from Client c where c.cnp = :cnp")
                .setParameter("cnp", cnp)
                .list();
        return clients;
    }


}
