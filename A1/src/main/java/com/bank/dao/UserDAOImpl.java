package com.bank.dao;

import com.bank.model.User;
import com.bank.model.UserRole;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * Created by Corina on 28.03.2016.
 */
@Repository
@Transactional
public class UserDAOImpl extends GenericDAOImpl<User,Integer> implements UserDAO {

    @Autowired
    private SessionFactory sessionFactory;


    @Transactional(readOnly = true)
    public List<User> getAll() {
        List<User> users = new ArrayList<User>();
        Session session = this.sessionFactory.getCurrentSession();
        session.beginTransaction();
        session.clear();
        users = session.createQuery("from User").list();
        return users;

    }

    @Override
    public User findByUserName(String name) {
        List<User> users = new ArrayList<User>();
        Session session = this.sessionFactory.getCurrentSession();
        session.beginTransaction();
        session.clear();
        users=session.createQuery("from User u where u.name = :name")
                    .setParameter("name",name)
                    .list();
        if (users.size() > 0) {
            return users.get(0);
        } else {
            return null;
        }
    }


}
