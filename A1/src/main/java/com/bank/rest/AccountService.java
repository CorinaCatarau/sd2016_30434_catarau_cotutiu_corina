package com.bank.rest;

import com.bank.model.Account;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * Created by Corina on 01.04.2016.
 */
@Component
public interface AccountService {

    public Account create(Account account);
    public void delete(int id);
    public void update(Account account);
    public Account getAccount(int id);
    public List<Account> getAll();
    public List<Account> getAllOfClient(int id);
    public boolean depositMoneyToAccount(int id,int amount);
    public boolean withdrawMoneyFromAccount(int id,int amount);
    public boolean hasBillAccount(int clientId);
}
