package com.bank.rest;

import com.bank.model.Account;
import com.bank.model.Client;
import com.bank.model.User;
import org.hibernate.engine.jdbc.spi.SqlExceptionHelper;
import org.hibernate.exception.ConstraintViolationException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.orm.hibernate4.HibernateJdbcException;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;

import javax.persistence.EntityExistsException;
import java.sql.SQLIntegrityConstraintViolationException;
import java.util.List;

/**
 * Created by Corina on 30.03.2016.
 */
@Controller
@EnableWebMvc
@RequestMapping("/rest")
public class ClientServiceController {

    @Autowired
    private ClientService clientService;
    @Autowired
    private  AccountService accountService;

    @RequestMapping(value = "/client", method = RequestMethod.POST,
            consumes ="application/JSON",
            produces = "application/JSON")
    @ResponseBody
    public ResponseEntity<Client> createClient(@RequestBody Client client)  {
        int id=0;
        try {
            if(clientService.validateCNP(client.getCnp())) {
                if(clientService.getByCnp(client.getCnp()).size()==0) {
                    id = clientService.create(client).getId();
                }else{
                    return  new ResponseEntity<Client>(HttpStatus.CONFLICT);
                }
            }else{
                return new ResponseEntity<Client>(HttpStatus.BAD_REQUEST);
            }

        } catch (Exception e) {
            return new ResponseEntity<Client>(HttpStatus.NOT_FOUND);
        }
        HttpHeaders header = new HttpHeaders();
        header.add("Location", "/rest/clients/" + String.valueOf(id));
        return new ResponseEntity<Client>(client, header, HttpStatus.CREATED);
    }

    @RequestMapping(value = "/clients/{idClient}", method = RequestMethod.GET, produces ="application/JSON")
    @ResponseBody
    public ResponseEntity<Client> getClient(@PathVariable int idClient) {
        try {
            Client client = clientService.getClient(idClient);
            return new ResponseEntity<>(client, HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }

    }

    @RequestMapping(value = "/clients", method = RequestMethod.GET,produces = "application/JSON")
    @ResponseBody
    public  ResponseEntity<List<Client>> getAll(Model model) {
        List<Client> clients;
        try {
            model.addAttribute("client", new Client());
            model.addAttribute("listClients", this.clientService.getAll());
            clients= this.clientService.getAll();
        } catch (Exception e) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity<>(clients, HttpStatus.OK);
    }

    @RequestMapping(value = "/clients/{idClient}", method = RequestMethod.PUT)
    @ResponseBody
    public ResponseEntity<Client> updateClient(@PathVariable("idClient") int idClient,  @RequestBody  Client client) {
        try {
            if((client.getCnp()!=null)&&(client.getName()!=null)) {
                if(clientService.validateCNP(client.getCnp())) {
                    if(clientService.getClient(idClient).getCnp().equals(client.getCnp())) {
                            client.setId(idClient);
                            clientService.update(client);

                    }else  if (clientService.getByCnp(client.getCnp()).size() == 0) {
                        client.setId(idClient);
                        clientService.update(client);
                    }else if (clientService.getByCnp(client.getCnp()).size() != 0) {
                        return new ResponseEntity<Client>(HttpStatus.CONFLICT);
                    }
                }else{
                    return new ResponseEntity<Client>(HttpStatus.NOT_ACCEPTABLE);
                }
            }else{
                return new ResponseEntity<Client>(HttpStatus.BAD_REQUEST);
            }
        } catch (Exception e) {
            return new ResponseEntity<Client>(HttpStatus.BAD_REQUEST);
        }
        return new ResponseEntity<Client>(client, HttpStatus.OK);
    }



    @RequestMapping(value = "/clients/{idClient}", method = RequestMethod.DELETE)
    @ResponseBody
    public ResponseEntity<Client> deleteClient(@PathVariable("idClient") int idClient) {
        try {
            clientService.delete(idClient);
        } catch (Exception e) {
            return new ResponseEntity<Client>(HttpStatus.BAD_REQUEST);
        }
        return new ResponseEntity<Client>(HttpStatus.OK);
    }
}
