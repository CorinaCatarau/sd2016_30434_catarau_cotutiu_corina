package com.bank.rest;

import com.bank.dao.AccountDAO;
import com.bank.model.Account;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by Corina on 01.04.2016.
 */
@Service
public class AccountServiceImpl implements AccountService {

    @Autowired
    private AccountDAO accountDAO;


    @Override
    public Account create(Account account) {
        return accountDAO.create(account);
    }

    @Override
    public void delete(int id) {
        accountDAO.delete(id,Account.class);
    }

    @Override
    public void update(Account account) {accountDAO.update(account);}

    @Override
    public Account getAccount(int id) {
        return accountDAO.get(id,Account.class);
    }

    @Override
    public List<Account> getAll() {
        return this.accountDAO.getAll();
    }

    @Override
    public List<Account> getAllOfClient(int id) {return accountDAO.getAllOfClient(id);}

    @Override
    public boolean depositMoneyToAccount(int id, int amount) {return accountDAO.depositMoneyToAccount(id,amount);}

    @Override
    public boolean withdrawMoneyFromAccount(int id, int amount) {return accountDAO.withdrawMoneyFromAccount(id,amount);}

    @Override
    public boolean hasBillAccount(int clientId) {
        if( accountDAO.getAllBillAccounts(clientId).size()==0){
            return false;
        }
        return  true;
    }


}
