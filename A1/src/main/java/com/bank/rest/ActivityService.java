package com.bank.rest;

import com.bank.model.Activity;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * Created by Corina on 02.04.2016.
 */
@Component
public interface ActivityService {

    public Activity create(Activity activity);
    public void delete(int id);
    public void update(Activity activity);
    public Activity getActivity(int id);
    public List<Activity> getAll();
    public List<Activity> getAllOfUser(int id);
    public List<Activity> getReportActivities(int userId, String startDate, String endDate);


}
