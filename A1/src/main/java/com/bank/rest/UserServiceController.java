package com.bank.rest;

import com.bank.model.Account;
import com.bank.model.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;

import java.util.List;

/**
 * Created by Corina on 30.03.2016.
 */
@Controller
@EnableWebMvc
@RequestMapping("/rest")
public class UserServiceController {

    @Autowired
    private UserService userService;
   
    @RequestMapping(value = "/user", method = RequestMethod.POST,
            consumes ="application/JSON",
            produces = "application/JSON")
    @ResponseBody
    public ResponseEntity<User> createUser(@RequestBody User user) {
        int id=0;
        try {
            if(userService.findByUsername(user.getName())==null) {
                id = userService.create(user).getId();
            }else{
                return new ResponseEntity<User>(HttpStatus.IM_USED);
            }
        } catch (Exception e) {
            return new ResponseEntity<User>(HttpStatus.NOT_FOUND);
        }
        HttpHeaders header = new HttpHeaders();
        header.add("Location", "/rest/users/" + String.valueOf(id));
        return new ResponseEntity<User>(user, header, HttpStatus.CREATED);
    }

    @RequestMapping(value = "/users/{idUser}", method = RequestMethod.GET, produces ="application/JSON")
    @ResponseBody
    public ResponseEntity<User> getUser(@PathVariable int idUser) {
        try {
            User user = userService.getUser(idUser);
            return new ResponseEntity<>(user, HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }

    }

    @RequestMapping(value = "/users", method = RequestMethod.GET,produces = "application/JSON")
    @ResponseBody
    public  ResponseEntity<List<User>> getAll(Model model) {
        List<User> users;
        try {
            model.addAttribute("user", new User());
            model.addAttribute("listUsers", this.userService.getAll());
            users= this.userService.getAll();

        } catch (Exception e) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity<>(users, HttpStatus.OK);
    }

    @RequestMapping(value = "/users/{idUser}", method = RequestMethod.PUT)
    @ResponseBody
    public ResponseEntity<User> updateUser(@PathVariable("idUser") int idUser,  @RequestBody  User user) {
        try {
            if(userService.getUser(idUser).getName().equals(user.getName())){
                user.setActivityList(userService.getUser(idUser).getActivityList());
                user.setId(idUser);
                userService.update(user);
            }else if(userService.findByUsername(user.getName())==null) {
                user.setActivityList(userService.getUser(idUser).getActivityList());
                user.setId(idUser);
                userService.update(user);
            }else{
                return new ResponseEntity<User>(HttpStatus.IM_USED);
            }
        } catch (Exception e) {
            return new ResponseEntity<User>(HttpStatus.BAD_REQUEST);
        }
        return new ResponseEntity<User>(user, HttpStatus.OK);
    }



    @RequestMapping(value = "/users/{idUser}", method = RequestMethod.DELETE)
    @ResponseBody
    public ResponseEntity<User> deleteUser(@PathVariable("idUser") int idUser) {
        try {
            userService.delete(idUser);
        } catch (Exception e) {
            return new ResponseEntity<User>(HttpStatus.BAD_REQUEST);
        }
        return new ResponseEntity<User>(HttpStatus.OK);
    }

    @RequestMapping(value = "/users/username/{username}", method = RequestMethod.GET, produces ="application/JSON")
    @ResponseBody
    public ResponseEntity<User> getUserByUsername(@PathVariable String username) {
        try {
            User user = userService.findByUsername(username);
            return new ResponseEntity<>(user, HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }

    }
}
