package com.bank.rest;

import com.bank.model.Account;
import com.bank.model.Bill;
import com.bank.model.Client;
import com.bank.model.Transaction;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;

import java.util.List;
import java.util.Map;

/**
 * Created by Corina on 02.04.2016.
 */
@Controller
@EnableWebMvc
@RequestMapping("/rest")
public class BillServiceController {

    @Autowired
    private BillService billService;
    @Autowired
    private  ClientService clientService;
    @Autowired
    private  AccountService accountService;

    @RequestMapping(value = "{clientId}/bill", method = RequestMethod.POST,
            consumes ="application/JSON",
            produces = "application/JSON")
    @ResponseBody
    public ResponseEntity<Bill> createBill(@PathVariable int clientId, @RequestBody Bill bill) {
        int id=0;
        Client client=clientService.getClient(clientId);
        if(client!=null) {
            try {
                bill.setClient(client);
                id = billService.create(bill).getId();
                client.getBillList().add(billService.getBill(id));

            } catch (Exception e) {
                return new ResponseEntity<Bill>(HttpStatus.NOT_FOUND);
            }
            HttpHeaders header = new HttpHeaders();
            header.add("Location", "/rest/bills/" + String.valueOf(id));
            return new ResponseEntity<Bill>(bill, header, HttpStatus.CREATED);
        }
        return new ResponseEntity<Bill>(HttpStatus.NOT_FOUND);
    }

    @RequestMapping(value = "/bills/{idBill}", method = RequestMethod.GET, produces ="application/JSON")
    @ResponseBody
    public ResponseEntity<Bill> getBill(@PathVariable int idBill) {
        try {
            Bill bill = billService.getBill(idBill);
            return new ResponseEntity<>(bill, HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }

    }

    @RequestMapping(value = "/bills", method = RequestMethod.GET,produces = "application/JSON")
    @ResponseBody
    public  ResponseEntity<List<Bill>> getAll(Model model) {
        List<Bill> bills;
        try {
            model.addAttribute("bill", new Bill());
            model.addAttribute("listBills", this.billService.getAll());
            bills= this.billService.getAll();
        } catch (Exception e) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity<>(bills, HttpStatus.OK);
    }

    @RequestMapping(value = "/bills/clients/{idClient}", method = RequestMethod.GET,produces = "application/JSON")
    @ResponseBody
    public  ResponseEntity<List<Bill>> getAllOfClient(@PathVariable("idClient") int idClient,Model model) {
        List<Bill> bills;
        try {
            model.addAttribute("bill", new Bill());
            model.addAttribute("listBillsOfClient", this.billService.getAllOfClient(idClient));
            bills= this.billService.getAllOfClient(idClient);
        } catch (Exception e) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity<>(bills, HttpStatus.OK);
    }

    @RequestMapping(value = "/bills/{idBill}", method = RequestMethod.PUT)
    @ResponseBody
    public ResponseEntity<Bill> updateBill(@PathVariable("idBill") int idBill,  @RequestBody  Bill bill) {
        try {
            bill.setId(idBill);
            billService.update(bill);
        } catch (Exception e) {
            return new ResponseEntity<Bill>(HttpStatus.BAD_REQUEST);
        }
        return new ResponseEntity<Bill>(bill, HttpStatus.OK);
    }



    @RequestMapping(value = "/bills/{idBill}", method = RequestMethod.DELETE)
    @ResponseBody
    public ResponseEntity<Bill> deleteBill(@PathVariable("idBill") int idBill) {
        try {
            billService.delete(idBill);
        } catch (Exception e) {
            return new ResponseEntity<Bill>(HttpStatus.BAD_REQUEST);
        }
        return new ResponseEntity<Bill>(HttpStatus.OK);
    }

    @RequestMapping(value = "/bills/pay", method = RequestMethod.GET,produces = "application/JSON")
    @ResponseBody
    public  boolean payBill(@RequestParam Map<String,String> requestParams) {
        List<Account> accounts;
        int idBill=Integer.parseInt(requestParams.get("bill"));
        int idClient=Integer.parseInt(requestParams.get("client"));
        Client client=clientService.getClient(idClient);
        Bill   bill=billService.getBill(idBill);
        int billAccount=0;
        accounts=client.getAccountList();
        for(int i=0;i<accounts.size();i++){
            if(accounts.get(i).isBillAccount()){
                billAccount=accounts.get(i).getId();
            }
        }
        if(billAccount!=0){
            if(!accountService.withdrawMoneyFromAccount(billAccount,bill.getAmount())) {
                bill.setAmount(bill.getAmount() + bill.getAmount());
                billService.update(bill);

            }else {
                return  true;
            }

            return false;
        }
      return false;
    }

}
