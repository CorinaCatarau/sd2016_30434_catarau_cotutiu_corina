package com.bank.rest;

import com.bank.dao.ClientDAO;
import com.bank.dao.UserDAO;
import com.bank.model.Client;
import com.bank.model.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by Corina on 30.03.2016.
 */
@Service
public class ClientServiceImpl implements ClientService {

    @Autowired
    private ClientDAO clientDAO;


    @Override
    public Client create(Client user) {
        return clientDAO.create(user);
    }

    @Override
    public void delete(int id) {
        clientDAO.delete(id,Client.class);
    }

    @Override
    public void update(Client user) {clientDAO.update(user);}

    @Override
    public Client getClient(int id) {
        return clientDAO.get(id,Client.class);
    }

    @Override
    public List<Client> getAll() {
        return this.clientDAO.getAll();
    }

    @Override
    public List<Client> getByCnp(String cnp) {return this.clientDAO.getByCnp(cnp);}

    @Override
    public boolean validateCNP(String cnp) {
        String regex = "^[1-2][0-9][0-9][0-1][0-9][0-3][0-9]{7}";
        if (cnp.matches(regex)) {
            return true;
        }
        return false;
    }
}
