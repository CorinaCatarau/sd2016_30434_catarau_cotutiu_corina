package com.bank.rest;

import com.bank.model.User;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * Created by Corina on 30.03.2016.
 */
@Component
public interface UserService {

    public User create(User user);
    public void delete(int id);
    public void update(User user);
    public User getUser(int id);
    public List<User> getAll();
    public User findByUsername(String username);


}
