package com.bank.rest;

import com.bank.model.Activity;
import com.bank.model.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;

import java.util.List;
import java.util.Map;

/**
 * Created by Corina on 02.04.2016.
 */
@Controller
@EnableWebMvc
@RequestMapping("/rest")
public class ActivityServiceController {

    @Autowired
    private ActivityService activityService;
    @Autowired
    private  UserService userService;

    @RequestMapping(value = "{userId}/activity", method = RequestMethod.POST,
            consumes ="application/JSON",
            produces = "application/JSON")
    @ResponseBody
    public ResponseEntity<Activity> createActivity(@PathVariable int userId, @RequestBody Activity activity) {
        int id=0;
        User user=userService.getUser(userId);
        if(user!=null) {
            try {
                activity.setUser(user);
                id = activityService.create(activity).getId();
                user.getActivityList().add(activityService.getActivity(id));

            } catch (Exception e) {
                return new ResponseEntity<Activity>(HttpStatus.NOT_FOUND);
            }
            HttpHeaders header = new HttpHeaders();
            header.add("Location", "/rest/activities/" + String.valueOf(id));
            return new ResponseEntity<Activity>(activity, header, HttpStatus.CREATED);
        }
        return new ResponseEntity<Activity>(HttpStatus.NOT_FOUND);
    }

    @RequestMapping(value = "/activities/{idActivity}", method = RequestMethod.GET, produces ="application/JSON")
    @ResponseBody
    public ResponseEntity<Activity> getAccount(@PathVariable int idActivity) {
        try {
            Activity activity = activityService.getActivity(idActivity);
            return new ResponseEntity<>(activity, HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }

    }

    @RequestMapping(value = "/activities", method = RequestMethod.GET,produces = "application/JSON")
    @ResponseBody
    public  ResponseEntity<List<Activity>> getAll(Model model) {
        List<Activity> activities;
        try {
            model.addAttribute("activity", new Activity());
            model.addAttribute("listActivities", this.activityService.getAll());
            activities= this.activityService.getAll();
        } catch (Exception e) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity<>(activities, HttpStatus.OK);
    }

    @RequestMapping(value = "/activities/users/{idUser}", method = RequestMethod.GET,produces = "application/JSON")
    @ResponseBody
    public  ResponseEntity<List<Activity>> getAllOfUser(@PathVariable("idUser") int idUser,Model model) {
        List<Activity> activities;
        try {
            model.addAttribute("activity", new Activity());
            model.addAttribute("listActivitiesOfUser", this.activityService.getAllOfUser(idUser));
            activities= this.activityService.getAllOfUser(idUser);
        } catch (Exception e) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity<>(activities, HttpStatus.OK);
    }

    @RequestMapping(value = "/activities/{idActivity}", method = RequestMethod.PUT)
    @ResponseBody
    public ResponseEntity<Activity> updateActivity(@PathVariable("idActivity") int idActivity,  @RequestBody  Activity activity) {
        try {
            activity.setId(idActivity);
            activityService.update(activity);
        } catch (Exception e) {
            return new ResponseEntity<Activity>(HttpStatus.BAD_REQUEST);
        }
        return new ResponseEntity<Activity>(activity, HttpStatus.OK);
    }



    @RequestMapping(value = "/activities/{idActivity}", method = RequestMethod.DELETE)
    @ResponseBody
    public ResponseEntity<Activity> deleteActivity(@PathVariable("idActivity") int idActivity) {
        try {
            activityService.delete(idActivity);
        } catch (Exception e) {
            return new ResponseEntity<Activity>(HttpStatus.BAD_REQUEST);
        }
        return new ResponseEntity<Activity>(HttpStatus.OK);
    }

    @RequestMapping(value = "/activities/users/{idUser}/date", method = RequestMethod.GET,produces = "application/JSON")
    @ResponseBody
    public  ResponseEntity<List<Activity>> getReportActivities(@PathVariable("idUser") int idUser,@RequestParam Map<String,String> requestParams) {
        List<Activity> activities;
        String startDate=requestParams.get("start");
        String endDate=requestParams.get("end");
        try {
            activities= this.activityService.getReportActivities(idUser,startDate,endDate);
        } catch (Exception e) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity<>(activities, HttpStatus.OK);
    }


}
