package com.bank.model;

/**
 * Created by Corina on 02.04.2016.
 */
public class Transaction {

    private int sourceAccountId;
    private int destinationAccountId;
    private int amount;

    public int getSourceAccountId() {return sourceAccountId;}

    public void setSourceAccountId(int sourceAccountId) {this.sourceAccountId = sourceAccountId;}

    public int getDestinationAccountId() {return destinationAccountId;}

    public void setDestinationAccountId(int destinationAccountId) {this.destinationAccountId = destinationAccountId;}

    public int getAmount() {return amount;}

    public void setAmount(int amount) {this.amount = amount;}
}
