package com.bank.model;

import javax.persistence.*;

/**
 * Created by Corina on 01.04.2016.
 */
@Entity
@Table(name="account")
public class Account {
    @Id
    @GeneratedValue
    @Column(name="id")
    private int id;

    @Column(name="name")
    private String name;

    @ManyToOne
    private Client client;

    @Column(name="amount")
    private int amount;

    @Column (name="billAccount")
    private boolean billAccount;

    public int getId() {return id;}

    public void setId(int id) {this.id = id;}

    public String getName() {
        return name;
    }

    public void setName(String name) {this.name = name;}

    public Client getClient() {return client;}

    public void setClient(Client client) {this.client= client;}

    public int getAmount() {
        return amount;
    }

    public void setAmount(int amount) {
        this.amount = amount;
    }

    public boolean isBillAccount() {return billAccount;}

    public void setBillAccount(boolean billAccount) {this.billAccount = billAccount;}
}