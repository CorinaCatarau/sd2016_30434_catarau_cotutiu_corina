package com.bank.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import org.hibernate.annotations.Cascade;
import org.hibernate.annotations.CascadeType;

import javax.persistence.*;
import java.util.List;

/**
 * Created by Corina on 31.03.2016.
 */
@Entity
@Table(name="client")
public class Client {
        @Id
        @GeneratedValue
        @Column(name="id")
        private int id;

        @Column(name="name")
        private String name;

        @Column(name="cnp")
        private String cnp;

        @Column(name="address")
        private String address;


        @OneToMany(fetch = FetchType.EAGER,mappedBy = "client")
        @Cascade(CascadeType.DELETE)
        private List<Account> accountList;

        @OneToMany(fetch = FetchType.EAGER)
        @JoinColumn(name="client_id")
        private List<Bill> billList;

    public int getId() {return id;}

    public void setId(int id) {this.id = id;}

    public String getName() {
        return name;
    }

    public void setName(String name) {this.name = name;}

    public String getCnp() {return cnp;}

    public void setCnp(String cnp) {this.cnp = cnp;}


    public String getAddress() {return address;}

    public void setAddress(String address) {this.address = address;}

    @JsonIgnore
    public List<Account> getAccountList() {return accountList;}
    @JsonIgnore
    public void setAccountList(List<Account> accountList) {this.accountList = accountList;}
    @JsonIgnore
    public List<Bill> getBillList() {return billList;}
    @JsonIgnore
    public void setBillList(List<Bill> billList) {this.billList = billList;}
}
