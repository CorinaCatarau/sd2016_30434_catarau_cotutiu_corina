var myApp = angular.module('app');

myApp .controller('BillsCtrl',['$scope','$http','$mdDialog','$mdMedia','$window', function($scope,$http, $mdDialog, $mdMedia,$window){
    $scope.showBills=false;
    $scope.days=[1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29,30,31];
    $scope.slectedClient="";
    $scope.editBill=false;
    $scope.editBillIndex=-1;
    $scope.onchange = function (client) {
        $scope.selectedClient=client;
        $scope.showBills=true;
        getBills(client.id);
    }

    $scope.setEditModeBill=function(index){
        $scope.editBillIndex=index;
        $scope.editBill=!$scope.editBill;
    }
    $scope.editModeBill=function(index){
        return (($scope.editBillIndex===index)&&$scope.editBill)
    }

    getBills=function(id){
        $http({
            method: 'GET',
            url: '/rest/bills/clients/'+id
        }).then(function successCallback(data) {
            $scope.bills=data.data;

        }, function errorCallback(data) {
            console.log("error");
        });
    }

    $scope.showAlertSuccessBill = function(ev,amount) {
        $mdDialog.show(
            $mdDialog.alert()
                .parent(angular.element(document.querySelector('#popupContainer')))
                .clickOutsideToClose(true)
                .title('Success')
                .textContent('Bill created')
                .ariaLabel('Alert Dialog Demo')
                .ok('Ok')
                .targetEvent(ev)
        );
    };

    $scope.createBill = function(newBill,event,bills) {
        var billNew=angular.fromJson(angular.toJson(newBill));
        $http({
            method: 'POST',
            url: '/rest/'+$scope.selectedClient.id+'/bill',
            headers: {
                'Content-Type': 'application/json'
            },
            data: billNew
        }).then(function successCallback(data) {
            if(data.status===201){
                $scope.showAlertSuccessBill(event);
                bills.push(newBill);
                createActivity("create bill");

            }

            console.log("create succesfull");

        }, function errorCallback(data) {

            console.log("error at client create");
        });
        ;
    };

    $scope.showAlertSuccessBillPayed = function(ev) {
        $mdDialog.show(
            $mdDialog.alert()
                .parent(angular.element(document.querySelector('#popupContainer')))
                .clickOutsideToClose(true)
                .title('Success')
                .textContent('Bill payed')
                .ariaLabel('Alert Dialog Demo')
                .ok('Ok')
                .targetEvent(ev)
        );
    };

    $scope.showAlertErrorBillPayed = function(ev) {
        $mdDialog.show(
            $mdDialog.alert()
                .parent(angular.element(document.querySelector('#popupContainer')))
                .clickOutsideToClose(true)
                .title('Error')
                .textContent('You have no bill account or not enough money')
                .ariaLabel('Alert Dialog Demo')
                .ok('Ok')
                .targetEvent(ev)
        );
    };

    $scope.payBill = function(bill,event) {
        var idBill=bill.id;
        $http({
            method: 'GET',
            url: '/rest/bills/pay?bill='+idBill+'&client='+$scope.selectedClient.id
        }).then(function successCallback(data) {
            if(data.data===true){
                $scope.showAlertSuccessBillPayed(event);
                createActivity("payed bill for: "+$scope.selectedClient.name);

            }else{
                $scope.showAlertErrorBillPayed(event);
            }

        }, function errorCallback(data) {
            console.log("error");
        });
    };


    $scope.deleteBill=function(bill,bills){
        var index = bills.indexOf(bill);
        $http({
            method: 'DELETE',
            url: '/rest/bills/'+bill.id
        }).then(function successCallback() {
            bills.splice(index, 1);
            console.log("Delete succesful");
            createActivity("deleted bill");


        }, function errorCallback() {
            console.log("error at account delete");
        });

    }
    $scope.updateBill=function(bill){
        var nBill=angular.fromJson(angular.toJson(bill));
        $http({
            method: 'PUT',
            url: '/rest/bills/'+bill.id,
            headers: {
                'Content-Type': 'application/json'
            },
            data: nBill
        }).then(function successCallback(data) {
            console.log("Update succesfull");
            $scope.editBill=false;
            createActivity("update bill");


        }, function errorCallback() {
            console.log("error at bill update");
        });

    }

    createActivity = function(name) {
        var allDate=new Date();
        var goodDate=allDate.getFullYear() + '-' + ('0' + (allDate.getMonth() + 1)).slice(-2) + '-' + ('0' + allDate.getDate()).slice(-2);
        var  activity={
            "name":name,
            "date":goodDate
        }
        var newActivity=angular.toJson(activity);
        $http({
            method: 'POST',
            url: '/rest/'+$window.localStorage.getItem("userId")+'/activity',
            headers: {
                'Content-Type': 'application/json'
            },
            data: newActivity
        }).then(function successCallback(data) {
            if(data.status===201){
                console.log("activity create succesfull");
            }


        }, function errorCallback(data) {

            console.log("error at activity create");
        });
        ;
    };




}]);