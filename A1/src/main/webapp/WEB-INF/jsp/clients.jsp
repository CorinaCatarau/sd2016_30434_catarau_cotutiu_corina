<div>
    <h1>Clients:</h1>
    <md-button class="lavender" ng-model="collapsed" ng-click="collapsed=!collapsed">ADD CLIENT</md-button>
    <div ng-show="collapsed">
        <form name="userForm" ng-submit="createClient(newClient,$event)">

            <md-input-container class="md-block">
                <label>Name</label>
                <input  required="" md-no-asterisk="" name="newClientName" ng-model="newClient.name">
                <div ng-messages="userForm.newClientName.$error">
                    <div ng-message="required">This is required.</div>
                </div>
            </md-input-container>

            <md-input-container class="md-block">
                <label>PNC</label>
                <input required="" name="newClientCnp" ng-model="newClient.cnp" ng-pattern="/^[1-2][0-9][0-9][0-1][0-9][0-3][0-9]{7}$/">
                <div ng-messages="userForm.newClientCnp.$error"  ng-show="userForm.newClientCnp.$dirty">
                    <div ng-message="required">This is required.</div>
                    <div ng-message="pattern">Invalid PNC format</div>
                </div>
            </md-input-container>

            <md-input-container class="md-block">
                <label>Address</label>
                <input  name="newClientAddress" ng-model="newClient.address">
            </md-input-container>
            <md-button type="submit" id="submit" class="lavender" ng-disabled="userForm.$invalid">Submit</md-button>

        </form>
    </div>

    <md-content ng-repeat="client in clients">

        <md-list flex="">
            <md-list-item class="lightBlue" class="md-4-line"  ng-click="null" ng-if="editModeClient($index)">
                <img ng-src="" class="md-avatar" alt="">
                <div class="md-list-item-text" layout="column">
                    <md-input-container class="md-block">
                        <label>Name</label>
                        <input  required="" md-no-asterisk="" name="clientName" ng-model="client.name">
                        <div ng-messages="clientName.$error">
                            <div ng-message="required">This is required.</div>
                        </div>
                    </md-input-container>
                    <md-input-container class="md-block">
                        <label>PNC</label>
                        <input  required="" md-no-asterisk="" name="clientCNP" ng-model="client.cnp" >
                        <div ng-messages="clientCNP.$error">
                            <div ng-message="required">This is required.</div>
                        </div>
                    </md-input-container>
                    <md-input-container class="md-block">
                        <label>Address</label>
                        <input  required="" md-no-asterisk="" name="clientAddress" ng-model="client.address">
                        <div ng-messages="clientAddress.$error">
                            <div ng-message="required">This is required.</div>
                        </div>
                    </md-input-container>
                </div>
                <div layout="row" flex>
                     <md-button >
                         <i class="material-icons" ng-click="updateClient(client,$event)"  ng-disabled="clientAddress.$invalid||clientCNP.$invalid||clientName.$invalid">arrow_forward</i>
                    </md-button>

                </div>
            </md-list-item>
            <md-list-item class="lightBlue" class="md-4-line"  ng-click="null" ng-if="!editModeClient($index)">
                <img ng-src="" class="md-avatar" alt="">
                <div class="md-list-item-text" layout="column">
                    <h3>{{ client.name}}</h3>
                    <h4>{{ client.cnp }}</h4>
                    <p>{{ client.address }}</p>
                </div>
                <div layout="row" flex>
                    <md-button >
                        <i class="material-icons"  class="md-secondary md-hue-3" ng-click="setEditMode($index)">mode_edit</i>
                    </md-button>
                    <md-button >
                        <i class="material-icons"  class="md-secondary" ng-click="deleteClient(client,clients)">delete_forever</i>
                    </md-button>

                </div>
            </md-list-item>

          </md-list>
        <md-divider></md-divider>
        <md-content class="md-padding">
            <md-tabs md-selected="selectedIndex" md-border-bottom="" md-autoselect="">
                <md-tab label="NEW ACCOUNT">
                    <form name="accountForm" ng-submit="createAccount(client,newAccount,client.accounts,$event)">

                        <md-input-container class="md-block">
                            <label>Name</label>
                            <input  required="" md-no-asterisk="" name="newAccountName" ng-model="newAccount.name">
                            <div ng-messages="accountForm.newAccountName.$error">
                                <div ng-message="required">This is required.</div>
                            </div>
                        </md-input-container>

                        <md-input-container class="md-block">
                            <label>Amount</label>
                            <input required="" name="newAccountAmount" ng-model="newAccount.amount">
                            <div ng-messages="accountForm.newAccountAmount.$error">
                                <div ng-message="required">This is required.</div>
                            </div>
                        </md-input-container>

                        <md-input-container class="md-block">
                            <md-checkbox ng-model="newAccount.billAccount" name="newAccountBill" aria-label="Checkbox 1">
                                Do you want it to be a bill account ?
                            </md-checkbox>
                            <div ng-messages="accountForm.newAccountBill.$error" role="alert">
                                <div ng-message="required">This is required.</div>
                            </div>
                        </md-input-container>
                        <md-button type="submit" id="submit" class="lavender" ng-disabled="accountForm.$invalid">Submit</md-button>

                    </form>
                    </md-tab>
                </md-tab>
                <md-tab ng-repeat="account in client.accounts" ng-disabled="tab.disabled" label="{{account.name}}">
                    <div class="demo-tab tab{{$index%4}}" style="padding: 25px; text-align: center;">
                        <div >
                            <div layout="row">
                                <md-button class="lavender" ng-click="deleteAccount(account,client.accounts)">DELETE</md-button>
                                <md-button ng-show="editAccount($index)" class="lavender" ng-click="editMode($index)">EDIT </md-button>
                                <md-button ng-hide="editAccount($index)" class="lavender" ng-click="updateAccount(account,$event)">SAVE </md-button>
                            </div>

                            <md-input-container class="md-block">
                                <label>Amount</label>
                                <input ng-model="account.amount" ng-disabled="editAccount($index)" required="">
                            </md-input-container>
                            <md-input-container class="md-block">
                                <label>Bill Account</label>
                                <input ng-model="account.billAccount" ng-disabled="editAccount($index)" required="">
                            </md-input-container>

                        </div>
                        <br>

                    </div>
                </md-tab>
            </md-tabs>
        </md-content>

    </md-content>
</div>

