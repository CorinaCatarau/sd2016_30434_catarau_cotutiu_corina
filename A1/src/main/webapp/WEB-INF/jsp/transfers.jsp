<div ng-app="app" ng-controller="TransferCtrl" ng-cloak="" layout-gt-sm="row" layout="column" class="listdemoBasicUsage" >
    <h1>Transfers</h1>

    <div flex-gt-sm="50" flex="">

        <md-toolbar layout="row" class="md-hue-3">
            <div class="md-toolbar-tools">
                <span>Source Account</span>
                <md-button >
                    <i class="material-icons" ng-click="backSource()" ng-hide="setSource">arrow_forward</i>
                </md-button>
            </div>


        </md-toolbar>

        <md-content ng-if="setSource">
            <md-list flex="">
                <md-list-item class="lightBlue" class="md-4-line"  ng-click="null" ng-repeat="source in clientsTransfer"  ng-click="setSourceClient(source)">
                    <div class="md-list-item-text" layout="column">
                        <h3>{{ source.name}}</h3>
                        <h4>{{ source.cnp }}</h4>
                        <p>{{ source.address }}</p>
                    </div>
                    <md-checkbox class="md-secondary" ng-model="checkedSource" ng-change="setSourceClient(source)"></md-checkbox>
                    <md-divider></md-divider>
                </md-list-item>


            </md-list>

        </md-content>
        <show-accounts ng-if="!setSource"></show-accounts>
    </div>

    <md-divider></md-divider>

    <div flex-gt-sm="50" flex="">

        <md-toolbar layout="row" class="md-hue-3">
            <div class="md-toolbar-tools">
                <span>Destination Account</span>
            </div>
            <md-button >
                <i class="material-icons" ng-click="backDestination()" ng-hide="setDestination">arrow_forward</i>
            </md-button>
        </md-toolbar>

        <md-content ng-if="setDestination">

            <md-list  flex="">
                <md-list-item class="lavender" class="md-4-line"  ng-click="null"  ng-repeat="dest in clientsTransfer" ng-click="setDestinationClient(dest)">
                    <div class="md-list-item-text" layout="column">
                        <h3>{{ dest.name}}</h3>
                        <h4>{{ dest.cnp }}</h4>
                        <p>{{ dest.address }}</p>
                    </div>
                    <md-checkbox class="md-secondary" ng-model="checkedDest" ng-change="setDestinationClient(dest)"></md-checkbox>
                    <md-divider></md-divider>
                </md-list-item>


            </md-list>
        </md-content>
        <dest-accounts ng-if="!setDestination"></dest-accounts>

    </div>

</div>

