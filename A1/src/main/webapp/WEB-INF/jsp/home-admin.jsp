<html ng-app="app">
<head>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- Angular Material style sheet -->
    <link rel="stylesheet" href="http://ajax.googleapis.com/ajax/libs/angular_material/1.0.0/angular-material.min.css">
    <!-- load bootstrap and fontawesome via CDN -->
    <link rel="stylesheet" href="//netdna.bootstrapcdn.com/bootstrap/3.0.0/css/bootstrap.min.css" />
    <link rel="stylesheet" href="//netdna.bootstrapcdn.com/font-awesome/4.0.0/css/font-awesome.css" />
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <link rel="stylesheet" href="../css/custom.css"/>
    <link rel="stylesheet" href="../css/timeline.css"/>


    <!-- SPELLS -->
    <!-- load angular and angular route via CDN -->
    <script src="http://ajax.googleapis.com/ajax/libs/angularjs/1.4.8/angular.min.js"></script>
    <script src="http://ajax.googleapis.com/ajax/libs/angularjs/1.4.8/angular-animate.min.js"></script>
    <script src="http://ajax.googleapis.com/ajax/libs/angularjs/1.4.8/angular-aria.min.js"></script>
    <script src="http://ajax.googleapis.com/ajax/libs/angularjs/1.4.8/angular-messages.min.js"></script>


    <!-- Angular Material Library -->
    <script src="http://ajax.googleapis.com/ajax/libs/angular_material/1.0.0/angular-material.min.js"></script>
    <script type="text/javascript" src="../js/app.js">var myVar = "${username}";</script>
    <script type="text/javascript" src="../js/transferController.js"></script>
    <script type="text/javascript" src="../js/billsController.js"></script>
    <script type="text/javascript" src="../js/admin.js"></script>



</head>

<!-- HEADER AND NAVBAR -->
<body  ng-controller="AdminCtrl">
<toolbar-admin ></toolbar-admin>


<!-- MAIN CONTENT AND INJECTED VIEWS -->
<div id="main"  >
    <admin-users ng-if="usersView"></admin-users>
    <admin-activities ng-if="adminActivitiesView"></admin-activities>


</div>

</body>
</html>