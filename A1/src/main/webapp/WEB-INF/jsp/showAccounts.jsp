<div flex-gt-sm="50" flex="">

    <md-toolbar layout="row" class="md-hue-3">
        <div class="md-toolbar-tools">
            <span>{{selectedSourceClient.name}}</span>
        </div>
    </md-toolbar>

    <md-content>
        <md-list flex="">
            <md-list-item class="lightBlue" class="md-4-line"  ng-click="null" ng-repeat="ac in sAcc"  >
                <div class="md-list-item-text" layout="column">
                    <h3>{{ ac.name}}</h3>
                    <h4>{{ ac.amount }}</h4>
                    <p>{{ ac.billAccount }}</p>
                    <md-input-container ng-if="isCheckedSource" class="md-block">
                        <label>Transfer Amount</label>
                        <input  required="" md-no-asterisk="" name="transferAmount" ng-model="transaction.amount">
                        <div ng-messages="transferAmount.$error">
                            <div ng-message="required">This is required.</div>
                        </div>
                    </md-input-container>
                    <md-button class="lavender" ng-if="isCheckedSource" ng-click="transferOperation()">CONFIRM TRANSFER</md-button>
                </div>
                <md-checkbox class="md-secondary" ng-model="isCheckedSource.checked" ng-change="setSourceAccount(ac,isCheckedSource,$index,sAcc)"></md-checkbox>
                <md-divider></md-divider>
            </md-list-item>


        </md-list>
    </md-content>
</div>
