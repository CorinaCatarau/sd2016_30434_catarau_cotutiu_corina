 <div class="container">
        <div class="page-header">
            <h1 id="timeline">Timeline</h1>
        </div>
        <ul class="timeline">
            <li ng-repeat="item in activities" ng-class-odd="" ng-class-even="'timeline-inverted'">
                <div class="timeline-badge"><i class="material-icons">playlist_add_check</i></div>
                <div class="timeline-panel">
                    <div class="timeline-heading">
                        <h4 class="timeline-title">Activity</h4>
                        <p><small class="text-muted"><i class="glyphicon glyphicon-time"></i>Date:{{item.date}}</small></p>
                    </div>
                    <div class="timeline-body">
                        <p>{{item.name}}</p>
                    </div>
                </div>
            </li>

        </ul>
    </div>
