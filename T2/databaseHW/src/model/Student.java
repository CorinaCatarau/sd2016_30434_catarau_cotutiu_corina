package model;

import java.sql.Date;

/**
 * Created by Corina on 12.03.2016.
 */
public class Student {
    private int student_id;
    private String student_name;
    private String birth_date;
    private  String address;

    public static class StudentBuilder {

        public String student_name;
        public String birth_date;
        public String address;

        public StudentBuilder(String student_name) {
            this.student_name = student_name;
        }


        public StudentBuilder birthdate(String birth_date) {
            this.birth_date = birth_date;
            return this;
        }


        public StudentBuilder address(String address) {
            this.address = address;
            return this;
        }



        public Student build() {
            return new Student(this);
        }




    }

    public Student(StudentBuilder builder){
        this.student_name=builder.student_name;
        this.birth_date=builder.birth_date;
        this.address=builder.address;

    }


    public int getStudent_id() {
        return student_id;
    }

    public void setStudent_id(int student_id) {
        this.student_id = student_id;
    }

    public String getStudent_name() {
        return student_name;
    }

    public void setStudent_name(String student_name) {
        this.student_name = student_name;
    }

    public String getBirth_date() {
        return birth_date;
    }

    public void setBirth_date(String birth_date) {
        this.birth_date = birth_date;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }
}
