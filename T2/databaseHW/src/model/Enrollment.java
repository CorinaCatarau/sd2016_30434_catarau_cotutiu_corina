package model;

/**
 * Created by Corina on 12.03.2016.
 */
public class Enrollment {
    private int stud_id;
    private int course_id;

    public static class EnrollmentBuilder {

        public int stud_id;
        public int course_id;

        public EnrollmentBuilder(int stud_id,int course_id) {
            this.stud_id = stud_id;
            this.course_id=course_id;
        }


        public Enrollment build() {
            return new Enrollment(this);
        }


    }


    public Enrollment(EnrollmentBuilder builder){
       this.stud_id=builder.stud_id;
        this.course_id=builder.course_id;

    }


    public int getStud_id() {
        return stud_id;
    }

    public void setStud_id(int stud_id) {
        this.stud_id = stud_id;
    }

    public int getCourse_id() {
        return course_id;
    }

    public void setCourse_id(int course_id) {
        this.course_id = course_id;
    }
}
