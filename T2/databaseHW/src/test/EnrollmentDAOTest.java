package test;

/**
 * Created by Corina on 13.03.2016.
 */
import dao.CourseDAOImpl;
import dao.EnrollmentDAOImpl;
import dao.StudentDAOImpl;
import model.Course;
import model.Enrollment;
import model.Student;
import org.junit.Test;

import static org.junit.Assert.*;
public class EnrollmentDAOTest {
    EnrollmentDAOImpl enrollmentDAO=new EnrollmentDAOImpl();
    StudentDAOImpl studentDAO=new StudentDAOImpl();
    CourseDAOImpl courseDAO=new CourseDAOImpl();

    @Test
    public void testInsertEnrollment(){
        int nrEnrollments=enrollmentDAO.count();
        assertEquals(0,enrollmentDAO.createEnrollment(7,4));
        assertEquals(nrEnrollments+1,enrollmentDAO.count());
        enrollmentDAO.delete(7,4);
    }

    @Test
    public void TestDeleteEnrollment(){
        int id=enrollmentDAO.createEnrollment(7,4);
        int nrEnrollments=enrollmentDAO.count();
        enrollmentDAO.delete(7,4);
        assertEquals(nrEnrollments-1,enrollmentDAO.count());

    }

    @Test
    public void getAllCoursesOfStudentTest(){
        Course testCourse=new Course(new Course.CourseBuilder("ENRLCourse").teacher("T1")
                .studyYear(1));
        Student testStud=new Student(new Student.StudentBuilder("ENRLStudent").birthdate("1995-03-03")
                .address("EnrlAddr"));
       int idStud= studentDAO.insert(testStud);
       int idCourse= courseDAO.insert(testCourse);
        enrollmentDAO.createEnrollment(idStud,idCourse);
        assertEquals("ENRLCourse",enrollmentDAO.getAllCoursesOfStudent(idStud).get(0).getCourse_name());
        enrollmentDAO.delete(idStud,idCourse);
        studentDAO.delete(idStud);
        courseDAO.delete(idCourse);

    }

    @Test
    public void getAllStudentsOfCourseTest(){
        Course testCourse=new Course(new Course.CourseBuilder("ENRLCourse").teacher("T1")
                .studyYear(1));
        Student testStud=new Student(new Student.StudentBuilder("ENRLStudent").birthdate("1995-03-03")
                .address("EnrlAddr"));
        int idStud= studentDAO.insert(testStud);
        int idCourse= courseDAO.insert(testCourse);
        enrollmentDAO.createEnrollment(idStud,idCourse);
        assertEquals("ENRLStudent",enrollmentDAO.getAllStudentsFromCourse(idCourse).get(0).getStudent_name());
        enrollmentDAO.delete(idStud,idCourse);
        studentDAO.delete(idStud);
        courseDAO.delete(idCourse);
    }




}
