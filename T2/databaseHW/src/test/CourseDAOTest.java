package test;

import dao.CourseDAOImpl;
import model.Course;
import org.junit.Test;

import static org.junit.Assert.*;
/**
 * Created by Corina on 13.03.2016.
 */
public class CourseDAOTest {

    CourseDAOImpl courseDAO=new CourseDAOImpl();

    @Test
    public void testInsertCourse(){
        int nrCourses=courseDAO.count();
        Course testCourse=new Course(new Course.CourseBuilder("TestCourse").teacher("TestTeacher")
                .studyYear(3));
        int id=courseDAO.insert(testCourse);
        assertEquals(testCourse.getCourse_id(),courseDAO.select(id).getCourse_id());
        assertEquals(nrCourses+1,courseDAO.count());
        courseDAO.delete(id);
    }

    @Test
    public void testSelectCourse(){
        Course testCourse=new Course(new Course.CourseBuilder("TestCourse").teacher("TestTeacher")
                .studyYear(3));
        int id=courseDAO.insert(testCourse);
        assertEquals("TestCourse",courseDAO.select(id).getCourse_name());
        courseDAO.delete(id);

    }

    @Test
    public void TestDeleteCourse(){
        Course testCourse=new Course(new Course.CourseBuilder("TestCourse").teacher("TestTeacher")
                .studyYear(3));
        int id=courseDAO.insert(testCourse);
        int nrCourses=courseDAO.count();
        courseDAO.delete(id);
        assertEquals(nrCourses-1,courseDAO.count());
    }

    @Test
    public void TestUpdateCourse(){
        Course testCourse=new Course(new Course.CourseBuilder("TestCourse").teacher("TestTeacher")
                .studyYear(3));
        Course updateTestCourse=new Course(new Course.CourseBuilder("TestCourseUpdated").teacher("TestTeacher")
                .studyYear(4));
        int id=courseDAO.insert(testCourse);
        courseDAO.update(id,updateTestCourse);
        assertEquals("TestCourseUpdated",courseDAO.select(id).getCourse_name());
        assertEquals("TestTeacher",courseDAO.select(id).getTeacher());
        assertEquals(4,courseDAO.select(id).getStudy_year());
        courseDAO.delete(id);

    }



}