package test;

import dao.StudentDAOImpl;
import model.Student;
import org.junit.Test;

import static org.junit.Assert.*;
/**
 * Created by Corina on 13.03.2016.
 */
public class StudentDAOTest {

    StudentDAOImpl studentDAO=new StudentDAOImpl();

    @Test
    public void testInsertStudent(){
        int nrStudents=studentDAO.count();
        Student testStud=new Student(new Student.StudentBuilder("TestStudent3").birthdate("TestDate3")
                                                                                .address("TestCity3"));
        int id=studentDAO.insert(testStud);
        assertEquals(testStud.getStudent_id(),studentDAO.select(id).getStudent_id());
        assertEquals(nrStudents+1,studentDAO.count());
        studentDAO.delete(id);
    }

    @Test
    public void testSelectStudent(){
        Student testStud=new Student(new Student.StudentBuilder("SELECTTEST").birthdate("SELECT")
                .address("SELECT"));
        int id=studentDAO.insert(testStud);
        assertEquals("SELECTTEST",studentDAO.select(id).getStudent_name());
        studentDAO.delete(id);

    }

    @Test
    public void TestDeleteStudent(){
        Student testStud=new Student(new Student.StudentBuilder("DeleteTEST").birthdate("DeleteTEST")
                .address("DeleteTest"));
        int id=studentDAO.insert(testStud);
        int nrStudents=studentDAO.count();
        studentDAO.delete(id);
        assertEquals(null,studentDAO.select(id));
    }

    @Test
    public void TestUpdateStudent(){
        Student testStud=new Student(new Student.StudentBuilder("UpdateTEST1").birthdate("UpdateTEST1")
                .address("UpdateTEST1"));
        Student testStudUpdate=new Student(new Student.StudentBuilder("UpdateTEST2").birthdate("UpdateTEST1")
                .address("UpdateTEST2"));
        int id=studentDAO.insert(testStud);
        studentDAO.update(id,testStudUpdate);
        assertEquals("UpdateTEST2",studentDAO.select(id).getStudent_name());
        assertEquals("UpdateTEST1",studentDAO.select(id).getBirth_date());
        assertEquals("UpdateTEST2",studentDAO.select(id).getAddress());
        studentDAO.delete(id);

    }



}