package dao;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.Statement;

/**
 * Created by Corina on 13.03.2016.
 */
public class DatabaseConnection {
    public Connection myConn;
    public Statement myStat;

    public DatabaseConnection() {
        try{
            //Get connection
            Class.forName("com.mysql.jdbc.Driver");

            this.myConn= DriverManager.getConnection("jdbc:mysql://127.0.0.1:3306/coursedb","root","root");

            //Create statement
            this.myStat=myConn.createStatement();

        }
        catch(Exception e){
            e.printStackTrace();
        }

    }

}
