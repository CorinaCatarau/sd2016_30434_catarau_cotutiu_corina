package dao;

import model.Course;
import model.Enrollment;
import model.Student;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Corina on 12.03.2016.
 */
public class EnrollmentDAOImpl implements EnrollmentDAO{

    private static final String CREATE_QUERY = "INSERT INTO enrollments (stud_id,course_id) VALUES (?,?)";
    private static final String GET_STUDENTS = "SELECT student_name,birth_date,address FROM students join enrollments WHERE student_id = stud_id  and course_id=?";
    private static final String GET_COURSES = "SELECT course_name,teacher,study_year FROM  enrollments join courses WHERE  enrollments.course_id=courses.course_id and stud_id=?";

    private static final String UPDATE_QUERY= "UPDATE courses SET course_name=?,teacher=?,study_year=? WHERE  course_id=?";
    /** The query for delete. */
    private static final String DELETE_ENROLLMENT= "DELETE FROM enrollments WHERE  stud_id=? and course_id=?";
    private static final String DELETE_ALL_ENROLLMENTS= "DELETE FROM enrollments WHERE stud_id=?";
    private static final String COUNT = "SELECT COUNT(*) FROM enrollments";
    private DatabaseConnection dbConn=new DatabaseConnection();

    @Override
    public int createEnrollment(int stud_id, int course_id) {
        int id=-1;
        java.sql.PreparedStatement preparedStatement = null;
        Connection conn=(Connection) dbConn.myConn;
        ResultSet result=null;
        try{
            preparedStatement=conn.prepareStatement(CREATE_QUERY, Statement.RETURN_GENERATED_KEYS);
            preparedStatement.setInt(1,stud_id);
            preparedStatement.setInt(2,course_id);
            preparedStatement.executeUpdate();
            return 0;

        }catch(Exception e){
            e.setStackTrace(null);
        }
        return  -1;

    }


    @Override
    public List<Student> getAllStudentsFromCourse(int course_id) {
        List<Student> students=new ArrayList<Student>();
        java.sql.PreparedStatement preparedStatement = null;
        Connection conn=(Connection) dbConn.myConn;
        ResultSet result=null;
        int index=0;
        try{
            preparedStatement=conn.prepareStatement(GET_STUDENTS);
            preparedStatement.setInt(1,course_id);
            preparedStatement.execute();
            result=preparedStatement.getResultSet();

            while (result.next() && result != null) {
                Student newStud=new Student(new Student.StudentBuilder(result.getString("student_name")).birthdate(result.getString("birth_date"))
                                                                                                        .address(result.getString("address")));
                students.add(index,newStud );
                index++;
            }


        }
        catch(Exception e){
            e.printStackTrace();
        }

        return students ;
    }

    @Override
    public List<Course> getAllCoursesOfStudent(int stud_id) {
        List<Course> courses=new ArrayList<Course>();
        java.sql.PreparedStatement preparedStatement = null;
        Connection conn=(Connection) dbConn.myConn;
        ResultSet result=null;
        int index=0;
        try{
            preparedStatement=conn.prepareStatement(GET_COURSES);
            preparedStatement.setInt(1,stud_id);
            preparedStatement.execute();
            result=preparedStatement.getResultSet();

            while (result.next() && result != null) {
                Course newCourse=new Course(new Course.CourseBuilder(result.getString("course_name")).teacher(result.getString("teacher"))
                        .studyYear(result.getInt("study_year")));
                courses.add(index,newCourse );
                index++;
            }


        }
        catch(Exception e){
            e.printStackTrace();
        }

        return courses ;
    }

    @Override
    public boolean delete(int stud_id, int course_id) {
        java.sql.PreparedStatement preparedStatement = null;
        Connection conn=(Connection) dbConn.myConn;
        ResultSet result=null;
        try{
            preparedStatement = conn.prepareStatement(DELETE_ENROLLMENT);
            preparedStatement.setInt(1,stud_id);
            preparedStatement.setInt(2,course_id);
            preparedStatement.execute();
            return true;
        }
        catch(Exception e){
            e.printStackTrace();
        }


        return false;
    }

    public boolean deleteAllStudentsEnrollments(int stud_id){
        java.sql.PreparedStatement preparedStatement = null;
        Connection conn=(Connection) dbConn.myConn;
        ResultSet result=null;
        try{
            preparedStatement = conn.prepareStatement(DELETE_ALL_ENROLLMENTS);
            preparedStatement.setInt(1,stud_id);
            preparedStatement.execute();
            return true;
        }
        catch(Exception e){
            e.printStackTrace();
        }


        return false;
    }

    public int count() {
        java.sql.PreparedStatement preparedStatement = null;
        Connection conn=(Connection) dbConn.myConn;
        ResultSet result=null;
        try{
            preparedStatement=conn.prepareStatement(COUNT);
            preparedStatement.execute();
            result=preparedStatement.getResultSet();

            if (result.next() && result != null) {
                return  result.getInt(1);

            }



        }
        catch(Exception e){
            e.printStackTrace();
        }



        return -1 ;

    }

    @Override
    public int update(int stud_id, int course_id) {
        return 0;
    }
}
