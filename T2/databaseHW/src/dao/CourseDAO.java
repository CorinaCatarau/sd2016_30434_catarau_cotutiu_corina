package dao;

import model.Course;

import java.util.List;

/**
 * Created by Corina on 12.03.2016.
 */
public interface CourseDAO {

    public int insert(Course course);
    public Course select(int id);
    public boolean delete(int id);
    public int update(int id,Course course);
    public List<Course> findAll();

}
