package dao;

import model.Student;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.List;

/**
 * Created by Corina on 12.03.2016.
 */
public class StudentDAOImpl implements StudentDAO{

    private static final String CREATE_QUERY = "INSERT INTO students (student_name,birth_date,address) VALUES (?,?,?)";
    private static final String READ_QUERY = "SELECT student_name,birth_date,address FROM students WHERE student_id = ?";
    private static final String READ_QUERY_All = "SELECT student_name,birth_date,address FROM students";

    private static final String UPDATE_QUERY= "UPDATE students SET student_name=?,birth_date=?,address=? WHERE  student_id=?";
    /** The query for delete. */
    private static final String DELETE_QUERY = "DELETE FROM students WHERE student_id= ?";
    private static final String COUNT = "SELECT COUNT(*) FROM students";
    private DatabaseConnection dbConn=new DatabaseConnection();

    @Override
    public int insert(Student student) {
        java.sql.PreparedStatement preparedStatement = null;
        Connection conn=(Connection) dbConn.myConn;
        ResultSet result=null;
        int id=-1;
        try{
            preparedStatement=conn.prepareStatement(CREATE_QUERY, Statement.RETURN_GENERATED_KEYS);
            preparedStatement.setString(1,student.getStudent_name());
            preparedStatement.setString(2,student.getBirth_date());
            preparedStatement.setString(3,student.getAddress());

            preparedStatement.executeUpdate();
                ResultSet rs = preparedStatement.getGeneratedKeys();
                rs.next();
                id = rs.getInt(1);



        }catch(Exception e){
            e.setStackTrace(null);
        }
        return id;


    }

    @Override
    public Student select(int id) {
        Student dbStudent=null;
        java.sql.PreparedStatement preparedStatement = null;
        Connection conn=(Connection) dbConn.myConn;
        ResultSet result=null;
        try{
            preparedStatement=conn.prepareStatement(READ_QUERY);
            preparedStatement.setInt(1,id);
            preparedStatement.execute();
            result=preparedStatement.getResultSet();

            if (result.next() && result != null) {

                dbStudent=new Student(new Student.StudentBuilder(result.getString(1))
                        .birthdate(result.getString(2))
                        .address(result.getString(3)));
            } else {
                System.out.println("Nothing found!");
            }



        }
        catch(Exception e){
            e.printStackTrace();
        }



        return dbStudent ;

    }

    @Override
    public boolean delete(int id) {
        java.sql.PreparedStatement preparedStatement = null;
        Connection conn=(Connection) dbConn.myConn;
        ResultSet result=null;
        try{
            preparedStatement = conn.prepareStatement(DELETE_QUERY);
            preparedStatement.setInt(1,id);
            preparedStatement.execute();
            return true;
        }
        catch(Exception e){
            e.printStackTrace();
        }


        return false;

    }

    @Override
    public int update(int id,Student student) {
        Student dbStudent=null;
        Connection conn=(Connection) dbConn.myConn;
        java.sql.PreparedStatement preparedStatement = null;
        ResultSet result=null;
        try{
                preparedStatement=conn.prepareStatement(UPDATE_QUERY);
                preparedStatement.setString(1,student.getStudent_name());
                preparedStatement.setString(2,student.getBirth_date());
                preparedStatement.setString(3,student.getAddress());
                preparedStatement.setInt(4,id);
                preparedStatement.execute();
                result=preparedStatement.getResultSet();



        }
        catch(Exception e){
            e.printStackTrace();
        }


        return id;
    }

    @Override
    public List<Student> findAll() {
        return null;
    }


    public int count() {
        java.sql.PreparedStatement preparedStatement = null;
        Connection conn=(Connection) dbConn.myConn;
        ResultSet result=null;
        try{
            preparedStatement=conn.prepareStatement(COUNT);
            preparedStatement.execute();
            result=preparedStatement.getResultSet();

            if (result.next() && result != null) {
                return  result.getInt(1);

            }



        }
        catch(Exception e){
            e.printStackTrace();
        }



        return -1 ;

    }
}
