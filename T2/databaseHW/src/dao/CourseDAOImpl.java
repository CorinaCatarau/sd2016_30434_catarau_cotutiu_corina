package dao;

import model.Course;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.List;

/**
 * Created by Corina on 12.03.2016.
 */
public class CourseDAOImpl implements CourseDAO {

    private static final String CREATE_QUERY = "INSERT INTO courses (course_name,teacher,study_year) VALUES (?,?,?)";
    private static final String READ_QUERY = "SELECT course_name,teacher,study_year FROM courses WHERE course_id = ?";
    private static final String READ_QUERY_All = "SELECT course_name,teacher,study_year FROM courses";

    private static final String UPDATE_QUERY= "UPDATE courses SET course_name=?,teacher=?,study_year=? WHERE  course_id=?";
    /** The query for delete. */
    private static final String DELETE_QUERY = "DELETE FROM courses WHERE course_id= ?";
    private static final String COUNT = "SELECT COUNT(*) FROM courses";
    private DatabaseConnection dbConn=new DatabaseConnection();


    @Override
    public int insert(Course course) {
        java.sql.PreparedStatement preparedStatement = null;
        Connection conn=(Connection) dbConn.myConn;
        ResultSet result=null;
        int id=-1;
        try{
            preparedStatement=conn.prepareStatement(CREATE_QUERY, Statement.RETURN_GENERATED_KEYS);
            preparedStatement.setString(1,course.getCourse_name());
            preparedStatement.setString(2,course.getTeacher());
            preparedStatement.setInt(3,course.getStudy_year());
            preparedStatement.executeUpdate();
            ResultSet rs = preparedStatement.getGeneratedKeys();
            rs.next();
            id = rs.getInt(1);



        }catch(Exception e){
            e.setStackTrace(null);
        }
        return  id;

    }

    @Override
    public Course select(int id) {
        Course dbCourse=null;
        java.sql.PreparedStatement preparedStatement = null;
        Connection conn=(Connection) dbConn.myConn;
        ResultSet result=null;
        try{
            preparedStatement=conn.prepareStatement(READ_QUERY);
            preparedStatement.setInt(1,id);
            preparedStatement.execute();
            result=preparedStatement.getResultSet();

            if (result.next() && result != null) {

                dbCourse=new Course(new Course.CourseBuilder(result.getString(1))
                        .teacher(result.getString(2))
                        .studyYear(result.getInt(3)));
            } else {
                System.out.println("Nothing found!");
            }



        }
        catch(Exception e){
            e.printStackTrace();
        }



        return dbCourse ;

    }

    @Override
    public boolean delete(int id) {

        java.sql.PreparedStatement preparedStatement = null;
        Connection conn=(Connection) dbConn.myConn;
        ResultSet result=null;
        try{
            preparedStatement = conn.prepareStatement(DELETE_QUERY);
            preparedStatement.setInt(1,id);
            preparedStatement.execute();
            return true;
        }
        catch(Exception e){
            e.printStackTrace();
        }


        return false;

    }

    @Override
    public int update(int id, Course course) {
        Course dbCourse=null;
        Connection conn=(Connection) dbConn.myConn;
        java.sql.PreparedStatement preparedStatement = null;
        ResultSet result=null;
        try{
            preparedStatement=conn.prepareStatement(UPDATE_QUERY);
            preparedStatement.setString(1,course.getCourse_name());
            preparedStatement.setString(2,course.getTeacher());
            preparedStatement.setInt(3,course.getStudy_year());
            preparedStatement.setInt(4,id);
            preparedStatement.execute();
            result=preparedStatement.getResultSet();


        }
        catch(Exception e){
            e.printStackTrace();
        }


        return id;    }

    @Override
    public List<Course> findAll() {
        return null;
    }

    public int count() {
        java.sql.PreparedStatement preparedStatement = null;
        Connection conn=(Connection) dbConn.myConn;
        ResultSet result=null;
        try{
            preparedStatement=conn.prepareStatement(COUNT);
            preparedStatement.execute();
            result=preparedStatement.getResultSet();

            if (result.next() && result != null) {
                return  result.getInt(1);

            }



        }
        catch(Exception e){
            e.printStackTrace();
        }



        return -1 ;

    }
}
