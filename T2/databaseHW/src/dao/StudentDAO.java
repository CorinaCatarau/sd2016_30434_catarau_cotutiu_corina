package dao;

import model.Student;

import java.util.List;

/**
 * Created by Corina on 12.03.2016.
 */
public interface StudentDAO  {

    public int insert(Student student);
    public Student select(int id);
    public boolean delete(int id);
    public int update(int id,Student student);
    public List<Student> findAll();
}
