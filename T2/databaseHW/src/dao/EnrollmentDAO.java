package dao;

import model.Course;
import model.Enrollment;
import model.Student;

import java.util.List;

/**
 * Created by Corina on 12.03.2016.
 */
public interface EnrollmentDAO {

    public int createEnrollment(int stud_id,int course_id);
    public List<Student> getAllStudentsFromCourse(int course_id);
    public List<Course> getAllCoursesOfStudent(int stud_id);
    public boolean delete(int stud_id,int course_id);
    public int update(int stud_id,int course_id);

}
