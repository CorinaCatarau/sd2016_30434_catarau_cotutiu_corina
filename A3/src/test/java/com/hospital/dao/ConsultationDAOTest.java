package com.hospital.dao;

import com.hospital.model.Consultation;
import org.hibernate.validator.constraints.br.CPF;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Spy;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.oxm.jaxb.Jaxb2Marshaller;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.ui.ModelMap;

import javax.xml.transform.stream.StreamSource;
import java.io.FileInputStream;
import java.sql.Connection;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static junit.framework.TestCase.assertSame;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.when;

/**
 * Created by Corina on 19.05.2016.
 */
@RunWith(MockitoJUnitRunner.class)
@WebAppConfiguration
public class ConsultationDAOTest {
    @Mock
    private ConsultationDAO consultationDAO;
    @Mock
    MongoTemplate mongoTemplate;



    @Spy
    List<Consultation> consultations = new ArrayList<Consultation>();
    private MockMvc mockMvc;

    @Spy
    ModelMap model;

    Consultation myConsultation;
    Consultation b1=new Consultation();

    @Before
    public void prepare() throws Exception {
        b1.setId("2");
        b1.setDay(4);
        b1.setDiagnostic("dead");
        b1.setDoctorId("45");
        b1.setPatientId("6");
        b1.setDuration(5);
        b1.setSummary("ok");

        consultations.add(b1);

    }

    @Test
    public void findByName() throws Exception  {
        when(consultationDAO.find(anyString())).thenReturn(b1);
        assertSame("dead",consultationDAO.find("2").getDiagnostic());

    }

    @Test
    public void findAll() throws  Exception{

        when(consultationDAO.findAll()).thenReturn(consultations);
        assertSame("dead",consultationDAO.findAll().get(0).getDiagnostic());
    }




}
