<%--
  Created by IntelliJ IDEA.
  User: Corina
  Date: 28.04.2016
  Time: 14:17
  To change this template use File | Settings | File Templates.
--%>
<html ng-app="app" >
<head>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- Angular Material style sheet -->
    <link rel="stylesheet" href="http://ajax.googleapis.com/ajax/libs/angular_material/1.0.0/angular-material.min.css">
    <!-- load bootstrap and fontawesome via CDN -->
    <link rel="stylesheet" href="//netdna.bootstrapcdn.com/bootstrap/3.0.0/css/bootstrap.min.css" />
    <link rel="stylesheet" href="//netdna.bootstrapcdn.com/font-awesome/4.0.0/css/font-awesome.css" />
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <link rel="stylesheet" href="../css/custom.css"/>


    <!-- SPELLS -->
    <!-- load angular and angular route via CDN -->
    <script src="http://ajax.googleapis.com/ajax/libs/angularjs/1.4.8/angular.min.js"></script>
    <script src="http://ajax.googleapis.com/ajax/libs/angularjs/1.4.8/angular-animate.min.js"></script>
    <script src="http://ajax.googleapis.com/ajax/libs/angularjs/1.4.8/angular-aria.min.js"></script>
    <script src="http://ajax.googleapis.com/ajax/libs/angularjs/1.4.8/angular-messages.min.js"></script>


    <!-- Angular Material Library -->
    <script src="http://ajax.googleapis.com/ajax/libs/angular_material/1.0.0/angular-material.min.js"></script>
    <script src="//cdn.jsdelivr.net/sockjs/1/sockjs.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/lodash.js/4.12.0/lodash.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/stomp.js/2.3.3/stomp.min.js"></script>
    <script type="text/javascript" src="../js/uiCalendar.js"></script>

    <script type="text/javascript" src="../js/app.js">var myVar = "${username}";</script>
    <script type="text/javascript" src="../js/admin.js" ></script>
    <script type="text/javascript" src="../js/chatCtrl.js" ></script>
</head>
<body  ng-controller="AdminCtrl">
<toolbar-admin ></toolbar-admin>


<!-- MAIN CONTENT AND INJECTED VIEWS -->
<div id="main"  >
    <admin-users ng-if="usersView"></admin-users>
    <admin-activities ng-if="adminActivitiesView"></admin-activities>


</div>
</body>
</html>