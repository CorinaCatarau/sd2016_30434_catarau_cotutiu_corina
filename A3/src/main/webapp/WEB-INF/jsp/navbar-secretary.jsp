<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<md-toolbar ng-app="app">
    <div class="md-toolbar-tools">
        <md-button ng-click="switchView('calendar')">
            Hello
        </md-button>
        <md-button aria-label="comment" class="md-icon-button" ng-click="switchView('patients')">
            <i class="material-icons" ng-click="switchView('patients')">face</i>
        </md-button>
        <md-button aria-label="label" class="md-icon-button" ng-click="switchView('consultations')">
            <i class="material-icons" ng-click="switchView('consultations')">compare_arrows</i>
        </md-button>

        <a href="<c:url value="/logout" />">Logout</a>

    </div>
</md-toolbar>
