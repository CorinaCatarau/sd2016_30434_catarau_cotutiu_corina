<div>
    <h1>Users:</h1>
    <md-button class="lavender" ng-model="collapsed" ng-click="collapsed=!collapsed">ADD USER</md-button>
    <div ng-show="collapsed">
        <form name="userFormCreate" ng-submit="createUser(newUser,$event)">

            <md-input-container class="md-block">
                <label>Username</label>
                <input  required="" md-no-asterisk="" name="newUserName" ng-model="newUser.name">
                <div ng-messages="userFormCreate.newUserName.$error">
                    <div ng-message="required">This is required.</div>
                </div>
            </md-input-container>

            <md-input-container class="md-block">
                <label>Password</label>
                <input required="" name="newUserPassword" ng-model="newUser.password" ng-pattern="/^(?=.*[A-Za-z])(?=.*\d)[A-Za-z\d]{6,}$/">
                <div ng-messages="userFormCreate.newUserPassword.$error"  ng-show="userFormCreate.newUserPassword.$dirty">
                    <div ng-message="required">This is required.</div>
                    <div ng-message="pattern">The password must contain at least 6 characters ,1 numeric and 1 letter</div>
                </div>
            </md-input-container>

            <md-input-container>
                <label>Type</label>
                <md-select placeholder="User type" ng-model="newUser.role" >
                    <md-option  >DOCTOR_ROLE</md-option>
                    <md-option  >ADMIN_ROLE</md-option>
                    <md-option  >SECRETARY_ROLE</md-option>
                </md-select>
            </md-input-container>
            <md-button type="submit" id="submit" class="lavender" ng-disabled="userFormCreate.$invalid">Submit</md-button>

        </form>
    </div>

    <md-content ng-repeat="user in users">

        <md-list flex="">
            <md-list-item class="lightBlue" class="md-4-line"  ng-click="null" ng-if="editModeUser($index)">
                <img ng-src="" class="md-avatar" alt="">
                <div class="md-list-item-text" layout="column">
                    <md-input-container class="md-block">
                        <label>Username</label>
                        <input  required="" md-no-asterisk="" name="username" ng-model="user.name">
                        <div ng-messages="username.$error">
                            <div ng-message="required">This is required.</div>
                        </div>
                    </md-input-container>
                    <md-input-container class="md-block">
                        <label>Password</label>
                        <input  required="" md-no-asterisk="" name="password" ng-model="user.password" >
                        <div ng-messages="password.$error">
                            <div ng-message="required">This is required.</div>
                        </div>
                    </md-input-container>
                    <md-input-container>
                        <label>Type</label>
                        <md-select placeholder="user.role" ng-model="user.role" >
                            <md-option  >USER_ROLE</md-option>
                            <md-option  >ADMIN_ROLE</md-option>
                        </md-select>
                    </md-input-container>
                </div>
                <div layout="row" flex>
                    <md-button >
                        <i class="material-icons" ng-click="updateUser(user,$event)"  >arrow_forward</i>
                    </md-button>

                </div>
            </md-list-item>
            <md-list-item class="lightBlue" class="md-4-line"  ng-click="null" ng-if="!editModeUser($index)">
                <img ng-src="" class="md-avatar" alt="">
                <div class="md-list-item-text" layout="column">
                    <h3>{{ user.name}}</h3>
                    <h4>{{ user.password }}</h4>
                    <p>{{ user.role }}</p>
                </div>
                <div layout="row" flex>
                    <md-button >
                        <i class="material-icons"  class="md-secondary md-hue-3" ng-click="setEditModeUser($index)">mode_edit</i>
                    </md-button>
                    <md-button >
                        <i class="material-icons"  class="md-secondary" ng-click="deleteUser(user,users)">delete_forever</i>
                    </md-button>

                </div>
            </md-list-item>

        </md-list>
        <md-divider></md-divider>


    </md-content>
</div>

