<%--
  Created by IntelliJ IDEA.
  User: Corina
  Date: 18.05.2016
  Time: 22:01
  To change this template use File | Settings | File Templates.
--%>
<md-dialog id="createConsDialog" aria-label="Create Consultation" ng-controller="calendarCtrl" flex="50" layout="column">
    <md-dialog-content>
        <h3 class="title-dialog" >Create Consultation</h3>

        <form id="formCreateComp" name="createConsultationForm" role="form" class="forms"  ng-submit="createConsultationForm.$valid && createConsultation()"
              novalidate>

            <md-input-container layout="row" class="md-block">
                <label for="consultationDuration" >Duration</label>
                <input type="text" id="consultationDuration" class="form-control" ng-model="consultation.duration" required="" ng-minlength="1"/>
                <div ng-messages="createConsultationForm.consultationDuration.$error">
                    <div ng-message="required">This is required</div>
                </div>
            </md-input-container>

            <md-input-container layout="row" class="md-block">
                <label for="consultationSummary" >Summary</label>
                <input type="text" id="consultationSummary" class="form-control" ng-model="consultation.summary" required="" ng-minlength="1"/>
                <div ng-messages="createConsultationForm.consultationSummary.$error">
                    <div ng-message="required">This is required</div>
                </div>
            </md-input-container>
            <md-button id="confirmCreateConsultation" class="md-fab-bottom-right md-accent" type="submit" ng-click="closeDialog()"  ng-disabled='createConsultationForm.$invalid' class="md-primary" >
                <h4 >CREATE</h4>
            </md-button>
            <md-button ng-click="closeDialog()" class="md-fab-bottom-right md-accent" translate>
                <h4 >CANCEL</h4>
            </md-button>
        </form>
    </md-dialog-content>


</md-dialog>
