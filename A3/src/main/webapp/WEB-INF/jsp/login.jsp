<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html ng-app="app">
<head>
    <head>
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- Angular Material style sheet -->
        <link rel="stylesheet" href="http://ajax.googleapis.com/ajax/libs/angular_material/1.0.0/angular-material.min.css">
        <!-- load bootstrap and fontawesome via CDN -->
        <link rel="stylesheet" href="//netdna.bootstrapcdn.com/bootstrap/3.0.0/css/bootstrap.min.css" />
        <link rel="stylesheet" href="//netdna.bootstrapcdn.com/font-awesome/4.0.0/css/font-awesome.css" />

        <!-- SPELLS -->
        <!-- load angular and angular route via CDN -->
        <script src="http://ajax.googleapis.com/ajax/libs/angularjs/1.4.8/angular.min.js"></script>
        <script src="http://ajax.googleapis.com/ajax/libs/angularjs/1.4.8/angular-animate.min.js"></script>
        <script src="http://ajax.googleapis.com/ajax/libs/angularjs/1.4.8/angular-aria.min.js"></script>
        <script src="http://ajax.googleapis.com/ajax/libs/angularjs/1.4.8/angular-messages.min.js"></script>

        <!-- Angular Material Library -->
        <script src="http://ajax.googleapis.com/ajax/libs/angular_material/1.0.0/angular-material.min.js"></script>
        <script type="text/javascript" src="js/uiCalendar.js"></script>
        <script type="text/javascript" src="js/app.js"></script>
    </head>
<body >
<center>


    <div  ng-controller="calendarCtrl"  layout-align="center center" layout-fill="layout-fill">
        <div layout="column" class="loginBox md-whiteframe-z2">
            <md-toolbar>
                <h2 class="md-toolbar-tools"><span>Login</span></h2>
            </md-toolbar>

            <form layout="column" class="md-padding" method="post" action="<c:url value='/j_spring_security_check' />">
                <md-input-container>
                    <label>EmailAddress</label>
                    <input type="text" name="j_username" ng-model="username" />
                </md-input-container>
                <md-input-container>
                    <label>Password</label>
                    <input type="password" name="j_password" />
                </md-input-container>

                <div layout="row" layout-align="center center" style="padding-top:20px;">
                    <input type="submit" value="Login" />
                </div>

                <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}"/>
            </form>
        </div>
    </div>
</center>
</body>
</html>