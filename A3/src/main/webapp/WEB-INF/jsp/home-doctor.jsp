<%--
  Created by IntelliJ IDEA.
  User: Corina
  Date: 28.04.2016
  Time: 14:17
  To change this template use File | Settings | File Templates.
--%>
<html ng-app="app" >
<head>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- Angular Material style sheet -->
    <link rel="stylesheet" href="http://ajax.googleapis.com/ajax/libs/angular_material/1.0.0/angular-material.min.css">
    <!-- load bootstrap and fontawesome via CDN -->
    <link rel="stylesheet" href="//netdna.bootstrapcdn.com/bootstrap/3.0.0/css/bootstrap.min.css" />
    <link rel="stylesheet" href="//netdna.bootstrapcdn.com/font-awesome/4.0.0/css/font-awesome.css" />
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <link rel="stylesheet" href="../css/custom.css"/>


    <!-- SPELLS -->
    <!-- load angular and angular route via CDN -->
    <script src="http://ajax.googleapis.com/ajax/libs/angularjs/1.4.8/angular.min.js"></script>
    <script src="http://ajax.googleapis.com/ajax/libs/angularjs/1.4.8/angular-animate.min.js"></script>
    <script src="http://ajax.googleapis.com/ajax/libs/angularjs/1.4.8/angular-aria.min.js"></script>
    <script src="http://ajax.googleapis.com/ajax/libs/angularjs/1.4.8/angular-messages.min.js"></script>


    <!-- Angular Material Library -->
    <script src="http://ajax.googleapis.com/ajax/libs/angular_material/1.0.0/angular-material.min.js"></script>
    <script src="//cdn.jsdelivr.net/sockjs/1/sockjs.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/lodash.js/4.12.0/lodash.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/stomp.js/2.3.3/stomp.min.js"></script>
    <script type="text/javascript" src="../js/uiCalendar.js"></script>

    <script type="text/javascript" src="../js/app.js">var myVar = "${username}";</script>
    <script type="text/javascript" src="../js/admin.js" ></script>
    <script type="text/javascript" src="../js/doctor.js" ></script>
    <script type="text/javascript" src="../js/chatCtrl.js" ></script>
</head>
<body  ng-controller="DoctorController">
<toolbar-doctor ></toolbar-doctor>


<!-- MAIN CONTENT AND INJECTED VIEWS -->
<div id="main"  >
   <div ng-init="getDoctor('${username}')">
       <h1>Consultations</h1>

       <md-input-container>
           <label>Patient</label>
           <md-select placeholder="Patient" ng-model="selectedPatient" >
               <md-option ng-repeat="patient in patients"  value="{{patient.id}}">{{patient.name}}</md-option>
           </md-select>
       </md-input-container>
       <md-button class="md-raised" ng-click="getConsultationsForPatient(selectedPatient)"> SHOW </md-button>
       <md-button class="md-raised" ng-click="resetSearch()"> SHOW ALL</md-button>



       <md-content ng-repeat="consultation in consultations">

           <md-list flex="">
               <md-list-item class="lightBlue" class="md-4-line"  ng-click="null" ng-if="editModeConsultation($index)">
                   <div class="md-list-item-text" layout="column">
                       <md-input-container>
                           <label>Patient id</label>
                           <md-select placeholder="{{consultation.patientId}}" ng-model="consultation.patientId" >
                               <md-option ng-repeat="patient in patients"  value="{{patient.id}}">{{patient.name}}</md-option>
                           </md-select>
                       </md-input-container>
                       <md-input-container class="md-block">
                           <label>Summary</label>
                           <input  required="" md-no-asterisk="" name="summary" ng-model="consultation.summary" >
                           <div ng-messages="summary.$error">
                               <div ng-message="required">This is required.</div>
                           </div>
                       </md-input-container>
                       <md-input-container class="md-block">
                           <label>Diagnostic</label>
                           <input  required="" md-no-asterisk="" name="diagnostic" ng-model="consultation.diagnostic" >
                           <div ng-messages="diagnostic.$error">
                               <div ng-message="required">This is required.</div>
                           </div>
                       </md-input-container>
                   </div>
                   <div layout="row" flex>
                       <md-button >
                           <i class="material-icons" ng-click="updateConsultation(consultation,$event)"  >arrow_forward</i>
                       </md-button>

                   </div>
               </md-list-item>
               <md-list-item class="lightBlue" class="md-4-line"  ng-click="null" ng-if="!editModeConsultation($index)">
                   <div class="md-list-item-text" layout="column">
                       <h3>{{ consultation.patientId}}</h3>
                       <h4>{{ consultation.summary }}</h4>
                       <p>{{ consultation.diagnostic }}</p>
                   </div>
                   <div layout="row" flex>
                       <md-button >
                           <i class="material-icons"  class="md-secondary md-hue-3" ng-click="setEditModeConsultation($index)">mode_edit</i>
                       </md-button>
                       <md-button >
                           <i class="material-icons"  class="md-secondary" ng-click="deleteConsultation(consultation,consultations)">delete_forever</i>
                       </md-button>

                   </div>
               </md-list-item>

           </md-list>
           <md-divider></md-divider>


       </md-content>
   </div>

</div>
</body>
</html>