<div>
    <h1>Patients:</h1>
    <md-button class="lavender" ng-model="collapsed" ng-click="collapsed=!collapsed">ADD PATIENT</md-button>
    <div ng-show="collapsed">
        <form name="patientForm" ng-submit="createPatient(newPatient,$event)">

            <md-input-container class="md-block">
                <label>Name</label>
                <input  required="" md-no-asterisk="" name="newPatientName" ng-model="newPatient.name">
                <div ng-messages="patientForm.newPatientName.$error">
                    <div ng-message="required">This is required.</div>
                </div>
            </md-input-container>

            <md-input-container class="md-block">
                <label>PNC</label>
                <input required="" name="newPatientCnp" ng-model="newPatient.id" ng-pattern="/^[1-2][0-9][0-9][0-1][0-9][0-3][0-9]{7}$/">
                <div ng-messages="patientForm.newPatientCnp.$error"  ng-show="patientForm.newPatientCnp.$dirty">
                    <div ng-message="required">This is required.</div>
                    <div ng-message="pattern">Invalid PNC format</div>
                </div>
            </md-input-container>

            <md-input-container class="md-block">
                <label>Address</label>
                <input  name="newPatientAddress" ng-model="newPatient.address">
            </md-input-container>
            <md-input-container class="md-block">
                <label>Date of birth</label>
                <input  name="newPatientDateOfBirth" ng-model="newPatient.dateOfBirth">
            </md-input-container>
            <md-button type="submit" id="submit" class="lavender" ng-disabled="patientForm.$invalid">Submit</md-button>


        </form>
    </div>

    <md-content ng-repeat="patient in patients">

        <md-list flex="">
            <md-list-item class="lightBlue" class="md-4-line"  ng-click="null" ng-if="editModePatient($index)">
                <div class="md-list-item-text" layout="column">
                    <md-input-container class="md-block">
                        <label>Name</label>
                        <input  required="" md-no-asterisk="" name="patientName" ng-model="patient.name">
                        <div ng-messages="patientName.$error">
                            <div ng-message="required">This is required.</div>
                        </div>
                    </md-input-container>
                    <md-input-container class="md-block">
                        <label>PNC</label>
                        <input  required="" md-no-asterisk="" name="patientCNP" ng-model="patient.id" >
                        <div ng-messages="patientCNP.$error">
                            <div ng-message="required">This is required.</div>
                        </div>
                    </md-input-container>
                    <md-input-container class="md-block">
                        <label>Address</label>
                        <input  required="" md-no-asterisk="" name="patientAddress" ng-model="patient.address">
                        <div ng-messages="clientAddress.$error">
                            <div ng-message="required">This is required.</div>
                        </div>
                    </md-input-container>
                </div>
                <div layout="row" flex>
                    <md-button >
                        <i class="material-icons" ng-click="updatePatient(patient,$event)"  ng-disabled="patientAddress.$invalid||patientCNP.$invalid||patientName.$invalid">arrow_forward</i>
                    </md-button>

                </div>
            </md-list-item>
            <md-list-item class="lightBlue" class="md-4-line"  ng-click="null" ng-if="!editModePatient($index)">
                <img ng-src="" class="md-avatar" alt="">
                <div class="md-list-item-text" layout="column">
                    <h3>{{ patient.name}}</h3>
                    <h4>{{ patient.id }}</h4>
                    <p>{{ patient.address }}</p>
                </div>
                <div layout="row" flex>
                    <md-button >
                        <i class="material-icons"  class="md-secondary md-hue-3" ng-click="setEditModePatient($index)">mode_edit</i>
                    </md-button>
                    <md-button >
                        <i class="material-icons"  class="md-secondary" ng-click="deletePatient(patient,patients)">delete_forever</i>
                    </md-button>

                </div>
            </md-list-item>

        </md-list>
        <md-divider></md-divider>
       <md-content class="md-padding">
            <md-tabs md-selected="selectedIndex" md-border-bottom="" md-autoselect="">
                <md-tab ng-repeat="consultation in patient.consultations" ng-disabled="tab.disabled" label="{{consultation.doctorId}}">
                    <div class="demo-tab tab{{$index%4}}" style="padding: 25px; text-align: center;">
                        <div >
                            <div layout="row">
                                <md-button class="lavender" ng-click="deleteConsultation(consultation,patient.consultations)">DELETE</md-button>
                                <md-button ng-show="!editModeConsultation($index)" class="lavender" ng-click="setEditModeConsultation($index)">EDIT </md-button>
                                <md-button ng-hide="!editModeConsultation($index)" class="lavender" ng-click="updateConsultation(consultation,$event)">SAVE </md-button>
                            </div>

                            <md-input-container class="md-block">
                                <label>Summary</label>
                                <input ng-model="consultation.summary" ng-disabled="!editModeConsultation($index)" required="">
                            </md-input-container>


                        </div>
                        <br>

                    </div>
                </md-tab>
            </md-tabs>
        </md-content>

    </md-content>
</div>

