/**
 * Created by Corina on 19.05.2016.
 */
/**
 * Created by Corina on 15.05.2016.
 */
var myApp = angular.module('app');

myApp.directive('toolbarDoctor', function() {
    return {
        restrict: 'E'
        ,
        replace: true,
        templateUrl: 'navbar-doctor'
    };
});

myApp .controller('DoctorController',['$scope','$http','$mdDialog','$mdMedia','$window', function($scope,$http, $mdDialog, $mdMedia,$window) {
    $scope.username;
    $scope.users=[];

    $scope.editIndex=-1;
    $scope.editConsultation=false;
    $scope.editConsultationIndex=-1;
    $scope.collapsedArea=-1;
    $scope.collapsedItem=-1;
    $scope.myStartDate=new Date();
    $scope.myEndDate=new Date();
    $scope.selectedUser="";
    $scope.showActivities=false;

    $scope.notCollpased=false;
    $scope.list=["A","B","C","E"];

    console.log("${username}");
    console.log("<%=username%>");

    $scope.getDoctor = function(username) {
        $http({
            method: 'GET',
            url: '/rest/users/name/'+username
        }).then(function successCallback(data) {
            $window.localStorage.setItem("doctorId",data.data[0].id);
            getConsultations(data.data[0].id);
            getPatients();
        }, function errorCallback(data) {
            console.log("error");
        });
    };


    getConsultations=function(doctorId){
        $http({
            method: 'GET',
            url: '/rest/consultations/doctor/'+doctorId
        }).then(function successCallback(data) {
            $scope.consultations=data.data;
        }, function errorCallback(data) {
            console.log("error");
        });
    }

    $scope.resetSearch=function(){
        $http({
            method: 'GET',
            url: '/rest/consultations/doctor/'+$window.localStorage.getItem("doctorId")
        }).then(function successCallback(data) {
            $scope.consultations=data.data;
        }, function errorCallback(data) {
            console.log("error");
        });
    }


    $scope.getConsultationsForPatient=function(patientId){

        $http({
            method: 'GET',
            url: '/rest/consultations/doctorPatient?doctor='+$window.localStorage.getItem("doctorId")+'&patient='+patientId
        }).then(function successCallback(data) {
            $scope.consultations.splice(0,$scope.consultations.length);
            $scope.consultations=data.data;

        }, function errorCallback(data) {
            console.log("error");
        });
    }


    $scope.deleteConsultation=function(user,users){
        var index = users.indexOf(user);
        var deleted=user.name;
        $http({
            method: 'DELETE',
            url: '/rest/users/'+user.id
        }).then(function successCallback() {
            users.splice(index, 1);
            console.log("Delete succesful");

        }, function errorCallback() {
            console.log("error at user delete");
        });

    }

    showNotNull = function(ev) {
        $mdDialog.show(
            $mdDialog.alert()
                .parent(angular.element(document.querySelector('#popupContainer')))
                .clickOutsideToClose(true)
                .title('Empty')
                .textContent('Required fields must not be empty.')
                .ariaLabel('Alert Dialog Demo')
                .ok('Ok')
                .targetEvent(ev)
        );
    };

    $scope.updateConsultation=function(consultation,event){

        var nconsultation=angular.fromJson(angular.toJson(consultation));
        $http({
            method: 'PUT',
            url: '/rest/consultations/'+consultation.id,
            headers: {
                'Content-Type': 'application/json'
            },
            data: nconsultation
        }).then(function successCallback(data) {
            if(data.status===226){
                $scope.showAlert(event);
            }else{
                console.log("Update succesfull");
                $scope.editConsultation=false;
            }


        }, function errorCallback(data) {
            if(data.status===400){
                showNotNull(event);
            }
            console.log("error at patient update");
        });

    }


    $scope.setEditModeConsultation=function(index){
        $scope.editConsultationIndex=index;
        $scope.editConsultation=!$scope.editConsultation;
    }

    $scope.editModeConsultation=function(index){
        return (($scope.editConsultationIndex===index)&&$scope.editConsultation)
    }



    getPatients=function(){
        $http({
            method: 'GET',
            url: '/rest/patients'
        }).then(function successCallback(data) {
            $scope.patients=data.data;

        }, function errorCallback(data) {
            console.log("error");
        });
    }

    $scope.onchange = function (user) {
        $scope.selectedUser=user;
        $scope.showActivities=true;
    }
    showAlertUser = function(ev) {
        $mdDialog.show(
            $mdDialog.alert()
                .parent(angular.element(document.querySelector('#popupContainer')))
                .clickOutsideToClose(true)
                .title('Error')
                .textContent('You must first select a user.')
                .ariaLabel('Alert Dialog Demo')
                .ok('Got it!')
                .targetEvent(ev)
        );
    };

    $scope.deleteConsultation=function(consultation,consultations){
        var index = consultations.indexOf(consultation);
        $http({
            method: 'DELETE',
            url: '/rest/consultations/'+consultation.id
        }).then(function successCallback() {
            consultations.splice(index, 1);
            console.log("Delete succesful");

        }, function errorCallback() {
            console.log("error at consultation delete");
        });

    }



}]);
