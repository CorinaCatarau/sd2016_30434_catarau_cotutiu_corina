/**
 * Created by Corina on 15.05.2016.
 */
angular.module('app').service("ChatService", function($q, $timeout) {

    var service = {}, listener = $q.defer(), socket = {
        client: null,
        stomp: null
    }, messageIds = [];

    service.RECONNECT_TIMEOUT = 30000;
    service.SOCKET_URL = "/ws";
    service.CHAT_TOPIC = "/topic/message";
    service.CHAT_BROKER = "/app/ws";

    service.receive = function() {
        return listener.promise;
    };

    service.send = function() {
        var id = Math.floor(Math.random() * 1000000);
        socket.stomp.send(service.CHAT_BROKER, {
            priority: 9
        }, "Hello");
        messageIds.push(id);
    };

    var reconnect = function() {
        $timeout(function() {
            initialize();
        }, this.RECONNECT_TIMEOUT);
    };

    service.getMessage = function(data) {
        var message =data, out = {};
        out.message = data;
        out.time = new Date(message.time);
      /*  if (_.contains(messageIds, message.id)) {
            out.self = true;
            messageIds = _.remove(messageIds, message.id);
        }*/
        return data;
    };

    var startListener = function() {
        socket.stomp.subscribe(service.CHAT_TOPIC, function(data) {
            //listener.notify(getMessage(data.body));
            alert(service.getMessage(data.body));
        });
    };

    var initialize = function() {
        socket.client = new SockJS(service.SOCKET_URL);
        socket.stomp = Stomp.over(socket.client);
        socket.stomp.connect({}, startListener);
        socket.stomp.onclose = reconnect;
    };

    initialize();
    return service;
});


angular.module('app').controller("ChatCtrl", ['$scope','ChatService', function($scope,ChatService){
    $scope.messages = [];
    $scope.message = "";
    $scope.max = 140;

    $scope.addMessage = function() {
        ChatService.send($scope.message);
        $scope.message = "";
    };
    ChatService.receive().then(null, null, function(message) {
        $scope.messages.push("Hello");
        $scope.messages.push(ChatService.getMessage("msg"));


    });
}]);
