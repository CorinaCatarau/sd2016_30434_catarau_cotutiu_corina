/**
 * Created by Corina on 26.04.2016.
 */
var app = angular.module('app', ['ngMaterial','ngMessages','ui.calendar']);
app.directive('printMessage', function () {
    return {
        restrict: 'A',
        template: '<span ng-show="message.priv">[private] </span><strong>{{message.username}}<span ng-show="message.to"> -> {{message.to}}</span>:</strong> {{message.message}}<br/>'

    };
});

app.directive('toolbarSecretary', function() {
    return {
        restrict: 'E'
        ,
        replace: true,
        templateUrl: 'navbar-secretary'
    };
});
/*app.directive('adminActivities', function() {
 return {
 restrict: 'E',
 replace: true,
 templateUrl: 'admin-activities'
 };
 });*/
app.directive('secretaryPatients', function() {
    return {
        restrict: 'E',
        replace: true,
        templateUrl: 'secretary-patients'
    };
});
app.controller('calendarCtrl', ['$scope','$http','$mdDialog','$mdMedia','$window',function($scope,$http, $mdDialog, $mdMedia,$window){
    var date = new Date();
    var d = date.getDate();
    var m = date.getMonth();
    var y = date.getFullYear();

    /* event source that pulls from google.com */
    $scope.eventSource = {

    };
    /* event source that contains custom events on the scope */
    $scope.events = [

        {id: 999,title: 'Repeating Event',start: new Date(y, m, d, 16, 0),allDay: false},

    ];

    $scope.goToRootScopeDate = function(date, jsEvent, view){
        console.log(date._i);
        var ny=date._i[0];
        var nm=date._i[1];
        var nd=date._i[2];
        var nh=date._i[3];
        console.log(ny+" "+y);
        console.log(nm+" "+m);
        console.log(nd+" "+d);
        var event={id: 4,title: 'Consultation for:',start: new Date(ny, nm, nd, nh, 0),end:new Date(ny, nm, nd, nh+1, 0),allDay: false};
        $window.localStorage.setItem("year",ny);
        $window.localStorage.setItem("month",nm);
        $window.localStorage.setItem("day",nd);
        $window.localStorage.setItem("time",nh);

        $scope.showDialogCreateConsultation();
        $scope.events.push(event);
        console.log($scope.events);
    };
    $scope.loading = function(isLoading, view){
    }
    /* config object */
    $scope.uiConfig = {
        calendar:{
            height: 450,
            editable: true,
            header:{
                left: 'title',
                center: '',
                right: 'today prev,next'
            },
            loading: $scope.loading,
            dayClick: $scope.goToRootScopeDate,
            eventClick: $scope.alertOnEventClick,
            eventDrop: $scope.alertOnDrop,
            eventResize: $scope.alertOnResize,
            defaultView: 'agendaWeek'
        }
    };


    /* event sources array*/
    $scope.eventSources = [$scope.events, $scope.eventSource];

//-----------------------------------------------------------------------------CALENDAR--------------------------------------------------------------------------------------------------------
    $scope.username;
    $scope.users=[];
    $scope.doctors=[];
    $scope.calendarView=true;
    $scope.patientsView=false;
    $scope.editIndex=-1;
    $scope.editPatient=false;
    $scope.editPatientIndex=-1;

    $scope.editIndexConsultation=-1;
    $scope.editConsultation=false;
    $scope.editConsultationIndex=-1;

    $scope.collapsedArea=-1;
    $scope.collapsedItem=-1;
    $scope.myStartDate=new Date();
    $scope.myEndDate=new Date();
    $scope.selectedUser="";
    $scope.showActivities=false;

    $scope.notCollpased=false;
    $scope.list=["A","B","C","E"];

    console.log("${username}");
    console.log("<%=username%>");



    $scope.showDialogCreateConsultation = function ($event) {
        var parentEl = angular.element(document.body);
        var confirm = $mdDialog.confirm({
            parent: parentEl,
            targetEvent: $event,
            templateUrl: "create-consultation",
            controller: DialogController
        });
        DialogController.$inject = ['$scope', '$mdDialog'];
        function DialogController($scope,$mdDialog) {

            $scope.closeDialog = function () {
                $mdDialog.cancel();
            };
            $scope.createConsultation = function () {

                newConsultation = {
                "day":$window.localStorage.getItem("day"),
                "month":$window.localStorage.getItem("month"),
                "year":$window.localStorage.getItem("year"),
                "time":$window.localStorage.getItem("time"),
                "duration":this.consultation.duration,
                "doctorId":$window.localStorage.getItem("doctorId"),
                "patientId":$window.localStorage.getItem("patientId"),
                "summary":this.consultation.summary,
                "diagnostic":""
                };
                var finalCons = angular.toJson(newConsultation);
                $http({
                    method: 'POST',
                    url: '/rest/consultation/',
                    headers: {
                        'Content-Type': 'application/json'
                    },
                    data: finalCons
                }).then(function successCallback(data) {
                    if(data.status===201){
                        $scope.showAlertSuccess(event);
                        $scope.users.push(data.data);
                        console.log("create succesfull");
                    }else if(data.status===226){
                        $scope.showAlert(event);
                    }
                    $scope.isCollapsed=true;


                }, function errorCallback(data) {

                    console.log("error at user create");
                });
                ;
            };



        }

        $mdDialog
            .show(confirm)
            .then(function () {
                $scope.companies.push(localStorage.getItem("newCmp"));
            })
            .finally(function () {
                confirm = undefined;
            });


    };


    $scope.showAlert = function(ev) {
        $mdDialog.show(
            $mdDialog.alert()
                .parent(angular.element(document.querySelector('#popupContainer')))
                .clickOutsideToClose(true)
                .title('Forbidden')
                .textContent('Username already used.')
                .ariaLabel('Alert Dialog Demo')
                .ok('Got it!')
                .targetEvent(ev)
        );
    };


    $scope.showAlertSuccess = function(ev) {
        $mdDialog.show(
            $mdDialog.alert()
                .parent(angular.element(document.querySelector('#popupContainer')))
                .clickOutsideToClose(true)
                .title('Success')
                .textContent('Patient has been created')
                .ariaLabel('Alert Dialog Demo')
                .ok('Got it!')
                .targetEvent(ev)
        );
    };




    getPatients=function(){
        $http({
            method: 'GET',
            url: '/rest/patients'
        }).then(function successCallback(data) {
            $scope.patients=data.data;
            for(var i=0;i<$scope.patients.length;i++){
                $scope.getConsultationsForPatient($scope.patients[i].id,$scope.patients[i]);
            }
        }, function errorCallback(data) {
            console.log("error");
        });
    }

    getUsers=function(){
        $http({
            method: 'GET',
            url: '/rest/users'
        }).then(function successCallback(data) {
            $scope.users=data.data;
            for (var i=0;i<$scope.users.length;i++){
                if ($scope.users[i].role === "DOCTOR_ROLE"){
                    $scope.doctors.push($scope.users[i]);
                }
            }
        }, function errorCallback(data) {
            console.log("error");
        });
    }


    $scope.getConsultationsForPatient=function(patientId,patient){

        $http({
            method: 'GET',
            url: '/rest/consultations/patient/'+patientId
        }).then(function successCallback(data) {
                patient.consultations=data.data;


        }, function errorCallback(data) {
            console.log("error");
        });
    }




    $scope.getConsultations=function(doctorId,patientId){
        $window.localStorage.setItem("doctorId",doctorId);
        $window.localStorage.setItem("patientId",patientId);
        $http({
            method: 'GET',
            url: '/rest/consultations/doctor/'+doctorId
        }).then(function successCallback(data) {
            $scope.events.splice(0,$scope.events.length);
            $scope.consultations=data.data;
            for (var i=0;i<$scope.consultations.length;i++){
                var c=$scope.consultations[i];
                if(c.year === 0){
                    c.year=2016;
                    if(c.month === 0){
                        c.month=5;
                    }
                }

                var event={id: c.id,title: 'Consultation with id'+ c.id,start: new Date(c.year, c.month, c.day, c.time, 0),end:new Date(c.year, c.month, c.day, c.time+ c.duration, 0),allDay: false};
                $scope.events.push(event);

            }

        }, function errorCallback(data) {
            console.log("error");
        });
    }



    $scope.deletePatient=function(patient,patients){
        var index = patients.indexOf(patient);
        var deleted=patient.name;
        $http({
            method: 'DELETE',
            url: '/rest/patients/'+patient.id
        }).then(function successCallback() {
            patients.splice(index, 1);
            console.log("Delete succesful");

        }, function errorCallback() {
            console.log("error at patient delete");
        });

    }


    $scope.deleteConsultation=function(consultation,consultations){
        var index = consultations.indexOf(consultation);
        $http({
            method: 'DELETE',
            url: '/rest/consultations/'+consultation.id
        }).then(function successCallback() {
            consultations.splice(index, 1);
            console.log("Delete succesful");

        }, function errorCallback() {
            console.log("error at consultation delete");
        });

    }



    showNotNull = function(ev) {
        $mdDialog.show(
            $mdDialog.alert()
                .parent(angular.element(document.querySelector('#popupContainer')))
                .clickOutsideToClose(true)
                .title('Empty')
                .textContent('Required fields must not be empty.')
                .ariaLabel('Alert Dialog Demo')
                .ok('Ok')
                .targetEvent(ev)
        );
    };

    $scope.updatePatient=function(patient,event){

        var nPatient=angular.fromJson(angular.toJson(patient));
        $http({
            method: 'PUT',
            url: '/rest/patients/'+patient.id,
            headers: {
                'Content-Type': 'application/json'
            },
            data: nPatient
        }).then(function successCallback(data) {
            if(data.status===226){
                $scope.showAlert(event);
            }else{
                console.log("Update succesfull");
                $scope.editPatient=false;
            }


        }, function errorCallback(data) {
            if(data.status===400){
                showNotNull(event);
            }
            console.log("error at patient update");
        });

    }


    $scope.updateConsultation=function(consultation,event){

        var nconsultation=angular.fromJson(angular.toJson(consultation));
        $http({
            method: 'PUT',
            url: '/rest/consultations/'+consultation.id,
            headers: {
                'Content-Type': 'application/json'
            },
            data: nconsultation
        }).then(function successCallback(data) {
            if(data.status===226){
                $scope.showAlert(event);
            }else{
                console.log("Update succesfull");
                $scope.editConsultation=false;
            }


        }, function errorCallback(data) {
            if(data.status===400){
                showNotNull(event);
            }
            console.log("error at patient update");
        });

    }




    $scope.editModePatient=function(index){
        $scope.editIndex=index;
    }

    $scope.setEditModePatient=function(index){
        $scope.editPatientIndex=index;
        $scope.editPatient=!$scope.editPatient;
    }

    $scope.editModePatient=function(index){
        return (($scope.editPatientIndex===index)&&$scope.editPatient)
    }


    $scope.editModeC=function(index){
        $scope.editConsultationIndex=index;
    }

    $scope.setEditModeConsultation=function(index){
        $scope.editConsultationIndex=index;
        $scope.editConsultation=!$scope.editConsultation;
    }

    $scope.editModeConsultation=function(index){
        return (($scope.editConsultationIndex===index)&&$scope.editConsultation)
    }



    $scope.isCollapsed=function(index,i){
        $scope.collapsedArea=i;
        $scope.collapsedItem=index;
        $scope.notCollpased=true;
    }
    $scope.collapsedAreaFunction=function(index,i){
        return((index===$scope.collapsedItem)&&(i===$scope.collapsedArea)&& $scope.notCollpased)
    }


    $scope.switchView=function(view){
        if(view==="calendar"){
            $scope.calendarView=true;
            $scope.patientsView=false;
            getUsers();
            getPatients();
        }else if(view==="patients"){
            $scope.calendarView=false;
            $scope.patientsView=true;
            getPatients();
        }

    }
    $scope.onchange = function (user) {
        $scope.selectedUser=user;
        $scope.showActivities=true;
    }
    showAlertUser = function(ev) {
        $mdDialog.show(
            $mdDialog.alert()
                .parent(angular.element(document.querySelector('#popupContainer')))
                .clickOutsideToClose(true)
                .title('Error')
                .textContent('You must first select a user.')
                .ariaLabel('Alert Dialog Demo')
                .ok('Got it!')
                .targetEvent(ev)
        );
    };

    /*  $scope.getActivities=function(user,sDate,eDate){
     if(user==undefined){
     showAlertUser(event);
     }else {
     var sDateNew = sDate.getFullYear() + '-' + ('0' + (sDate.getMonth() + 1)).slice(-2) + '-' + ('0' + sDate.getDate()).slice(-2);
     var eDateNew = eDate.getFullYear() + '-' + ('0' + (eDate.getMonth() + 1)).slice(-2) + '-' + ('0' + eDate.getDate()).slice(-2);
     $http({
     method: 'GET',
     url: '/rest/activities/users/' + user.id + '/date?start=' + sDateNew + '&end=' + eDateNew
     }).then(function successCallback(data) {
     $scope.activities = data.data;
     }, function errorCallback(data) {
     console.log("error");
     });
     }
     };*/


    $scope.createPatient = function(newPatient,event) {
        var userNew=angular.fromJson(angular.toJson(newPatient));
        $http({
            method: 'POST',
            url: '/rest/patient/',
            headers: {
                'Content-Type': 'application/json'
            },
            data: userNew
        }).then(function successCallback(data) {
            if(data.status===201){
                $scope.showAlertSuccess(event);
                $scope.patients.push(data.data);
                console.log("create succesfull");
            }else if(data.status===226){
                $scope.showAlert(event);
            }
            $scope.isCollapsed=true;


        }, function errorCallback(data) {

            console.log("error at user create");
        });
        ;
    };







}]);
