/**
 * Created by Corina on 15.05.2016.
 */
var myApp = angular.module('app');

myApp.directive('toolbarAdmin', function() {
    return {
        restrict: 'E'
        ,
        replace: true,
        templateUrl: 'navbar-ad'
    };
});
/*app.directive('adminActivities', function() {
    return {
        restrict: 'E',
        replace: true,
        templateUrl: 'admin-activities'
    };
});*/
myApp.directive('adminUsers', function() {
    return {
        restrict: 'E',
        replace: true,
        templateUrl: 'admin-users'
    };
});

myApp .controller('AdminCtrl',['$scope','$http','$mdDialog','$mdMedia','$window', function($scope,$http, $mdDialog, $mdMedia,$window) {
    $scope.username;
    $scope.users=[];
    $scope.userView=true;
    $scope.adminActivitiesView=false;
    $scope.editIndex=-1;
    $scope.editUser=false;
    $scope.editUserIndex=-1;
    $scope.collapsedArea=-1;
    $scope.collapsedItem=-1;
    $scope.myStartDate=new Date();
    $scope.myEndDate=new Date();
    $scope.selectedUser="";
    $scope.showActivities=false;

    $scope.notCollpased=false;
    $scope.list=["A","B","C","E"];

    console.log("${username}");
    console.log("<%=username%>");

    $scope.showAlert = function(ev) {
        $mdDialog.show(
            $mdDialog.alert()
                .parent(angular.element(document.querySelector('#popupContainer')))
                .clickOutsideToClose(true)
                .title('Forbidden')
                .textContent('Username already used.')
                .ariaLabel('Alert Dialog Demo')
                .ok('Got it!')
                .targetEvent(ev)
        );
    };


    $scope.showAlertSuccess = function(ev) {
        $mdDialog.show(
            $mdDialog.alert()
                .parent(angular.element(document.querySelector('#popupContainer')))
                .clickOutsideToClose(true)
                .title('Success')
                .textContent('Your account has been created')
                .ariaLabel('Alert Dialog Demo')
                .ok('Got it!')
                .targetEvent(ev)
        );
    };




    getUsers=function(){
        $http({
            method: 'GET',
            url: '/rest/users'
        }).then(function successCallback(data) {
            $scope.users=data.data;
        }, function errorCallback(data) {
            console.log("error");
        });
    }


    $scope.deleteUser=function(user,users){
        var index = users.indexOf(user);
        var deleted=user.name;
        $http({
            method: 'DELETE',
            url: '/rest/users/'+user.id
        }).then(function successCallback() {
            users.splice(index, 1);
            console.log("Delete succesful");

        }, function errorCallback() {
            console.log("error at user delete");
        });

    }

    showNotNull = function(ev) {
        $mdDialog.show(
            $mdDialog.alert()
                .parent(angular.element(document.querySelector('#popupContainer')))
                .clickOutsideToClose(true)
                .title('Empty')
                .textContent('Required fields must not be empty.')
                .ariaLabel('Alert Dialog Demo')
                .ok('Ok')
                .targetEvent(ev)
        );
    };

    $scope.updateUser=function(user,event){

        var nUser=angular.fromJson(angular.toJson(user));
        delete nUser.activityList;
        $http({
            method: 'PUT',
            url: '/rest/users/'+user.id,
            headers: {
                'Content-Type': 'application/json'
            },
            data: nUser
        }).then(function successCallback(data) {
            if(data.status===226){
                $scope.showAlert(event);
            }else{
                console.log("Update succesfull");
                $scope.editUser=false;
            }


        }, function errorCallback(data) {
            if(data.status===400){
                showNotNull(event);
            }
            console.log("error at user update");
        });

    }
    $scope.editModeUser=function(index){
        $scope.editIndex=index;
    }

    $scope.setEditModeUser=function(index){
        $scope.editUserIndex=index;
        $scope.editUser=!$scope.editUser;
    }

    $scope.editModeUser=function(index){
        return (($scope.editUserIndex===index)&&$scope.editUser)
    }
    $scope.isCollapsed=function(index,i){
        $scope.collapsedArea=i;
        $scope.collapsedItem=index;
        $scope.notCollpased=true;
    }
    $scope.collapsedAreaFunction=function(index,i){
        return((index===$scope.collapsedItem)&&(i===$scope.collapsedArea)&& $scope.notCollpased)
    }


    $scope.switchView=function(view){
        if(view==="admin-users"){
            $scope.usersView=true;
            $scope.adminActivitiesView=false;
            getUsers();
        }else if(view==="admin-activities"){
            $scope.usersView=false;
            $scope.adminActivitiesView=true;
            getUsers();
        }

    }
    $scope.onchange = function (user) {
        $scope.selectedUser=user;
        $scope.showActivities=true;
    }
    showAlertUser = function(ev) {
        $mdDialog.show(
            $mdDialog.alert()
                .parent(angular.element(document.querySelector('#popupContainer')))
                .clickOutsideToClose(true)
                .title('Error')
                .textContent('You must first select a user.')
                .ariaLabel('Alert Dialog Demo')
                .ok('Got it!')
                .targetEvent(ev)
        );
    };

  /*  $scope.getActivities=function(user,sDate,eDate){
        if(user==undefined){
            showAlertUser(event);
        }else {
            var sDateNew = sDate.getFullYear() + '-' + ('0' + (sDate.getMonth() + 1)).slice(-2) + '-' + ('0' + sDate.getDate()).slice(-2);
            var eDateNew = eDate.getFullYear() + '-' + ('0' + (eDate.getMonth() + 1)).slice(-2) + '-' + ('0' + eDate.getDate()).slice(-2);
            $http({
                method: 'GET',
                url: '/rest/activities/users/' + user.id + '/date?start=' + sDateNew + '&end=' + eDateNew
            }).then(function successCallback(data) {
                $scope.activities = data.data;
            }, function errorCallback(data) {
                console.log("error");
            });
        }
    };*/


    $scope.createUser = function(newUser,event) {
        var userNew=angular.fromJson(angular.toJson(newUser));
        $http({
            method: 'POST',
            url: '/rest/user/',
            headers: {
                'Content-Type': 'application/json'
            },
            data: userNew
        }).then(function successCallback(data) {
            if(data.status===201){
                $scope.showAlertSuccess(event);
                $scope.users.push(data.data);
                console.log("create succesfull");
            }else if(data.status===226){
                $scope.showAlert(event);
            }
            $scope.isCollapsed=true;


        }, function errorCallback(data) {

            console.log("error at user create");
        });
        ;
    };


}]);
