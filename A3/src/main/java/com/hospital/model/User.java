package com.hospital.model;

import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.Field;

/**
 * Created by Corina on 04.05.2016.
 */
@Document(collection ="USER")
public class User {
    @Field
    private String id;
    @Field
    private String name;
    @Field
    private String password;
    @Field
    private String role;
    @Field
    private String occupation;

    public String getId() {return id;}
    public void setId(String id) {this.id = id;}
    public String getName() {return name;}
    public void setName(String name) {this.name = name;}
    public String getPassword() {return password;}
    public void setPassword(String password) {this.password = password;}
    public String getRole() {return role;}
    public void setRole(String role) {this.role = role;}
    public String getOccupation() {return occupation;}
    public void setOccupation(String occupation) {this.occupation = occupation;}
}