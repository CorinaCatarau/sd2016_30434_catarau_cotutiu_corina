package com.hospital.model;

import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.Field;

/**
 * Created by Corina on 09.05.2016.
 */
@Document(collection ="PATIENT")
public class Patient {
    @Field
    private String name;
    @Field
    private String id;
    @Field
    private String dateOfBirth;
    @Field
    private String address;

    public String getName() {return name;}

    public void setName(String name) {this.name = name;}

    public String getId() {return id;}

    public void seId(String id) {this.id = id;}

    public String getDateOfBirth() {return dateOfBirth;}

    public void setDateOfBirth(String dateOfBirth) {this.dateOfBirth = dateOfBirth;}

    public String getAddress() {return address;}

    public void setAddress(String address) {this.address = address;}
}

