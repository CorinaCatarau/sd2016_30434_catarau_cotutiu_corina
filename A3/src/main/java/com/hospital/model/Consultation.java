package com.hospital.model;

import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.Field;

/**
 * Created by Corina on 09.05.2016.
 */
@Document(collection ="CONSULTATION")
public class Consultation {
    @Field
    private String id;
    @Field
    private int  day;
    @Field
    private int  month;
    @Field
    private int  year;
    @Field
    private int time;
    @Field
    private  int duration;
    @Field
    private String  doctorId;
    @Field
    private String  patientId;
    @Field
    private String  summary;
    @Field
    private String diagnostic;

    public String getId() {return id;}

    public void setId(String id) {this.id = id;}

    public int getDay() {return day;}

    public void setDay(int day) {this.day = day;}

    public int getMonth() {return month;}

    public void setMonth(int month) {this.month = month;}

    public int getYear() {
        return year;
    }

    public void setYear(int year) {
        this.year = year;
    }

    public int getTime() {return time;}

    public void setTime(int time) {this.time = time;}

    public int getDuration() {return duration;}

    public void setDuration(int duration) {this.duration = duration;}

    public String getDoctorId() {return doctorId;}

    public void setDoctorId(String doctorId) {this.doctorId = doctorId;}

    public String getPatientId() {return patientId;}

    public void setPatientId(String patientId) {this.patientId = patientId;}

    public String getSummary() {return summary;}

    public void setSummary(String summary) {this.summary = summary;}

    public String getDiagnostic() {return diagnostic;}

    public void setDiagnostic(String diagnostic) {this.diagnostic = diagnostic;}
}
