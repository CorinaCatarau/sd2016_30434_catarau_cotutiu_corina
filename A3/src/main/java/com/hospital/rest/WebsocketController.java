package com.hospital.rest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.converter.SimpleMessageConverter;
import org.springframework.messaging.handler.annotation.DestinationVariable;
import org.springframework.messaging.handler.annotation.MessageMapping;
import org.springframework.messaging.handler.annotation.Payload;
import org.springframework.messaging.handler.annotation.SendTo;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.messaging.simp.annotation.SendToUser;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import java.security.Principal;

/**
 * Created by Corina on 14.05.2016.
 */
@Controller
public class WebsocketController {
    @Autowired
    private SimpMessagingTemplate simpMessagingTemplate;


    @MessageMapping("/chat.checker" )
    @SendToUser("/chat.message.filtered")
    public String addNum(@Payload String input) throws Exception {
        Thread.sleep(2000);
        return input;
    }

    @Scheduled(fixedDelay=5000)
    public void sendMessages(Principal principal){
    simpMessagingTemplate.convertAndSendToUser(principal.getName(), "/queue/horray", "Horray, " + principal.getName() + "!");
}
    @RequestMapping("/start")
    public String start() {
        return "start";
    }


    @MessageMapping("/greeting")
    public void filterPrivateMessage( Principal principal) {


        simpMessagingTemplate.convertAndSendToUser("Corina", "/reply", "Hellooo");
        simpMessagingTemplate.convertAndSendToUser(principal.getName(), "/queue/horray", "Horray, " + principal.getName() + "!");

    }

    @MessageMapping("/ws")
    @SendTo("/topic/message")
    public String sendMessage(String message) {
        return message;
    }

}
