package com.hospital.rest;

import com.hospital.dao.ConsultationDAO;
import com.hospital.model.Consultation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;

import java.util.List;
import java.util.Map;

/**
 * Created by Corina on 09.05.2016.
 */
@Controller
@EnableWebMvc
@RequestMapping("/rest")
public class ConsultationController {

    @Autowired
    private ConsultationDAO consultationDAO;

    @RequestMapping(value = "/consultation", method = RequestMethod.POST,
            consumes ="application/JSON",
            produces = "application/JSON")
    @ResponseBody
    public ResponseEntity<Consultation> createConsultation(@RequestBody Consultation consultation) {
        try {
            this.consultationDAO.create(consultation);
        }catch (Exception e) {
            return new ResponseEntity<Consultation>(HttpStatus.NOT_FOUND);
        }
        HttpHeaders header = new HttpHeaders();
        header.add("Location", "/rest/consultations/" + String.valueOf(consultation.getId()));
        return new ResponseEntity<Consultation>(consultation, header, HttpStatus.CREATED);
    }

    @RequestMapping(value = "/consultations/{idConsultation}", method = RequestMethod.PUT)
    @ResponseBody
    public ResponseEntity<Consultation> updateConsultation(@PathVariable("idConsultation") String idConsultation, @RequestBody  Consultation consultation) {
        try {
            consultation.setId(idConsultation);
            consultationDAO.update(consultation);

        } catch (Exception e) {
            return new ResponseEntity<Consultation>(HttpStatus.BAD_REQUEST);
        }
        return new ResponseEntity<Consultation>(consultation, HttpStatus.OK);
    }
    @RequestMapping(value = "/consultations", method = RequestMethod.GET,produces = "application/JSON")
    @ResponseBody
    public  ResponseEntity<List<Consultation>> getAll(Model model) {
        List<Consultation> consultations;
        try {
            consultations=this.consultationDAO.findAll();

        } catch (Exception e) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity<>(consultations, HttpStatus.OK);
    }

    @RequestMapping(value = "/consultations/{idConsultation}",method = RequestMethod.GET,produces = "application/JSON")
    @ResponseBody
    public ResponseEntity<Consultation> getConsultationById(@PathVariable String idConsultation ){
        try {
            Consultation consultation = consultationDAO.find(idConsultation);
            return new ResponseEntity<>(consultation, HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    @RequestMapping(value = "/consultations/patient/{patientId}",method = RequestMethod.GET,produces = "application/JSON")
    @ResponseBody
    public ResponseEntity<List<Consultation>> getConsultationsOfPatient(@PathVariable String patientId ){
        try {
            List <Consultation> consultations = consultationDAO.findAllOfPatient(patientId);
            return new ResponseEntity<>(consultations, HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    @RequestMapping(value = "/consultations/doctor/{doctorId}",method = RequestMethod.GET,produces = "application/JSON")
    @ResponseBody
    public ResponseEntity<List<Consultation>> getConsultationsOfDoctor(@PathVariable String doctorId ){
        try {
            List <Consultation> consultations = consultationDAO.findAllOfDoctor(doctorId);
            return new ResponseEntity<>(consultations, HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }


    @RequestMapping(value = "/consultations/doctorPatient",method = RequestMethod.GET,produces = "application/JSON")
    @ResponseBody
    public ResponseEntity<List<Consultation>> getConsultationsOfDoctorPatient(@RequestParam Map<String,String> requestParams ){
        try {
            String idDoctor=requestParams.get("doctor");
            String idPatient=requestParams.get("patient");


            List <Consultation> consultations = consultationDAO.findAllOfDoctorPatient(idDoctor,idPatient);
            return new ResponseEntity<>(consultations, HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    @RequestMapping(value = "/consultations/{idConsultation}",method = RequestMethod.DELETE)
    @ResponseBody
    public ResponseEntity<Consultation> deleteConsultation(@PathVariable String idConsultation){
        try {
            consultationDAO.delete(idConsultation,Consultation.class);
            return new ResponseEntity<>( HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }
}

