package com.hospital.rest;

import com.hospital.dao.UserDAO;
import com.hospital.model.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Corina on 08.05.2016.
 */
@Controller
@EnableWebMvc
@RequestMapping("/rest")
public class UserController {

    @Autowired
    private UserDAO userDAO;

    @RequestMapping(value = "/user", method = RequestMethod.POST,
            consumes ="application/JSON",
            produces = "application/JSON")
    @ResponseBody
    public ResponseEntity<User> createUser(@RequestBody User user) {
        try {
          this.userDAO.create(user);
        }catch (Exception e) {
            return new ResponseEntity<User>(HttpStatus.NOT_FOUND);
        }
        HttpHeaders header = new HttpHeaders();
        header.add("Location", "/rest/users/" + String.valueOf(user.getId()));
        return new ResponseEntity<User>(user, header, HttpStatus.CREATED);
    }

    @RequestMapping(value = "/users/{idUser}", method = RequestMethod.PUT)
    @ResponseBody
    public ResponseEntity<User> updateUser(@PathVariable("idUser") String idUser, @RequestBody  User user) {
        try {
            user.setId(idUser);
            userDAO.update(user);

        } catch (Exception e) {
            return new ResponseEntity<User>(HttpStatus.BAD_REQUEST);
        }
        return new ResponseEntity<User>(user, HttpStatus.OK);
    }
    @RequestMapping(value = "/users", method = RequestMethod.GET,produces = "application/JSON")
    @ResponseBody
    public  ResponseEntity<List<User>> getAll(Model model) {
        List<User> users;
        try {
                users=this.userDAO.findAll();

        } catch (Exception e) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity<>(users, HttpStatus.OK);
    }

    @RequestMapping(value = "/users/{idUser}",method = RequestMethod.GET,produces = "application/JSON")
    @ResponseBody
    public ResponseEntity<User> getUserById(@PathVariable String idUser ){
        try {
            User user = userDAO.find(idUser);
            return new ResponseEntity<>(user, HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    @RequestMapping(value = "/users/name/{nameUser}",method = RequestMethod.GET,produces = "application/JSON")
    @ResponseBody
    public ResponseEntity<List<User>> getUserByName(@PathVariable String nameUser ){
        try {
           List <User> users = userDAO.findByName(nameUser);
            return new ResponseEntity<>(users, HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    @RequestMapping(value = "/users/{idUser}",method = RequestMethod.DELETE)
    @ResponseBody
    public ResponseEntity<User> deleteUser(@PathVariable String idUser){
        try {
            userDAO.delete(idUser,User.class);
            return new ResponseEntity<>( HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }
}
