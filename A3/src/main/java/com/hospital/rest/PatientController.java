package com.hospital.rest;

import com.hospital.dao.PatientDAO;
import com.hospital.model.Patient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Corina on 08.05.2016.
 */
@Controller
@EnableWebMvc
@RequestMapping("/rest")
public class PatientController {

    @Autowired
    private PatientDAO patientDAO;

    @RequestMapping(value = "/patient", method = RequestMethod.POST,
            consumes ="application/JSON",
            produces = "application/JSON")
    @ResponseBody
    public ResponseEntity<Patient> createPatient(@RequestBody Patient patient) {
        try {
            this.patientDAO.create(patient);
        }catch (Exception e) {
            return new ResponseEntity<Patient>(HttpStatus.NOT_FOUND);
        }
        HttpHeaders header = new HttpHeaders();
        header.add("Location", "/rest/patients/" + String.valueOf(patient.getId()));
        return new ResponseEntity<Patient>(patient, header, HttpStatus.CREATED);
    }

    @RequestMapping(value = "/patients/{idPatient}", method = RequestMethod.PUT)
    @ResponseBody
    public ResponseEntity<Patient> updateUser(@PathVariable("idPatient") String idPatient, @RequestBody  Patient patient) {
        try {
            patient.seId(idPatient);
            patientDAO.update(patient);

        } catch (Exception e) {
            return new ResponseEntity<Patient>(HttpStatus.BAD_REQUEST);
        }
        return new ResponseEntity<Patient>(patient, HttpStatus.OK);
    }
    @RequestMapping(value = "/patients", method = RequestMethod.GET,produces = "application/JSON")
    @ResponseBody
    public  ResponseEntity<List<Patient>> getAll(Model model) {
        List<Patient> patients;
        try {
            patients=this.patientDAO.findAll();

        } catch (Exception e) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity<>(patients, HttpStatus.OK);
    }

    @RequestMapping(value = "/patients/{idPatient}",method = RequestMethod.GET,produces = "application/JSON")
    @ResponseBody
    public ResponseEntity<Patient> getPatientById(@PathVariable String idPatient ){
        try {
            Patient patient = patientDAO.find(idPatient);
            return new ResponseEntity<>(patient, HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    @RequestMapping(value = "/patients/name/{namePatient}",method = RequestMethod.GET,produces = "application/JSON")
    @ResponseBody
    public ResponseEntity<List<Patient>> getPatientByName(@PathVariable String namePatient ){
        try {
            List <Patient> patients = patientDAO.findByName(namePatient);
            return new ResponseEntity<>(patients, HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    @RequestMapping(value = "/patients/{idPatient}",method = RequestMethod.DELETE)
    @ResponseBody
    public ResponseEntity<Patient> deletePatient(@PathVariable String idPatient){
        try {
            patientDAO.delete(idPatient,Patient.class);
            return new ResponseEntity<>( HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }
}
