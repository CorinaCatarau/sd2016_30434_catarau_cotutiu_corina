package com.hospital.dao;

import com.hospital.model.Consultation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Corina on 09.05.2016.
 */
@Repository
public class ConsultationDAOImpl extends MongoGenericDAO<Consultation,String> implements ConsultationDAO {
    @Autowired
    MongoTemplate mongoTemplate;

    @Override
    public Consultation find(String id) {
        Query query = new Query();
        query.addCriteria(Criteria.where("id").is(id));
        Consultation consultation=null;
        List<Consultation> consultationList = mongoTemplate.find(query, Consultation.class);
        if (consultationList != null && !consultationList.isEmpty()) {
            consultation = consultationList.get(0);

        }
        return  consultation;
    }


    @Override
    public List<Consultation> findAll() {
        Query query = new Query();
        List<Consultation> consultations = mongoTemplate.find(query,Consultation.class);
        if(consultations == null){
            consultations = new ArrayList<Consultation>();
        }
        return consultations;
    }

    @Override
    public List<Consultation> findAllOfPatient(String patientId) {
        Query query = new Query();
        query.addCriteria(Criteria.where("patientId").is(patientId));
        Consultation consultation=null;
        List<Consultation> consultationList = mongoTemplate.find(query, Consultation.class);
        return  consultationList;
    }

    @Override
    public List<Consultation> findAllOfDoctor(String doctorId) {
        Query query = new Query();
        query.addCriteria(Criteria.where("doctorId").is(doctorId));
        Consultation consultation=null;
        List<Consultation> consultationList = mongoTemplate.find(query, Consultation.class);
        return  consultationList;
    }

    @Override
    public List<Consultation> findAllOfDoctorPatient(String doctorId, String patientId) {
        Query query = new Query();
        query.addCriteria(Criteria.where("doctorId").is(doctorId));
        query.addCriteria(Criteria.where("patientId").is(patientId));
        Consultation consultation=null;
        List<Consultation> consultationList = mongoTemplate.find(query, Consultation.class);
        return  consultationList;
    }
}
