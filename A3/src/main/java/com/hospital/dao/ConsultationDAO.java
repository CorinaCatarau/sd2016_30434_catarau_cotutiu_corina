package com.hospital.dao;

import com.hospital.model.Consultation;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * Created by Corina on 09.05.2016.
 */
@Component
public interface ConsultationDAO extends GenericDAO<Consultation,String> {

    Consultation find(String id);
    List<Consultation> findAll();
    List<Consultation> findAllOfPatient(String patientId);
    List<Consultation> findAllOfDoctor(String doctorId);
    List<Consultation> findAllOfDoctorPatient(String doctorId,String patientId);

}
