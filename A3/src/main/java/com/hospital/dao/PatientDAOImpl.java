package com.hospital.dao;

import com.hospital.model.Patient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Corina on 09.05.2016.
 */
@Repository
public class PatientDAOImpl extends MongoGenericDAO<Patient,String> implements PatientDAO {
    @Autowired
    MongoTemplate mongoTemplate;

    @Override
    public Patient find(String id) {
        Query query = new Query();
        query.addCriteria(Criteria.where("id").is(id));
        Patient patient=null;
        List<Patient> patientList = mongoTemplate.find(query, Patient.class);
        if (patientList != null && !patientList.isEmpty()) {
            patient = patientList.get(0);

        }
        return  patient;
    }

    @Override
    public List<Patient> findByName(String name) {
        Query query = new Query();
        query.addCriteria(Criteria.where("name").is(name));
        Patient patient=null;
        List<Patient> patientList = mongoTemplate.find(query, Patient.class);

        return  patientList;    }

    @Override
    public List<Patient> findAll() {
        Query query = new Query();
        List<Patient> patients = mongoTemplate.find(query,Patient.class);
        if(patients == null){
            patients = new ArrayList<Patient>();
        }
        return patients;
    }
}
