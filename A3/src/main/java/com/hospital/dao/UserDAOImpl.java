package com.hospital.dao;

import com.hospital.model.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Corina on 04.05.2016.
 */
@Repository
public class UserDAOImpl extends  MongoGenericDAO<User,String> implements UserDAO {
    @Autowired
    MongoTemplate mongoTemplate;
    @Override
    public User find(String id) {
        Query query = new Query();
        query.addCriteria(Criteria.where("id").is(id));
        User user=null;
        List<User> userList = mongoTemplate.find(query, User.class);
        if (userList != null && !userList.isEmpty()) {
            user = userList.get(0);

        }
        return  user;

    }

    @Override
    public List<User> findByName(String name) {
        Query query = new Query();
        query.addCriteria(Criteria.where("name").is(name));
        User user=null;
        List<User> userList = mongoTemplate.find(query, User.class);

        return  userList;    }

    @Override
    public List<User> findAll() {
        Query query = new Query();
        List<User> users = mongoTemplate.find(query,User.class);
        if(users == null){
            users = new ArrayList<User>();
        }
        return users;
    }
}
