package com.hospital.dao;

import com.hospital.model.User;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * Created by Corina on 04.05.2016.
 */
@Component
public interface UserDAO extends GenericDAO<User,String> {

    User find(String id);
    List<User> findByName(String name);
    List<User> findAll();
}
