package com.hospital.dao;

import com.hospital.model.Patient;
import org.springframework.security.access.method.P;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * Created by Corina on 09.05.2016.
 */
@Component
public interface PatientDAO extends GenericDAO<Patient,String> {

    Patient find(String id);
    List<Patient> findByName(String name);
    List<Patient> findAll();

}
