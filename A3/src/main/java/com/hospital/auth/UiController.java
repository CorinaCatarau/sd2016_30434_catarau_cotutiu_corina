package com.hospital.auth;

import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 * Created by Corina on 11.05.2016.
 */
@Controller
@RequestMapping("/")
public class UiController {
    public static final String PATH = "common";
    public static final String USER = "[USER_ROLE]";
    public static final String ADMIN = "[ADMIN_ROLE]";
    public static final String DOCTOR = "[DOCTOR_ROLE]";



    @RequestMapping(value = "/{path}", method = RequestMethod.GET)
    public String getPathPage(@PathVariable("path") String path) {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        return path;

    }


    @RequestMapping(value = "/admin/{path}", method = RequestMethod.GET)
    public String getAdminPathPage(@PathVariable("path") String path) {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        if(ADMIN.equals(auth.getAuthorities().toString())||DOCTOR.equals(auth.getAuthorities().toString())){
            return path;
        }
        return "login";

    }
}