package com.clothes.dao;

import com.clothes.model.Order;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * Created by Corina on 23.05.2016.
 */
@Component
public interface OrderDAO extends GenericDAO<Order,String> {
    Order find(String id);
    List<Order> findAll(String userId);

}
