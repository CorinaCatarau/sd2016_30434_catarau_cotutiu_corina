package com.clothes.dao;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Component;

/**
 * Created by Corina on 22.05.2016.
 */
@Component
public  class MongoGenericDao<T,K> implements GenericDAO<T, K> {
    @Autowired
    private MongoTemplate mongoTemplate;

    @Override
    public void create(T document) {mongoTemplate.insert(document);}

    @Override
    public void update(T object) { mongoTemplate.save(object);}

    @Override
    public void delete(K key,Class<T> entityClass) {mongoTemplate.remove(Query.query(Criteria.where("id").is(key)), entityClass);}


}

