package com.clothes.dao;

import com.clothes.model.Product;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Corina on 22.05.2016.
 */
@Repository
public class ProductDAOImpl extends MongoGenericDao<Product,String> implements ProductDAO {
    @Autowired
    MongoTemplate mongoTemplate;
    @Override
    public Product find(String id) {
        Query query = new Query();
        query.addCriteria(Criteria.where("id").is(id));
        Product product=null;
        List<Product> productList = mongoTemplate.find(query, Product.class);
        if (productList != null && !productList.isEmpty()) {
            product = productList.get(0);

        }
        return  product;

    }

    @Override
    public List<Product> findByPrice(int price) {
        Query query = new Query();
        query.addCriteria(Criteria.where("price").is(price));
        Product product=null;
        List<Product> productList = mongoTemplate.find(query, Product.class);

        return  productList;
    }

    @Override
    public List<Product> findByColor(String color) {
        Query query = new Query();
        query.addCriteria(Criteria.where("color").is(color));
        Product product=null;
        List<Product> productList = mongoTemplate.find(query, Product.class);

        return  productList;
    }

    @Override
    public List<Product> findBySize(String size) {
        Query query = new Query();
        query.addCriteria(Criteria.where("size").is(size));
        Product product=null;
        List<Product> productList = mongoTemplate.find(query, Product.class);

        return  productList;
    }

    @Override
    public List<Product> findByType(String type) {
        Query query = new Query();
        query.addCriteria(Criteria.where("type").is(type));
        Product product=null;
        List<Product> productList = mongoTemplate.find(query, Product.class);

        return  productList;
    }

    @Override
    public List<Product> findByGender(String gender) {
        Query query = new Query();
        query.addCriteria(Criteria.where("gender").is(gender));
        Product product=null;
        List<Product> productList = mongoTemplate.find(query, Product.class);

        return  productList;
    }

    @Override
    public List<Product> findByFabric(String fabric) {
        Query query = new Query();
        query.addCriteria(Criteria.where("fabric").is(fabric));
        Product product=null;
        List<Product> productList = mongoTemplate.find(query, Product.class);

        return  productList;
    }

    @Override
    public List<Product> multiSearch(List<com.clothes.model.Criteria> searchCriterias) {
        Query query = new Query();
        for(int i=0;i<searchCriterias.size();i++) {
            if("".equals(searchCriterias.get(i).getStringValue())) {
                query.addCriteria(Criteria.where(searchCriterias.get(i).getName()).is(searchCriterias.get(i).getIntValue()));
            }else {
                query.addCriteria(Criteria.where(searchCriterias.get(i).getName()).is(searchCriterias.get(i).getStringValue()));

            }
        }
        Product product=null;
        List<Product> productList = mongoTemplate.find(query, Product.class);

        return  productList;
    }


    @Override
    public List<Product> findAll() {
        Query query = new Query();
        List<Product> products = mongoTemplate.find(query,Product.class);
        if(products == null){
            products = new ArrayList<Product>();
        }
        return products;
    }




}
