package com.clothes.dao;

import com.clothes.model.Order;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Created by Corina on 23.05.2016.
 */
@Repository
public class OrderDAOImpl  extends  MongoGenericDao<Order,String> implements OrderDAO {
    @Autowired
    MongoTemplate mongoTemplate;

    @Override
    public Order find(String id) {
        Query query = new Query();
        query.addCriteria(Criteria.where("id").is(id));
        Order order = null;
        List<Order> orderList = mongoTemplate.find(query, Order.class);
        if (orderList != null && !orderList.isEmpty()) {
            order = orderList.get(0);

        }
        return order;
    }

    @Override
    public List<Order> findAll(String userId) {
        Query query = new Query();
        query.addCriteria(Criteria.where("userId").is(userId));
        Order order = null;
        List<Order> orderList = mongoTemplate.find(query, Order.class);
        return orderList;
    }
}
