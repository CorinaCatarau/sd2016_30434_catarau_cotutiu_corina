package com.clothes.dao;


import com.clothes.model.Cart;
import com.clothes.model.Product;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * Created by Corina on 23.05.2016.
 */
@Component
public interface CartDAO extends GenericDAO<Cart,String > {
    Cart find(String id);
    List<Cart> findAll(String userId);
    List<Product> addToCart(Product product,String cartId);
    List<Product> removeFromCart(Product product,String cartId);
    void emptyCart(String cartId);

}
