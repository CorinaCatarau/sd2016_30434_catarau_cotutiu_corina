package com.clothes.dao;

import com.clothes.model.Criteria;
import com.clothes.model.Product;

import java.util.List;

/**
 * Created by Corina on 22.05.2016.
 */
public interface ProductDAO extends GenericDAO<Product,String> {

    Product find(String id);
    List<Product> findByPrice(int price);
    List<Product> findByColor(String color);
    List<Product> findBySize(String size);
    List<Product> findByFabric(String fabric);
    List<Product> findByType(String type);
    List<Product> findByGender(String gender);
    List<Product> multiSearch(List<Criteria> searchCriterias);
    List<Product> findAll();
}
