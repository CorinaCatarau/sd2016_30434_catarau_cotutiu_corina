package com.clothes.dao;

import com.clothes.model.Cart;
import com.clothes.model.Product;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Corina on 23.05.2016.
 */
@Repository
public class CartDAOImpl extends MongoGenericDao<Cart,String> implements CartDAO {
    @Autowired
    MongoTemplate mongoTemplate;

    @Override
    public Cart find(String id) {
        Query query = new Query();
        query.addCriteria(Criteria.where("id").is(id));
        Cart cart=null;
        List<Cart> cartList = mongoTemplate.find(query, Cart.class);
        if (cartList != null && !cartList.isEmpty()) {
            cart = cartList.get(0);

        }
        return  cart;
    }

    @Override
    public List<Cart> findAll(String userId) {
        Query query = new Query();
        query.addCriteria(Criteria.where("userId").is(userId));
        Cart cart = null;
        List<Cart> cartList = mongoTemplate.find(query, Cart.class);
        return cartList;
    }

    @Override
    public List<Product> addToCart(Product product,String cartId) {
        Query query = new Query();
        query.addCriteria(Criteria.where("id").is(cartId));
        Cart cart=null;
        List<Cart> cartList = mongoTemplate.find(query, Cart.class);
        if (cartList != null && !cartList.isEmpty()) {
            cart = cartList.get(0);

        }
        cart.getProducts().add(product);
        update(cart);
        return cart.getProducts();
    }

    @Override
    public List<Product> removeFromCart(Product product, String cartId) {
        Query query = new Query();
        query.addCriteria(Criteria.where("id").is(cartId));
        Cart cart=null;
        List<Cart> cartList = mongoTemplate.find(query, Cart.class);
        if (cartList != null && !cartList.isEmpty()) {
            cart = cartList.get(0);

        }
        cart.getProducts().remove(product);
        for(int i=0;i<cart.getProducts().size();i++){
            if(cart.getProducts().get(i).getId().equals(product.getId())){
                cart.getProducts().remove(i);
            }
        }

        update(cart);
        return cart.getProducts();
    }

    @Override
    public void emptyCart(String cartId) {
        Query query = new Query();
        query.addCriteria(Criteria.where("id").is(cartId));
        Cart cart=null;
        List<Cart> cartList = mongoTemplate.find(query, Cart.class);
        if (cartList != null && !cartList.isEmpty()) {
            cart = cartList.get(0);

        }
        List<Product> products=new ArrayList<Product>();
        cart.setProducts(products);
        update(cart);
    }

}
