package com.clothes.dao;

import com.clothes.model.User;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * Created by Corina on 22.05.2016.
 */
@Component
public interface UserDAO extends GenericDAO<User,String> {
    User find(String id);
    List<User> findByName(String name);
    List<User> findAll();
    User findByEmail(String email);
}
