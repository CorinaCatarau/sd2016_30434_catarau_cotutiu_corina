package com.clothes.dao;

/**
 * Created by Corina on 22.05.2016.
 */
public interface GenericDAO<T, K> {

    void create(T object);
    void update(T object);
    void delete(K key, Class<T> entity);
}
