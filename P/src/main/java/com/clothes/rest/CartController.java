package com.clothes.rest;

import com.clothes.dao.CartDAO;
import com.clothes.model.Cart;
import com.clothes.model.Product;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.method.P;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Corina on 23.05.2016.
 */

@Controller
@EnableWebMvc
@RequestMapping("/rest")
public class CartController {

    @Autowired
    private CartDAO cartDAO;

    @RequestMapping(value = "/cart", method = RequestMethod.POST,
            consumes ="application/JSON",
            produces = "application/JSON")
    @ResponseBody
    public ResponseEntity<Cart> createCart(@RequestBody Cart cart) {
        try {
            this.cartDAO.create(cart);
        }catch (Exception e) {
            return new ResponseEntity<Cart>(HttpStatus.NOT_FOUND);
        }
        HttpHeaders header = new HttpHeaders();
        header.add("Location", "/rest/carts/" + String.valueOf(cart.getId()));
        return new ResponseEntity<Cart>(cart, header, HttpStatus.CREATED);
    }

    @RequestMapping(value = "/carts/{idCart}", method = RequestMethod.PUT)
    @ResponseBody
    public ResponseEntity<Cart> updateCart(@PathVariable("idCart") String idCart, @RequestBody Cart cart) {
        try {
            cart.setId(idCart);
            cartDAO.update(cart);

        } catch (Exception e) {
            return new ResponseEntity<Cart>(HttpStatus.BAD_REQUEST);
        }
        return new ResponseEntity<Cart>(cart, HttpStatus.OK);
    }

    @RequestMapping(value = "/carts/add/{idCart}", method = RequestMethod.PUT)
    @ResponseBody
    public ResponseEntity<List<Product>> addProductToCart(@PathVariable("idCart") String idCart, @RequestBody Product product) {
        List<Product> products=new ArrayList<Product>();
        try {
           products= cartDAO.addToCart(product,idCart);

        } catch (Exception e) {
            return new ResponseEntity<List<Product>>(HttpStatus.BAD_REQUEST);
        }
        return new ResponseEntity<List<Product>>(products, HttpStatus.OK);
    }

    @RequestMapping(value = "/carts/remove/{idCart}", method = RequestMethod.PUT)
    @ResponseBody
    public ResponseEntity<List<Product>> removeProductFromCart(@PathVariable("idCart") String idCart, @RequestBody Product product) {
        List<Product> products=new ArrayList<Product>();
        try {
            products= cartDAO.removeFromCart(product,idCart);

        } catch (Exception e) {
            return new ResponseEntity<List<Product>>(HttpStatus.BAD_REQUEST);
        }
        return new ResponseEntity<List<Product>>(products, HttpStatus.OK);
    }

    @RequestMapping(value = "/carts/empty/{idCart}", method = RequestMethod.PUT)
    @ResponseBody
    public ResponseEntity<List<Product>> emptyCart(@PathVariable("idCart") String idCart) {
        List<Product> products=new ArrayList<Product>();
        try {
            cartDAO.emptyCart(idCart);

        } catch (Exception e) {
            return new ResponseEntity<List<Product>>(HttpStatus.BAD_REQUEST);
        }
        return new ResponseEntity<List<Product>>(products, HttpStatus.OK);
    }



    @RequestMapping(value = "/carts/cart/{userId}", method = RequestMethod.GET,produces = "application/JSON")
    @ResponseBody
    public  ResponseEntity<List<Cart>> findAllOfUser(@PathVariable("userId") String userId) {
        List<Cart> carts;
        try {
            carts=this.cartDAO.findAll(userId);

        } catch (Exception e) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity<>(carts, HttpStatus.OK);
    }

    @RequestMapping(value = "/carts/{idCart}",method = RequestMethod.GET,produces = "application/JSON")
    @ResponseBody
    public ResponseEntity<Cart> getCartById(@PathVariable String idCart ){
        try {
            Cart cart = cartDAO.find(idCart);
            return new ResponseEntity<>(cart, HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }


    @RequestMapping(value = "/carts/{idCart}",method = RequestMethod.DELETE)
    @ResponseBody
    public ResponseEntity<Cart> deleteCart(@PathVariable String idCart){
        try {
            cartDAO.delete(idCart,Cart.class);
            return new ResponseEntity<>( HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }
}
