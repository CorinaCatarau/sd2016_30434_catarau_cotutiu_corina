package com.clothes.rest;

import com.clothes.dao.OrderDAO;
import com.clothes.model.Order;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;

import java.util.List;

/**
 * Created by Corina on 23.05.2016.
 */

@Controller
@EnableWebMvc
@RequestMapping("/rest")
public class OrderController {

    @Autowired
    private OrderDAO orderDAO;

    @RequestMapping(value = "/order", method = RequestMethod.POST,
            consumes ="application/JSON",
            produces = "application/JSON")
    @ResponseBody
    public ResponseEntity<Order> createOrder(@RequestBody Order order) {
        try {
            this.orderDAO.create(order);
        }catch (Exception e) {
            return new ResponseEntity<Order>(HttpStatus.NOT_FOUND);
        }
        HttpHeaders header = new HttpHeaders();
        header.add("Location", "/rest/orders/" + String.valueOf(order.getId()));
        return new ResponseEntity<Order>(order, header, HttpStatus.CREATED);
    }

    @RequestMapping(value = "/orders/{idOrder}", method = RequestMethod.PUT)
    @ResponseBody
    public ResponseEntity<Order> updateOrder(@PathVariable("idOrder") String idOrder, @RequestBody Order order) {
        try {
            order.setId(idOrder);
            orderDAO.update(order);

        } catch (Exception e) {
            return new ResponseEntity<Order>(HttpStatus.BAD_REQUEST);
        }
        return new ResponseEntity<Order>(order, HttpStatus.OK);
    }
    @RequestMapping(value = "/orders/user/{userId}", method = RequestMethod.GET,produces = "application/JSON")
    @ResponseBody
    public  ResponseEntity<List<Order>> findAllOfUser(@PathVariable("userId") String userId) {
        List<Order> orders;
        try {
            orders=this.orderDAO.findAll(userId);

        } catch (Exception e) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity<>(orders, HttpStatus.OK);
    }

    @RequestMapping(value = "/orders/{idOrder}",method = RequestMethod.GET,produces = "application/JSON")
    @ResponseBody
    public ResponseEntity<Order> getOrderById(@PathVariable String idOrder ){
        try {
            Order order = orderDAO.find(idOrder);
            return new ResponseEntity<>(order, HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }


    @RequestMapping(value = "/orders/{idOrder}",method = RequestMethod.DELETE)
    @ResponseBody
    public ResponseEntity<Order> deleteOrder(@PathVariable String idOrder){
        try {
            orderDAO.delete(idOrder,Order.class);
            return new ResponseEntity<>( HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }
}
