package com.clothes.rest;

import com.clothes.dao.ProductDAO;
import com.clothes.model.Criteria;
import com.clothes.model.ListCriteria;
import com.clothes.model.Product;
import com.sun.org.apache.xerces.internal.impl.dv.util.Base64;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.util.List;

/**
 * Created by Corina on 22.05.2016.
 */
@Controller
@EnableWebMvc
@RequestMapping("/rest")
public class ProductController {

    @Autowired
    private ProductDAO productDAO;
    @Autowired
    MongoTemplate mongoTemplate;

    @RequestMapping(value = "/product", method = RequestMethod.POST,
            consumes ="application/JSON",
            produces = "application/JSON")
    @ResponseBody
    public ResponseEntity<Product> createProduct(@RequestBody Product product) {
        try {

            this.productDAO.create(product);
        }catch (Exception e) {
            return new ResponseEntity<Product>(HttpStatus.NOT_FOUND);
        }
        HttpHeaders header = new HttpHeaders();
        header.add("Location", "/rest/products/" + String.valueOf(product.getId()));
        return new ResponseEntity<Product>(product, header, HttpStatus.CREATED);
    }

    @RequestMapping(value = "/products/{idProduct}", method = RequestMethod.PUT)
    @ResponseBody
    public ResponseEntity<Product> updateProduct(@PathVariable("idProduct") String idProduct, @RequestBody Product product) {
        try {
            product.setId(idProduct);
            productDAO.update(product);

        } catch (Exception e) {
            return new ResponseEntity<Product>(HttpStatus.BAD_REQUEST);
        }
        return new ResponseEntity<Product>(product, HttpStatus.OK);
    }
    @RequestMapping(value = "/products", method = RequestMethod.GET,produces = "application/JSON")
    @ResponseBody
    public  ResponseEntity<List<Product>> getAll(Model model) {
        List<Product> products;
        try {
            products=this.productDAO.findAll();

        } catch (Exception e) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity<>(products, HttpStatus.OK);
    }

    @RequestMapping(value = "/products/{idProduct}",method = RequestMethod.GET,produces = "application/JSON")
    @ResponseBody
    public ResponseEntity<Product> getProductById(@PathVariable String idProduct ){
        try {
            Product product = productDAO.find(idProduct);
            return new ResponseEntity<>(product, HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    @RequestMapping(value = "/products/price/{price}",method = RequestMethod.GET,produces = "application/JSON")
    @ResponseBody
    public ResponseEntity<List<Product>> getProductsByPrice(@PathVariable int price ){
        try {
            List<Product> products = productDAO.findByPrice(price);
            return new ResponseEntity<>(products, HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    @RequestMapping(value = "/products/gender/{gender}",method = RequestMethod.GET,produces = "application/JSON")
    @ResponseBody
    public ResponseEntity<List<Product>> getProductsByGender(@PathVariable String gender ){
        try {
            List<Product> products = productDAO.findByGender(gender);
            return new ResponseEntity<>(products, HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    @RequestMapping(value = "/products/fabric/{fabric}",method = RequestMethod.GET,produces = "application/JSON")
    @ResponseBody
    public ResponseEntity<List<Product>> getProductsByFabric(@PathVariable String fabric ){
        try {
            List<Product> products = productDAO.findByFabric(fabric);
            return new ResponseEntity<>(products, HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    @RequestMapping(value = "/products/size/{size}",method = RequestMethod.GET,produces = "application/JSON")
    @ResponseBody
    public ResponseEntity<List<Product>> getProductsBySize(@PathVariable String size ){
        try {
            List<Product> products = productDAO.findBySize(size);
            return new ResponseEntity<>(products, HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    @RequestMapping(value = "/products/color/{color}",method = RequestMethod.GET,produces = "application/JSON")
    @ResponseBody
    public ResponseEntity<List<Product>> getProductsByColor(@PathVariable String color ){
        try {
            List<Product> products = productDAO.findByColor(color);
            return new ResponseEntity<>(products, HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    @RequestMapping(value = "/products/type/{type}",method = RequestMethod.GET,produces = "application/JSON")
    @ResponseBody
    public ResponseEntity<List<Product>> getProductsByType(@PathVariable String type ){
        try {
            List<Product> products = productDAO.findByType(type);
            return new ResponseEntity<>(products, HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    @RequestMapping(value = "/products/multisearch", method = RequestMethod.GET)
    @ResponseBody
    public ResponseEntity<List<Product>> getProductsByMultiSearch (@RequestBody ListCriteria criterias) {
        try {
            List<Product> products= productDAO.multiSearch(criterias.getCriteriaList());
            return new ResponseEntity<List<Product>>(products, HttpStatus.OK);


        } catch (Exception e) {
            return new ResponseEntity<List<Product>>(HttpStatus.BAD_REQUEST);
        }
    }

    @RequestMapping(value = "/products/{idProduct}",method = RequestMethod.DELETE)
    @ResponseBody
    public ResponseEntity<Product> deleteProduct(@PathVariable String idProduct){
        try {
            productDAO.delete(idProduct,Product.class);
            return new ResponseEntity<>( HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    @RequestMapping(value = "/product/{id}/image", method = RequestMethod.GET,produces = MediaType.IMAGE_JPEG_VALUE)
    @ResponseBody
    public ResponseEntity<String> getProductImage(@PathVariable("id") String id) {
        try{
            Product myProduct = productDAO.find(id);
            BufferedImage img = ImageIO.read(new File(myProduct.getImagePath()));
            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            ImageIO.write(img, "jpg", baos);
            baos.flush();
            byte[] bytes = baos.toByteArray();
            String encodedImage = Base64.encode(baos.toByteArray());
            HttpHeaders headers = new HttpHeaders();
            return new ResponseEntity<String>(encodedImage, headers, HttpStatus.OK);

        } catch (Exception e) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }
}