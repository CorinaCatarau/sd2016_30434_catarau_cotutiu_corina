package com.clothes.model;

import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.Field;

/**
 * Created by Corina on 22.05.2016.
 */
@Document(collection ="PRODUCT")
public class Product {
    @Field
    private  String id;
    @Field
    private String name;
    @Field
    private int price;
    @Field
    private int quantity;
    @Field
    private String color;
    @Field
    private String size;
    @Field
    private String fabric;
    @Field
    private  String gender;
    @Field
    private String imagePath;
    @Field
    private String type;
    @Field
    private int totalQuantity;


    public String getSize() {return size;}
    public void setSize(String size) {this.size = size;}
    public String getId() {return id;}
    public void setId(String id) {this.id = id;}
    public String getName() {return name;}
    public void setName(String name) {this.name = name;}
    public int getPrice() {return price;}
    public void setPrice(int price) {this.price = price;}
    public String getColor() {return color;}
    public void setColor(String color) {this.color = color;}
    public int getQuantity() {return quantity;}
    public void setQuantity(int quantity) {this.quantity = quantity;}
    public String getFabric() {return fabric;}
    public void setFabric(String fabric) {this.fabric = fabric;}
    public String getGender() {return gender;}
    public void setGender(String gender) {this.gender = gender;}
    public String getImagePath() {return imagePath;}
    public void setImagePath(String imagePath) {this.imagePath = imagePath;}
    public String getType() {return type;}
    public void setType(String type) {this.type = type;}
    public int getTotalQuantity() {return totalQuantity;}
    public void setTotalQuantity(int totalQuantity) {this.totalQuantity = totalQuantity;}
}
