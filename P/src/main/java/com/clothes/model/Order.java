package com.clothes.model;

import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.Field;

import java.util.List;

/**
 * Created by Corina on 22.05.2016.
 */
@Document(collection ="ORDER")
public class Order {
    @Field
    private String id;
    @Field
    private String userId;
    @Field
    private List<Product> products;
    @Field
    private String date;

    public String getId() {return id;}
    public void setId(String id) {this.id = id;}
    public String getUserId() {return userId;}
    public void setUserId(String userId) {this.userId = userId;}
    public List<Product> getProducts() {return products;}
    public void setProducts(List<Product> products) {this.products = products;}
    public String getDate() {return date;}
    public void setDate(String date) {this.date = date;}
}
