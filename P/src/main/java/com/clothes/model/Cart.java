package com.clothes.model;

import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.Field;

import java.util.List;

/**
 * Created by Corina on 22.05.2016.
 */
@Document(collection ="CART")
public class Cart {
    @Field
    private String id;
    @Field
    private String userId;
    @Field
    private List<Product> products;
    @Field
    private int total;

    public String getId() {return id;}
    public void setId(String id) {this.id = id;}
    public String getUserId() {return userId;}
    public void setUserId(String userId) {this.userId = userId;}
    public List<Product> getProducts() {return products;}
    public void setProducts(List<Product> products) {this.products = products;}
    public int getTotal() {return total;}
    public void setTotal(int total) {this.total = total;}
}
