package com.clothes.model;

import java.util.List;

/**
 * Created by Corina on 26.05.2016.
 */
public class ListCriteria {
   private List<Criteria> criteriaList;

    public List<Criteria> getCriteriaList() {return criteriaList;}
    public void setCriteriaList(List<Criteria> criteriaList) {this.criteriaList = criteriaList;}
}
