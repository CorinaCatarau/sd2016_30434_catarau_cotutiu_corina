package com.clothes.model;

/**
 * Created by Corina on 22.05.2016.
 */
public class Criteria {
    private String name;
    private String stringValue;
    private int intValue;

    public String getName() {return name;}
    public void setName(String name) {this.name = name;}
    public String getStringValue() {return stringValue;}
    public void setStringValue(String stringValue) {this.stringValue = stringValue;}
    public int getIntValue() {return intValue;}
    public void setIntValue(int intValue) {this.intValue = intValue;}
}
