
<%--
  Created by IntelliJ IDEA.
  User: Corina
  Date: 26.05.2016
  Time: 1:01
  To change this template use File | Settings | File Templates.
--%>


                <div class="col-md-9" id="customer-orders">
                    <div class="box">
                        <h1>My orders</h1>

                        <p class="lead">Your orders on one place.</p>
                        <p class="text-muted">If you have any questions, please feel free to <a href="contact.html">contact us</a>, our customer service center is working for you 24/7.</p>

                        <hr>

                        <div class="table-responsive">
                            <table class="table table-hover">
                                <thead>
                                <tr>
                                    <th>Order</th>
                                    <th>Date</th>
                                    <th>Status</th>
                                    <th>Action</th>
                                </tr>
                                </thead>
                                <tbody>
                                <tr ng-repeat="order in orders">
                                    <th># {{order.id}}</th>
                                    <td>{{order.date}}</td>
                                    <td><span class="label label-info">Sent</span>
                                    </td>
                                    <td><a ng-click="showOrderDetails(order)" class="btn btn-primary btn-sm">View</a>
                                    </td>
                                </tr>

                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
