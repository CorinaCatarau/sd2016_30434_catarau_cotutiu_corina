<%--
  Created by IntelliJ IDEA.
  User: Corina
  Date: 25.05.2016
  Time: 21:40
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<div id="all">

    <div id="content">
        <div class="container">

            <div class="col-md-12">
                <ul class="breadcrumb">
                    <li><a href="#">Home</a>
                    </li>
                    <li>Shopping cart</li>
                </ul>
            </div>

            <div class="col-md-9" id="basket">

                <div class="box">

                    <form method="post" >

                        <h1>Shopping cart</h1>
                        <p class="text-muted">You currently have {{nrItems}} item(s) in your cart.</p>
                        <div class="table-responsive">
                            <table class="table">
                                <thead>
                                <tr>
                                    <th colspan="2">Product</th>
                                    <th>Unit price</th>
                                    <th>Discount</th>
                                    <th colspan="2">Total</th>
                                </tr>
                                </thead>
                                <tbody>
                                <tr ng-repeat="item in items">
                                    <td>
                                        <a href="#">
                                            <img src="{{item.imagePath}}" class="img-responsive product">
                                        </a>
                                    </td>
                                    <td><a href="#">{{item.name}}</a>
                                    </td>

                                    <td>{{item.price}}</td>
                                    <td><a ng-click="removeFromCart(item)"><i class="fa fa-trash-o"></i></a>
                                    </td>
                                </tr>

                            </table>

                        </div>
                        <!-- /.table-responsive -->

                        <div class="box-footer">
                            <div class="pull-left">
                                <a href="category.html" class="btn btn-default"><i class="fa fa-chevron-left"></i> Continue shopping</a>
                            </div>
                            <div class="pull-right">
                                <button class="btn btn-default"><i class="fa fa-refresh"></i> Update basket</button>
                                <button type="submit" class="btn btn-primary" ng-click="checkout()">Proceed to checkout <i class="fa fa-chevron-right"></i>
                                </button>
                            </div>
                        </div>

                    </form>

                </div>
                <!-- /.box -->


                 </div>
            </div>
        </div>
    </div>
