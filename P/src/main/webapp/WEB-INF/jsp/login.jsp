<%--
  Created by IntelliJ IDEA.
  User: Corina
  Date: 24.05.2016
  Time: 14:38
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!DOCTYPE html>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<html ng-app="app">
<head>
    <head>
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- Angular Material style sheet -->
        <link rel="stylesheet" href="http://ajax.googleapis.com/ajax/libs/angular_material/1.0.0/angular-material.min.css">
        <!-- load bootstrap and fontawesome via CDN -->
        <link rel="stylesheet" href="//netdna.bootstrapcdn.com/bootstrap/3.0.0/css/bootstrap.min.css" />
        <link rel="stylesheet" href="//netdna.bootstrapcdn.com/font-awesome/4.0.0/css/font-awesome.css" />
        <link rel="stylesheet" href="/src/main/webapp/css/custom.css"/>

        <!-- SPELLS -->
        <!-- load angular and angular route via CDN -->
        <script src="http://ajax.googleapis.com/ajax/libs/angularjs/1.4.8/angular.min.js"></script>
        <script src="http://ajax.googleapis.com/ajax/libs/angularjs/1.4.8/angular-animate.min.js"></script>
        <script src="http://ajax.googleapis.com/ajax/libs/angularjs/1.4.8/angular-aria.min.js"></script>
        <script src="http://ajax.googleapis.com/ajax/libs/angularjs/1.4.8/angular-messages.min.js"></script>

        <!-- Angular Material Library -->
        <script src="http://ajax.googleapis.com/ajax/libs/angular_material/1.0.0/angular-material.min.js"></script>

        <script type="text/javascript" src="js/app.js"></script>
        <script type="text/javascript" src="js/users.js"></script>


        <meta name="keywords" content="">

        <link href='http://fonts.googleapis.com/css?family=Roboto:400,500,700,300,100' rel='stylesheet' type='text/css'>

        <!-- styles -->
        <link href="css/font-awesome.css" rel="stylesheet">
        <link href="css/bootstrap.min.css" rel="stylesheet">
        <link href="css/animate.min.css" rel="stylesheet">
        <link href="css/owl.carousel.css" rel="stylesheet">
        <link href="css/owl.theme.css" rel="stylesheet">

        <!-- theme stylesheet -->
        <link href="css/style.default.css" rel="stylesheet" id="theme-stylesheet">

        <!-- your stylesheet with modifications -->
        <link href="css/custom.css" rel="stylesheet">

        <script src="js/respond.min.js"></script>

        <link rel="shortcut icon" href="favicon.png">



    </head>

<body ng-controller="AppCtrl">


<div id="all">

    <div id="content">
        <div class="container">

            <div class="col-md-12">

                <ul class="breadcrumb">
                    <li><a href="#">Home</a>
                    </li>
                    <li>New account / Sign in</li>
                </ul>

            </div>

            <div class="col-md-6">
                <div class="box">
                    <h1>New account</h1>

                    <form name="userFormCreate" ng-submit="createUser(newUser,$event)">

                        <md-input-container class="md-block">
                            <label>Username</label>
                            <input  required="" md-no-asterisk="" name="newUserName" ng-model="newUser.name">
                            <div ng-messages="userFormCreate.newUserName.$error">
                                <div ng-message="required">This is required.</div>
                            </div>
                        </md-input-container>


                        <md-input-container class="md-block">
                            <label>Password</label>
                            <input required="" name="newUserPassword" ng-model="newUser.password" ng-pattern="/^(?=.*[A-Za-z])(?=.*\d)[A-Za-z\d]{6,}$/">
                            <div ng-messages="userFormCreate.newUserPassword.$error"  ng-show="userFormCreate.newUserPassword.$dirty">
                                <div ng-message="required">This is required.</div>
                                <div ng-message="pattern">The password must contain at least 6 characters ,1 numeric and 1 letter</div>
                            </div>
                        </md-input-container>

                        <md-input-container class="md-block">
                            <label>Email</label>
                            <input type="email" required="" name="newUserEmail" ng-model="newUser.email" >
                            <div ng-messages="userFormCreate.newUserPassword.$error"  ng-show="userFormCreate.newUserEmail.$dirty">
                                <div ng-message="required">This is required.</div>
                            </div>
                        </md-input-container>

                        <md-input-container class="md-block">
                            <label>Address</label>
                            <input  required="" md-no-asterisk="" name="newUserAddress" ng-model="newUser.address">
                            <div ng-messages="userFormCreate.newUserAddress.$error">
                                <div ng-message="required">This is required.</div>
                            </div>
                        </md-input-container>



                        <md-button type="submit" id="submit" class="lavender" ng-disabled="userFormCreate.$invalid">Submit</md-button>

                    </form>
                </div>
            </div>

            <div class="col-md-6">
                <div class="box">
                    <h1>Login</h1>


                    <form layout="column" class="md-padding" method="post" action="<c:url value='/j_spring_security_check' />">
                        <md-input-container>
                            <label>EmailAddress</label>
                            <input type="text" name="j_username" ng-model="username" />
                        </md-input-container>
                        <md-input-container>
                            <label>Password</label>
                            <input type="password" name="j_password" />
                        </md-input-container>

                        <div layout="row" layout-align="center center" style="padding-top:20px;">
                            <input type="submit" value="Login" />
                        </div>

                        <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}"/>
                    </form>
                </div>
            </div>


        </div>
        <!-- /.container -->
    </div>
    <!-- /#content -->


    <!-- *** FOOTER ***
_________________________________________________________ -->
    <div id="footer" data-animate="fadeInUp">
        <div class="container">
            <div class="row">
                <div class="col-md-3 col-sm-6">
                    <h4>Pages</h4>

                    <ul>
                        <li><a href="text.html">About us</a>
                        </li>
                        <li><a href="text.html">Terms and conditions</a>
                        </li>
                        <li><a href="faq.html">FAQ</a>
                        </li>
                        <li><a href="contact.html">Contact us</a>
                        </li>
                    </ul>

                    <hr>

                    <h4>User section</h4>

                    <ul>
                        <li><a href="#" data-toggle="modal" data-target="#login-modal">Login</a>
                        </li>
                        <li><a href="register.html">Regiter</a>
                        </li>
                    </ul>

                    <hr class="hidden-md hidden-lg hidden-sm">

                </div>
                <!-- /.col-md-3 -->


                <!-- /.col-md-3 -->
            </div>
        </div>
        <!-- /.row -->

    </div>
    <!-- /.container -->
</div>




<!-- *** SCRIPTS TO INCLUDE ***
_________________________________________________________ -->
<script src="js/jquery-1.11.0.min.js"></script>
<script src="js/bootstrap.min.js"></script>
<script src="js/jquery.cookie.js"></script>
<script src="js/waypoints.min.js"></script>
<script src="js/modernizr.js"></script>
<script src="js/bootstrap-hover-dropdown.js"></script>
<script src="js/owl.carousel.min.js"></script>
<script src="js/front.js"></script>



</body>

</html>
