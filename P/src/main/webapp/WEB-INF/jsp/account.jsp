<%--
  Created by IntelliJ IDEA.
  User: Corina
  Date: 26.05.2016
  Time: 1:01
  To change this template use File | Settings | File Templates.
--%>




<div id="all">

    <div id="content">
        <div class="container">

            <div class="col-md-12">

                <ul class="breadcrumb">
                    <li><a href="#">Home</a>
                    </li>
                    <li>My account</li>
                </ul>

            </div>

            <div class="col-md-3">
                <!-- *** CUSTOMER MENU ***
_________________________________________________________ -->
                <div class="panel panel-default sidebar-menu">

                    <div class="panel-heading">
                        <h3 class="panel-title">Customer section</h3>
                    </div>

                    <div class="panel-body">

                        <ul class="nav nav-pills nav-stacked">
                            <li class="active">
                                <a ng-click="showOrders('orders')"><i class="fa fa-list"></i> My orders</a>
                            </li>
                            <li>
                                <a ng-click="showOrders('account')"><i class="fa fa-user"></i> My account</a>
                            </li>
                            <li>
                                <a href="index.html"><i class="fa fa-sign-out"></i> Logout</a>
                            </li>
                        </ul>
                    </div>

                </div>
                <!-- /.col-md-3 -->

                <!-- *** CUSTOMER MENU END *** -->
            </div>

            <div class="col-md-9" ng-if="accountInfo">
                <div class="box">
                    <h1>My account</h1>
                    <p class="lead">Change your personal details or your password here.</p>

                    <h3>Change password</h3>

                    <form>
                        <div class="row">
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label for="password_old">Old password</label>
                                    <input ng-model="activeUser.password" type="password" class="form-control" id="password_old">
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label for="password_1">New password</label>
                                    <input ng-model="newPass" type="password" class="form-control" id="password_1">
                                </div>
                            </div>

                        </div>
                        <!-- /.row -->

                        <div class="col-sm-12 text-center">
                            <button type="submit" class="btn btn-primary" ng-click="updatePass(newPass)"><i class="fa fa-save"></i> Save new password</button>
                        </div>
                    </form>

                    <hr>

                    <h3>Personal details</h3>
                    <form>
                        <div class="row">
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label for="firstname">Name</label>
                                    <input ng-model="activeUser.name" type="text" class="form-control" id="firstname">
                                </div>
                            </div>

                        </div>
                        <!-- /.row -->

                        <div class="row">

                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label for="address">Address</label>
                                    <input ng-model="activeUser.address" type="text" class="form-control" id="address">
                                </div>
                            </div>
                        </div>
                        <!-- /.row -->

                        <div class="row">

                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label for="email">Email</label>
                                    <input  ng-model="activeUser.email"type="email" class="form-control" id="email">
                                </div>
                            </div>
                            <div class="col-sm-12 text-center">
                                <button type="submit" class="btn btn-primary" ng-click="updateUser(activeUser)"><i class="fa fa-save"></i> Save changes</button>

                            </div>
                        </div>
                    </form>
                </div>
            </div>
            <orders ng-if="ordersInfo"></orders>
            <order ng-if="orderInfo"></order>

        </div>
        </div>
    </div>
