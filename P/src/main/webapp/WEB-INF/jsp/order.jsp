<%--
  Created by IntelliJ IDEA.
  User: Corina
  Date: 26.05.2016
  Time: 3:42
  To change this template use File | Settings | File Templates.
--%>
<div class="col-md-9" id="customer-order">
    <div class="box">
        <h1>Order # {{selectedOrder.id}}</h1>

        <p class="lead">Order # {{selectedOrder.id}} was placed on <strong>{{selectedOrder.date}}</strong> </p>
        <hr>

        <div class="table-responsive">
            <table class="table">
                <thead>
                <tr>
                    <th colspan="2">Product</th>
                    <th>Quantity</th>
                    <th>Unit price</th>
                </tr>
                </thead>
                <tbody>
                <tr ng-repeat="oproduct in selectedOrder.products">
                    <td>
                        <a href="#">
                            <img src="img/detailsquare.jpg" alt="White Blouse Armani">
                        </a>
                    </td>
                    <td><a >{{oproduct.name}}</a>
                    </td>
                    <td>{{oproduct.quantity}}</td>
                    <td>{{oproduct.price}}</td>

                </tr>

                </tfoot>
            </table>

        </div>
        <!-- /.table-responsive -->



    </div>
</div>

</div>
<!-- /.container -->
</div>
<!-- /#content -->