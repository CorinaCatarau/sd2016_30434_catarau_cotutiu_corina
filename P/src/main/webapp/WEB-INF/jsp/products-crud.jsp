<%--
  Created by IntelliJ IDEA.
  User: Corina
  Date: 26.05.2016
  Time: 4:57
  To change this template use File | Settings | File Templates.
--%>
<div class="container">
    <div class="row">
        <md-input-container>
            <label>Report Type</label>
            <md-select  ng-model="reportType" >
                <md-option  >PDF</md-option>
                <md-option  >CSV</md-option>
            </md-select>
        </md-input-container>
        <md-button class="lavender"  ng-click="generateReport(reportType)">GENERATE REPORT</md-button>
        <md-button class="lavender" ng-model="collapsed" ng-click="collapsed=!collapsed">ADD PRODUCT</md-button>
    </div>
    <div class="row">

        <div ng-show="collapsed">
            <form name="productFormCreate" ng-submit="createProduct(newProd,$event)" class="myForm">
                <md-input-container class="md-block">
                    <label>Name</label>
                    <input  required="" md-no-asterisk="" name="newProdName" ng-model="newProd.name">
                    <div ng-messages="productFormCreate.newProdName.$error">
                        <div ng-message="required">This is required.</div>
                    </div>
                </md-input-container>

                <md-input-container class="md-icon-float md-icon-right md-block">
                    <label>Price</label>
                    <input  min="0" type="number" ng-model="newProd.price" >
                </md-input-container>

                <md-input-container class="md-icon-float md-icon-right md-block">
                    <label>Quantity</label>
                    <input  min="0" type="number" ng-model="newProd.quantity" >
                </md-input-container>
                <md-input-container>
                    <label>Color</label>
                    <md-select placeholder="Size" ng-model="newProd.color" >
                        <md-option>Red</md-option>
                        <md-option>Green</md-option>
                        <md-option>Blue</md-option>
                        <md-option>White</md-option>
                        <md-option>Violet</md-option>
                        <md-option>Brown</md-option>
                        <md-option>Black</md-option>
                    </md-select>
                </md-input-container>

                <md-input-container>
                    <label>Size</label>
                    <md-select placeholder="Size" ng-model="newProd.size" >
                        <md-option>XS</md-option>
                        <md-option>S</md-option>
                        <md-option>M</md-option>
                        <md-option>L</md-option>
                        <md-option>XL</md-option>
                        <md-option>XXL</md-option>
                        <md-option>ALL</md-option>
                    </md-select>
                </md-input-container>



                <md-input-container>
                    <label>Fabric</label>
                    <md-select placeholder="Fabric" ng-model="newProd.fabric" >
                        <md-option>cotton</md-option>
                        <md-option>silk</md-option>
                        <md-option>jeans</md-option>
                        <md-option>elastan</md-option>

                    </md-select>
                </md-input-container>

                <md-input-container>
                    <label>Gender</label>
                    <md-select placeholder="Gender" ng-model="newProd.gender" >
                        <md-option>M</md-option>
                        <md-option>F</md-option>
                    </md-select>
                </md-input-container>

                <md-input-container class="md-block">
                    <label>Image Path</label>
                    <input required="" name="newProdPath" ng-model="newProd.imagePath" >
                    <div ng-messages="productFormCreate.newProdPath.$error"  >
                        <div ng-message="required">This is required.</div>
                    </div>
                </md-input-container>

                <md-input-container>
                    <label>Type</label>
                    <md-select placeholder="Type" ng-model="newProd.type" >
                        <md-option>blouse</md-option>
                        <md-option>jeans</md-option>
                        <md-option>dress</md-option>
                        <md-option>skirt</md-option>
                        <md-option>t-shirt</md-option>
                        <md-option>bleaser</md-option>
                        <md-option>cardigan</md-option>
                        <md-option>shirt</md-option>
                        <md-option>costume</md-option>

                    </md-select>
                </md-input-container>



                <md-input-container class="md-block">
                    <label>TotalQuantity</label>
                    <input  required="" md-no-asterisk="" name="newProdQuantity" ng-model="newProd.quantity">
                    <div ng-messages="productFormCreate.newProdQuantity.$error">
                        <div ng-message="required">This is required.</div>
                    </div>
                </md-input-container>


                <md-button type="submit" id="submit" class="lavender" ng-disabled="productFormCreate.$invalid">Submit</md-button>

            </form>
        </div>
        <div   class="media"  ng-repeat="product in products">

            <div class="media-body" ng-if="!editModeProduct($index)">
                <div class="col-md-4 col-sm-6">
                    <div class="product" >
                        <div class="flip-container">
                            <div class="flipper">
                                <div class="front">
                                    <a href="detail.html">
                                        <img src="{{product.imagePath}}" class="img-responsive product">
                                    </a>
                                </div>

                            </div>
                        </div>

                        <div class="text">
                            <h3><a>{{product.name}}</a></h3>
                            <p class="price">{{product.price}}</p>
                            <p class="price">{{product.quantity}}</p>

                            <p class="buttons">
                                <a  class="btn btn-default" ng-click="setEditModeBook($index)">Edit</a>
                                <a class="btn btn-primary" ng-click="deleteProduct(product,products)">Delete</a>
                            </p>
                        </div>
                        <!-- /.text -->
                    </div>
                    <!-- /.product -->
                </div>
            </div>

            <div class="media-body"  ng-if="editModeBook($index)">
                <md-button >
                    <i class="material-icons" ng-click="updateProduct(product,$event)"  >arrow_forward</i>
                </md-button>
                <ul class="list-group">
                    <li class="list-group-item">
                        <md-input-container class="md-block">
                            <label>Name</label>
                            <input  required="" md-no-asterisk="" name="newProdName" ng-model="product.name">
                            <div ng-messages="productFormCreate.newProdName.$error">
                                <div ng-message="required">This is required.</div>
                            </div>
                        </md-input-container>

                        <md-input-container class="md-icon-float md-icon-right md-block">
                            <label>Price</label>
                            <input  min="0" type="number" ng-model="product.price" >
                        </md-input-container>

                        <md-input-container class="md-icon-float md-icon-right md-block">
                            <label>Quantity</label>
                            <input  min="0" type="number" ng-model="product.quantity" >
                        </md-input-container>
                        <md-input-container>
                            <label>Color</label>
                            <md-select placeholder="Size" ng-model="product.color" >
                                <md-option>Red</md-option>
                                <md-option>Green</md-option>
                                <md-option>Blue</md-option>
                                <md-option>White</md-option>
                                <md-option>Violet</md-option>
                                <md-option>Brown</md-option>
                                <md-option>Black</md-option>
                            </md-select>
                        </md-input-container>

                        <md-input-container>
                            <label>Size</label>
                            <md-select placeholder="Size" ng-model="product.size" >
                                <md-option>XS</md-option>
                                <md-option>S</md-option>
                                <md-option>M</md-option>
                                <md-option>L</md-option>
                                <md-option>XL</md-option>
                                <md-option>XXL</md-option>
                                <md-option>ALL</md-option>
                            </md-select>
                        </md-input-container>



                        <md-input-container>
                            <label>Fabric</label>
                            <md-select placeholder="Fabric" ng-model="product.fabric" >
                                <md-option>cotton</md-option>
                                <md-option>silk</md-option>
                                <md-option>jeans</md-option>
                                <md-option>elastan</md-option>

                            </md-select>
                        </md-input-container>

                        <md-input-container>
                            <label>Gender</label>
                            <md-select placeholder="Gender" ng-model="product.gender" >
                                <md-option>M</md-option>
                                <md-option>F</md-option>
                            </md-select>
                        </md-input-container>

                        <md-input-container class="md-block">
                            <label>Image Path</label>
                            <input required="" name="newProdPath" ng-model="product.imagePath" >
                            <div ng-messages="productFormCreate.newProdPath.$error"  >
                                <div ng-message="required">This is required.</div>
                            </div>
                        </md-input-container>

                        <md-input-container>
                            <label>Type</label>
                            <md-select placeholder="Type" ng-model="product.type" >
                                <md-option>blouse</md-option>
                                <md-option>jeans</md-option>
                                <md-option>dress</md-option>
                                <md-option>skirt</md-option>
                                <md-option>t-shirt</md-option>
                                <md-option>bleaser</md-option>
                                <md-option>cardigan</md-option>
                                <md-option>shirt</md-option>
                                <md-option>costume</md-option>

                            </md-select>
                        </md-input-container>



                        <md-input-container class="md-block">
                            <label>TotalQuantity</label>
                            <input  required="" md-no-asterisk="" name="newProdQuantity" ng-model="product.quantity">
                            <div ng-messages="productFormCreate.newProdQuantity.$error">
                                <div ng-message="required">This is required.</div>
                            </div>
                        </md-input-container>


                </ul>

            </div>
        </div>


    </div>
</div>