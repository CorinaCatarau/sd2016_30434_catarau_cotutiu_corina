
/**
 * Created by Corina on 24.05.2016.
 */
/**
 * Created by Corina on 26.04.2016.
 */
var myApp = angular.module('app');
app.directive('basket', function() {
    return {
        restrict: 'E'
        ,
        replace: true,
        templateUrl: 'basket'
    };
});

app.directive('account', function() {
    return {
        restrict: 'E'
        ,
        replace: true,
        templateUrl: 'account'
    };
});

app.directive('orders', function() {
    return {
        restrict: 'E'
        ,
        replace: true,
        templateUrl: 'orders'
    };
});


app.directive('order', function() {
    return {
        restrict: 'E'
        ,
        replace: true,
        templateUrl: 'order'
    };
});


app .controller('UserCtrl',['$scope','$http','$mdDialog','$mdMedia','$window', function($scope,$http, $mdDialog, $mdMedia,$window){
    $scope.username;
    $scope.allBooks;
    $scope.advanced=false;
    $scope.amountOfInterval=0;
    $scope.nrMen=0;
    $scope.nrWomen=0;
    $scope.types=[];
    $scope.activeUser;
    $scope.colors=[];
    $scope.itemsView=true;
    $scope.basketView=false;
    $scope.accountView=false;
    $scope.accountInfo=true;
    $scope.ordersInfo=false;
    $scope.orderInfo=false;


    console.log("${username}");
    console.log("<%=username%>");

    $scope.switchView=function(view){
        if(view==="itemView"){
            $scope.itemsView=true;
            $scope.accountView=false;
            $scope.basketView=false;
        }else if(view==="accountView"){
            $scope.itemsView=false;
            $scope.accountView=true;
            $scope.basketView=false;
        }
    }

    $scope.showOrders=function(view){
        if(view==="orders"){
            $scope.accountInfo=false;
            $scope.ordersInfo=true;
            $scope.orderInfo=false;
            getOrders();
        }else if(view==="account"){
            $scope.accountInfo=true;
            $scope.ordersInfo=false;
            $scope.orderInfo=false;

        }
    }

    $scope.showOrderDetails=function(order){
            $scope.accountInfo=false;
            $scope.ordersInfo=false;
            $scope.orderInfo=true;
        $scope.selectedOrder=order;

    }
    $scope.basket=function(){
        getCart();
        $scope.itemsView=!$scope.itemsView;
        $scope.basketView=!$scope.basketView;
    }

     $scope.getData = function(username) {
         console.log(username);
     $http({
     method: 'GET',
     url: '/rest/users/name/'+username
     }).then(function successCallback(data) {
        $scope.activeUser=data.data[0];
     $window.localStorage.setItem("userId",data.data[0].id);
     }, function errorCallback(data) {
     console.log("error");
     });
     };
    $scope.showAlert = function(ev) {
        $mdDialog.show(
            $mdDialog.alert()
                .parent(angular.element(document.querySelector('#popupContainer')))
                .clickOutsideToClose(true)
                .title('Forbidden')
                .textContent('Username already used.')
                .ariaLabel('Alert Dialog Demo')
                .ok('Got it!')
                .targetEvent(ev)
        );
    };


    $scope.showAlertSuccess = function(ev) {
        $mdDialog.show(
            $mdDialog.alert()
                .parent(angular.element(document.querySelector('#popupContainer')))
                .clickOutsideToClose(true)
                .title('Success')
                .textContent('Your account has been created')
                .ariaLabel('Alert Dialog Demo')
                .ok('Got it!')
                .targetEvent(ev)
        );
    };


    $scope.updateCart=function(product){
       getCart();
        console.log($scope.userCart);

            $http({
                method: 'PUT',
                url: '/rest/carts/add/' + $scope.userCart.id,
                headers: {
                    'Content-Type': 'application/json'
                },
                data: product
            }).then(function successCallback(data) {
                console.log("cart added");
            }, function errorCallback(data) {

                console.log("error at user update");
            });


    }

    $scope.updatePass=function(pass){
        $scope.activeUser.password=pass;
        $http({
            method: 'PUT',
            url: '/rest/users/'+$scope.activeUser.id,
            headers: {
                'Content-Type': 'application/json'
            },
            data: $scope.activeUser
        }).then(function successCallback(data) {
            console.log("password updated added");
        }, function errorCallback(data) {

            console.log("error at password update");
        });

    }

    $scope.updateUser=function(user){
        $http({
            method: 'PUT',
            url: '/rest/users/'+$scope.activeUser.id,
            headers: {
                'Content-Type': 'application/json'
            },
            data: user
        }).then(function successCallback(data) {
            console.log("user updated added");
        }, function errorCallback(data) {

            console.log("error at user update");
        });

    }

    $scope.removeFromCart=function(product){
        getCart();
        console.log($scope.userCart);
        $http({
            method: 'PUT',
            url: '/rest/carts/remove/'+$scope.userCart.id,
            headers: {
                'Content-Type': 'application/json'
            },
            data: product
        }).then(function successCallback(data) {
            console.log("product added");
        }, function errorCallback(data) {

            console.log("error at user update");
        });

    }

    $scope.createOrder = function() {
        var newDate=new Date();
        var order={
            "userId":$window.localStorage.getItem("userId"),
            "products":$scope.items,
            "date":newDate.toString()
        };
        var orderNew=angular.fromJson(angular.toJson(order));
        $http({
            method: 'POST',
            url: '/rest/order',
            headers: {
                'Content-Type': 'application/json'
            },
            data: orderNew
        }).then(function successCallback(data) {
            console.log("order created");


        }, function errorCallback(data) {
            console.log("error at order create");
        });
        ;
    };

    $scope.emptyCart=function(){
        getCart();
        console.log($scope.userCart);
        $http({
            method: 'PUT',
            url: '/rest/carts/empty/'+$scope.userCart.id,
            headers: {
                'Content-Type': 'application/json'
            },
        }).then(function successCallback(data) {
            console.log("empty cart");
        }, function errorCallback(data) {

            console.log("error at empty cart");
        });

    }
    $scope.checkout=function(){
        $scope.createOrder();
        $scope.emptyCart();
    }

    getCart = function() {
        $http({
            method: 'GET',
            url: '/rest/carts/cart/'+$window.localStorage.getItem("userId")
        }).then(function successCallback(data) {
            $scope.userCart= data.data[0];
            $scope.items=data.data[0].products;
            $scope.nrItems=data.data[0].products.length;
        }, function errorCallback(data) {
            return null;
            console.log("error");
        });
    };


    getOrders = function() {
        $http({
            method: 'GET',
            url: '/rest/orders/user/'+$window.localStorage.getItem("userId")
        }).then(function successCallback(data) {
          $scope.orders=data.data;
        }, function errorCallback(data) {
            return null;
            console.log("error");
        });
    };


    $scope.showMen=function(){

    }



    $scope.multiSearch=function(author,genre,title,start,end){
        if(author ===undefined){
            author="";
        }
        if(genre ===undefined){
            genre="";
        }

        if(start ===null ||(start ===undefined)){
            start="";
        }
        if(end ===null ||(end ===undefined)){
            end="";
        }
        $http({
            method: 'GET',
            url: '/rest/books/filter?author='+author+'&genre='+genre+'&start='+start+'&end='+end
        }).then(function successCallback(data) {
            $scope.books=data.data;
            for (var i=0;i<$scope.books.length;i++){
                $scope.getImage($scope.books[i].id,i);
            }
        }, function errorCallback(data) {
            console.log("error");
        });


    }

    $scope.getImage = function(idProduct,i) {
        $http({
            method: 'GET',
            url: '/rest/product/'+idProduct+'/image'
        }).then(function successCallback(data) {
            $scope.products[i].image=data.data;
        }, function errorCallback(data) {
            console.log("error");
        });
    };

    $scope.getAll = function() {
        $http({
            method: 'GET',
            url: '/rest/products/'
        }).then(function successCallback(data) {
            $scope.products=data.data;
            for (var i=0;i<$scope.products.length;i++){
                console.log($scope.products[i].id);
                if($scope.types.indexOf($scope.products[i].type) === -1){
                    $scope.types.push($scope.products[i].type);
                }
                if($scope.colors.indexOf($scope.products[i].color) === -1){
                    $scope.colors.push($scope.products[i].color);
                }
                if($scope.products[i].gender === "F"){
                    $scope.nrWomen++;
                }else{
                    $scope.nrMen++;
                }
            }
        }, function errorCallback(data) {
            console.log("error");
        });
    }();



    $scope.showMen = function() {

        $http({
            method: 'GET',
            url: '/rest/products/gender/M',
        }).then(function successCallback(data) {
            $scope.products=data.data;

        }, function errorCallback(data) {
            console.log("error");
        });
    }

    $scope.showWomen = function() {

        $http({
            method: 'GET',
            url: '/rest/products/gender/F',
        }).then(function successCallback(data) {
            $scope.products=data.data;

        }, function errorCallback(data) {
            console.log("error");
        });
    }

    $scope.showWomenT = function(type) {
        $http({
            method: 'GET',
            url: '/rest/products/type/'+type,
        }).then(function successCallback(data) {
            $scope.products=data.data;
        }, function errorCallback(data) {
            console.log("error");
        });
    }

    $scope.colorFilter = function(color) {
        $http({
            method: 'GET',
            url: '/rest/products/color/'+color,
        }).then(function successCallback(data) {
            $scope.products=data.data;
        }, function errorCallback(data) {
            console.log("error");
        });
    }

    $scope.showMenT = function(type) {
        $http({
            method: 'GET',
            url: '/rest/products/type/'+type,
        }).then(function successCallback(data) {
            $scope.products=data.data;

        }, function errorCallback(data) {
            console.log("error");
        });
    }



    $scope.getQuantity=function(book,value){
        if (book.quantity>value){
            $scope.amountOfInterval++;
        }else{
            $scope.amountOfInterval=0;
        }
    }


    $scope.sellBook=function(book,amount,event){
        $http({
            method: 'PUT',
            url: '/rest/books/sell?q='+amount+'&id='+book.id,
            headers: {
                'Content-Type': 'application/json'
            },
        }).then(function successCallback(data) {
            console.log("Selling succesfull");
            $scope.getBooks;
            $scope.showAlertSuccess(event);

        }, function errorCallback(data) {

            $scope.showInvalidAmount(event);
            console.log("error at account update");
        });

    }
    $scope.hideSearch=function(){
        $scope.advanced=!$scope.advanced;
    }
    $scope.clearSearch=function(){
        $scope.books=$scope.allBooks;
        for (var i=0;i<$scope.books.length;i++){
            $scope.getImage($scope.books[i].id,i);
        }
    }


}]);
































