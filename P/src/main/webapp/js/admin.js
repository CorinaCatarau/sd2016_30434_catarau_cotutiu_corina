/**
 * Created by Corina on 26.05.2016.
 */
var myApp = angular.module('app');

app.directive('toolbarAdmin', function() {
    return {
        restrict: 'E'
        ,
        replace: true,
        templateUrl: 'navbar-admin'
    };
});
app.directive('productsCrud', function() {
    return {
        restrict: 'E',
        replace: true,
        templateUrl: 'products-crud'
    };
});
app.directive('adminUsers', function() {
    return {
        restrict: 'E',
        replace: true,
        templateUrl: 'admin-users'
    };
});

app .controller('AdminCtrl',['$scope','$http','$mdDialog','$mdMedia','$window', function($scope,$http, $mdDialog, $mdMedia,$window) {
    $scope.username;
    $scope.users=[];
    $scope.userView=true;
    $scope.booksView=false;
    $scope.editIndex=-1;
    $scope.editUser=false;
    $scope.editBook=false;
    $scope.editUserIndex=-1;
    $scope.editBookIndex=-1;
    $scope.collapsedArea=-1;
    $scope.collapsedItem=-1;
    $scope.isCollapsed=false;


    $scope.notCollpased=false;
    $scope.list=["A","B","C","E"];


    $scope.showAlert = function(ev) {
        $mdDialog.show(
            $mdDialog.alert()
                .parent(angular.element(document.querySelector('#popupContainer')))
                .clickOutsideToClose(true)
                .title('Forbidden')
                .textContent('Username already used.')
                .ariaLabel('Alert Dialog Demo')
                .ok('Got it!')
                .targetEvent(ev)
        );
    };


    $scope.showAlertSuccess = function(ev) {
        $mdDialog.show(
            $mdDialog.alert()
                .parent(angular.element(document.querySelector('#popupContainer')))
                .clickOutsideToClose(true)
                .title('Success')
                .textContent('Your account has been created')
                .ariaLabel('Alert Dialog Demo')
                .ok('Got it!')
                .targetEvent(ev)
        );
    };


    $scope.showDetail = function (u) {
        if ($scope.active != u.username) {
            $scope.active = u.username;
        }
        else {
            $scope.active = null;
        }
    };


    $scope.getUsers=function(){
        $http({
            method: 'GET',
            url: '/rest/users'
        }).then(function successCallback(data) {

            $scope.users=data.data;
        }, function errorCallback(data) {
            console.log("error");
        });
    };

    $scope.generateReport=function(reportType){
        $http({
            method: 'GET',
            url: '/rest/generate/'+reportType
        }).then(function successCallback() {
            console.log("report generated");
        }, function errorCallback(data) {
            console.log("error");
        });
    };


    $scope.deleteUser=function(user,users){
        var index = users.indexOf(user);
        var deleted=user.name;
        $http({
            method: 'DELETE',
            url: '/rest/users/'+user.id
        }).then(function successCallback() {
            users.splice(index, 1);
            console.log("Delete succesful");

        }, function errorCallback() {
            console.log("error at user delete");
        });

    }

    $scope.deleteProduct=function(product,products){
        var index = products.indexOf(product);
        $http({
            method: 'DELETE',
            url: '/rest/products/'+product.id
        }).then(function successCallback() {
            products.splice(index, 1);
            console.log("Delete succesful");

        }, function errorCallback() {
            console.log("error at user delete");
        });

    }

    showNotNull = function(ev) {
        $mdDialog.show(
            $mdDialog.alert()
                .parent(angular.element(document.querySelector('#popupContainer')))
                .clickOutsideToClose(true)
                .title('Empty')
                .textContent('Required fields must not be empty.')
                .ariaLabel('Alert Dialog Demo')
                .ok('Ok')
                .targetEvent(ev)
        );
    };

    $scope.updateUser=function(user,event){

        var nUser=angular.fromJson(angular.toJson(user));
        $http({
            method: 'PUT',
            url: '/rest/users/'+user.id,
            headers: {
                'Content-Type': 'application/json'
            },
            data: nUser
        }).then(function successCallback(data) {
            if(data.status===226){
                $scope.showAlert(event);
            }else{
                console.log("Update succesfull");
                $scope.editUser=false;
            }


        }, function errorCallback(data) {
            if(data.status===400){
                showNotNull(event);
            }
            console.log("error at user update");
        });

    }


    $scope.updateProduct=function(product,event){
        var nProduct=angular.fromJson(angular.toJson(product));
        $http({
            method: 'PUT',
            url: '/rest/products/'+product.id,
            headers: {
                'Content-Type': 'application/json'
            },
            data: nProduct
        }).then(function successCallback(data) {
            if(data.status===226){
                $scope.showAlert(event);
            }else{
                console.log("Update succesfull");
                $scope.editProduct=false;
                $scope.getProducts();
            }


        }, function errorCallback(data) {
            if(data.status===400){
                showNotNull(event);
            }
            console.log("error at book update");
        });

    }


    $scope.setEditModeUser=function(index){
        $scope.editUserIndex=index;
        $scope.editUser=!$scope.editUser;
    }

    $scope.editModeUser=function(index){
        return (($scope.editUserIndex===index)&&$scope.editUser)
    }



    $scope.setEditModeBook=function(index){
        $scope.editBookIndex=index;
        $scope.editBook=!$scope.editBook;
    }

    $scope.editModeBook=function(index){
        return (($scope.editBookIndex===index)&&$scope.editBook)
    }
    $scope.isCollapsed=function(index,i){
        $scope.collapsedArea=i;
        $scope.collapsedItem=index;
        $scope.notCollpased=true;
    }
    $scope.collapsedAreaFunction=function(index,i){
        return((index===$scope.collapsedItem)&&(i===$scope.collapsedArea)&& $scope.notCollpased)
    }


    $scope.switchView=function(view){
        if(view==="admin-users"){
            $scope.booksView=false;
            $scope.userView=true;
            $scope.getUsers();
        }else if(view==="books-crud"){
            $scope.userView=false;
            $scope.booksView=true;
            $scope.getProducts();
        }

    }
    $scope.onchange = function (user) {
        $scope.selectedUser=user;
        $scope.showActivities=true;
    }
    showAlertUser = function(ev) {
        $mdDialog.show(
            $mdDialog.alert()
                .parent(angular.element(document.querySelector('#popupContainer')))
                .clickOutsideToClose(true)
                .title('Error')
                .textContent('You must first select a user.')
                .ariaLabel('Alert Dialog Demo')
                .ok('Got it!')
                .targetEvent(ev)
        );
    };

    showAlertCnp = function(ev) {
        $mdDialog.show(
            $mdDialog.alert()
                .parent(angular.element(document.querySelector('#popupContainer')))
                .clickOutsideToClose(true)
                .title('Error')
                .textContent('Duplicate PNC.')
                .ariaLabel('Alert Dialog Demo')
                .ok('Got it!')
                .targetEvent(ev)
        );
    };

    $scope.createProduct = function(newProduct,event) {
        var productNew=angular.fromJson(angular.toJson(newProduct));
        $http({
            method: 'POST',
            url: '/rest/product/',
            headers: {
                'Content-Type': 'application/json'
            },
            data: productNew
        }).then(function successCallback(data) {
            if(data.status===201){
                $scope.showAlertSuccess(event);
                $scope.books.push(data.data);
                console.log("create succesfull");
                $scope.notCollpased=true;
            }else if(data.status===226){
                $scope.showAlert(event);
            }
            $scope.isCollapsed=false;


        }, function errorCallback(data) {

            console.log("error at user create");
        });
        ;
    };


    $scope.createUser = function(newUser,event) {
        var userNew=angular.fromJson(angular.toJson(newUser));
        $http({
            method: 'POST',
            url: '/rest/user/',
            headers: {
                'Content-Type': 'application/json'
            },
            data: userNew
        }).then(function successCallback(data) {
            if(data.status===201){
                $scope.showAlertSuccess(event);
                $scope.users.push(data.data);
                console.log("create succesfull");
                $scope.isCollapsed=false;
            }else if(data.status===226){
                $scope.showAlert(event);
            }
            $scope.isCollapsed=false;


        }, function errorCallback(data) {
            if(data.status===409){
                showAlertCnp(event);
            }
            console.log("error at user create");
        });
        ;
    };

    $scope.getProducts = function() {
        $http({
            method: 'GET',
            url: '/rest/products/'
        }).then(function successCallback(data) {
            $scope.products=data.data;

        }, function errorCallback(data) {
            console.log("error");
        });
    };






}]);
