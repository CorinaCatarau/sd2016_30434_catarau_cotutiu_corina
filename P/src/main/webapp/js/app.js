/**
 * Created by Corina on 24.05.2016.
 */
/**
 * Created by Corina on 26.04.2016.
 */
var app = angular.module('app', ['ngMaterial','ngMessages']);
app.directive('toolbar', function() {
    return {
        restrict: 'E'
        ,
        replace: true,
        templateUrl: 'navbar'
    };
});



app .controller('AppCtrl',['$scope','$http','$mdDialog','$mdMedia','$window', function($scope,$http, $mdDialog, $mdMedia,$window){
    $scope.username;
    $scope.allBooks;
    $scope.advanced=false;
    $scope.amountOfInterval=0;
    $scope.types=[];
    $scope.colors=[];

    console.log("${username}");
    console.log("<%=username%>");

    /* $scope.getData = function(username) {
     $http({
     method: 'GET',
     url: '/rest/users/username/'+username
     }).then(function successCallback(data) {
     $scope.getActivities(data.data.id);
     $window.localStorage.setItem("userId",data.data.id);
     }, function errorCallback(data) {
     console.log("error");
     });
     };*/
    $scope.showAlert = function(ev) {
        $mdDialog.show(
            $mdDialog.alert()
                .parent(angular.element(document.querySelector('#popupContainer')))
                .clickOutsideToClose(true)
                .title('Forbidden')
                .textContent('Username already used.')
                .ariaLabel('Alert Dialog Demo')
                .ok('Got it!')
                .targetEvent(ev)
        );
    };


    $scope.showAlertSuccess = function(ev) {
        $mdDialog.show(
            $mdDialog.alert()
                .parent(angular.element(document.querySelector('#popupContainer')))
                .clickOutsideToClose(true)
                .title('Success')
                .textContent('Your account has been created')
                .ariaLabel('Alert Dialog Demo')
                .ok('Got it!')
                .targetEvent(ev)
        );
    };
    $scope.createUser = function(newUser,event) {
        newUser.role="USER_ROLE";
        var userNew=angular.fromJson(angular.toJson(newUser));
        $http({
            method: 'POST',
            url: '/rest/user',
            headers: {
                'Content-Type': 'application/json'
            },
            data: userNew
        }).then(function successCallback(data) {
            $scope.createCart(data.data.id);
            if(data.status===201){
                $scope.showAlertSuccess(event);
                $scope.users.push(data.data);
                console.log("create succesfull");
                $scope.isCollapsed=false;
            }else if(data.status===226){
                $scope.showAlert(event);
            }
            $scope.isCollapsed=false;


        }, function errorCallback(data) {
            if(data.status===409){
                showAlertCnp(event);
            }
            console.log("error at user create");
        });
        ;
    };


    $scope.createCart = function(userId) {
        var cart={
            "userId":userId,
            "products":[],
            "total":0
        };
        var cartNew=angular.fromJson(angular.toJson(cart));
        $http({
            method: 'POST',
            url: '/rest/cart',
            headers: {
                'Content-Type': 'application/json'
            },
            data: cartNew
        }).then(function successCallback(data) {
           console.log("cart created");


        }, function errorCallback(data) {
            console.log("error at cart create");
        });
        ;
    };






    $scope.multiSearch=function(author,genre,title,start,end){
        if(author ===undefined){
            author="";
        }
        if(genre ===undefined){
            genre="";
        }

        if(start ===null ||(start ===undefined)){
            start="";
        }
        if(end ===null ||(end ===undefined)){
            end="";
        }
        $http({
            method: 'GET',
            url: '/rest/books/filter?author='+author+'&genre='+genre+'&start='+start+'&end='+end
        }).then(function successCallback(data) {
            $scope.books=data.data;
            for (var i=0;i<$scope.books.length;i++){
                $scope.getImage($scope.books[i].id,i);
            }
        }, function errorCallback(data) {
            console.log("error");
        });


    }

    $scope.getImage = function(idProduct,i) {
        $http({
            method: 'GET',
            url: '/rest/product/'+idProduct+'/image'
        }).then(function successCallback(data) {
            $scope.products[i].image=data.data;
        }, function errorCallback(data) {
            console.log("error");
        });
    };


    $scope.byGenre = function(genre) {
        $http({
            method: 'GET',
            url: '/rest/books/genre/'+genre
        }).then(function successCallback(data) {
            $scope.books=data.data;
            for (var i=0;i<$scope.books.length;i++){
                $scope.getImage($scope.books[i].id,i);
            }
        }, function errorCallback(data) {
            console.log("error");
        });
    };

    $scope.byTitle = function(name) {
        $http({
            method: 'GET',
            url: '/rest/books/name/'+name
        }).then(function successCallback(data) {
            $scope.books=data.data;
            for (var i=0;i<$scope.books.length;i++){
                $scope.getImage($scope.books[i].id,i);
            }
        }, function errorCallback(data) {
            console.log("error");
        });
    };

    $scope.byPrice = function(start,end) {
        if(start === undefined){
            start=0;
        }
        if(end === undefined){
            end=0;
        }
        $http({
            method: 'GET',
            url: '/rest/books/price?start='+start+'&end='+end
        }).then(function successCallback(data) {
            $scope.books=data.data;
            for (var i=0;i<$scope.books.length;i++){
                $scope.getImage($scope.books[i].id,i);
            }
        }, function errorCallback(data) {
            console.log("error");
        });
    };








    $scope.getQuantity=function(book,value){
        if (book.quantity>value){
            $scope.amountOfInterval++;
        }else{
            $scope.amountOfInterval=0;
        }
    }


    $scope.sellBook=function(book,amount,event){
        $http({
            method: 'PUT',
            url: '/rest/books/sell?q='+amount+'&id='+book.id,
            headers: {
                'Content-Type': 'application/json'
            },
        }).then(function successCallback(data) {
            console.log("Selling succesfull");
            $scope.getBooks;
            $scope.showAlertSuccess(event);

        }, function errorCallback(data) {

            $scope.showInvalidAmount(event);
            console.log("error at account update");
        });

    }
    $scope.hideSearch=function(){
        $scope.advanced=!$scope.advanced;
    }
    $scope.clearSearch=function(){
        $scope.books=$scope.allBooks;
        for (var i=0;i<$scope.books.length;i++){
            $scope.getImage($scope.books[i].id,i);
        }
    }


}]);































