package com.bookstore.rest;

import com.bookstore.dao.UserDAO;
import com.bookstore.model.ListUser;
import com.bookstore.model.User;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Spy;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.http.MediaType;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.ui.ModelMap;
import java.util.ArrayList;
import java.util.List;

import static org.mockito.Mockito.*;
import static org.mockito.Mockito.verify;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

/**
 * Created by Corina on 05.05.2016.
 */
@RunWith(MockitoJUnitRunner.class)
@WebAppConfiguration
public class UserControllerTest {
    @Mock
    private UserDAO userDAOMock;
    @Mock
    private ListUser listUserMock;
    @InjectMocks
    UserController userControllerTest;
    @Spy
    List<User> users = new ArrayList<User>();
    private MockMvc mockMvc;

    @Spy
    ModelMap model;
    public List<User> getUsersList(){

        User u1=new User();
        u1.setId("1940305124567");
        u1.setName("User1");
        u1.setPassword("abcdefg1");
        u1.setRole("USER_ROLE");
        users.add(u1);

        User u2=new User();
        u1.setId("2940404123678");
        u1.setName("User2");
        u1.setPassword("abcdefg2");
        u1.setRole("USER_ROLE");
        users.add(u2);
        return users;
    }

    @Before
    public void prepare() throws Exception {
        this.mockMvc = MockMvcBuilders.standaloneSetup(userControllerTest).build();
        users=getUsersList();

    }

    @Test
    public void createUser() throws Exception {
        mockMvc.perform(post("/rest/user").contentType(MediaType.APPLICATION_JSON)
                .content("{\"id\":\"2450102345\",\"name\":\"name\",\"password\":\"pass\",\"role\":\"USER_ROLE\"}")).andExpect(status().isCreated());

    }

    @Test
    public void findClientsWithException() throws Exception {
        when(userDAOMock.getAll(anyString())).thenThrow(new RuntimeException());
        mockMvc.perform(get("/rest/users")).andExpect(status().isNotFound());
        verify(userDAOMock, times(1)).getAll(anyString());
        verifyNoMoreInteractions(userDAOMock);
    }

    @Test
    public void findClientsOk() throws Exception {
        ListUser myList=new ListUser();
        myList.setUserList(users);
        when(userDAOMock.getAll(anyString())).thenReturn(myList);
        mockMvc.perform(get("/rest/users")).andExpect(status().isOk());
    }

    @Test
    public void deleteClient() throws Exception{
        mockMvc.perform(delete("/rest/users/2").contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk());
        verify(userDAOMock).delete(anyString(),anyString());


    }


}
