package com.bookstore.model;

import org.springframework.stereotype.Component;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Corina on 24.04.2016.
 */
@Component
@XmlRootElement
public class ListBook {
    List<Book> bookList=new ArrayList<Book>();

    public List<Book> getBookList() {return bookList;}
    @XmlElementWrapper(name = "books")
    @XmlElement(name ="book")
    public void setBookList(List<Book> bookList) {this.bookList = bookList;}
}
