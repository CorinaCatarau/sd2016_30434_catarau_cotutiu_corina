package com.bookstore.model;

import com.sun.org.apache.xerces.internal.impl.dv.util.Base64;

import javax.imageio.ImageIO;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;

/**
 * Created by Corina on 24.04.2016.
 */
@XmlRootElement
public class Book {

    private String id;
    private String title;
    private String genre;
    private String author;
    private int price;
    private int quantity;
    private String imagePath;

    public String getImage() {return this.imagePath;}

    @XmlElement(name="image")
    public void setImage(String image) throws IOException {this.imagePath=image;}
    public String getId() {return id;}
    public void setId(String id) {this.id = id;}
    public String getTitle() {return title;}
    @XmlElement(name="title")
    public void setTitle(String title) {this.title = title;}

    public String getGenre() {return genre;}
    @XmlElement(name="genre")
    public void setGenre(String genre) {this.genre = genre;}

    public String getAuthor() {return author;}
    @XmlElement(name="author")
    public void setAuthor(String author) {this.author = author;}

    public int getPrice() {return price;}
    @XmlElement(name="price")
    public void setPrice(int price) {this.price = price;}

    public int getQuantity() {return quantity;}
    @XmlElement(name="quantity")
    public void setQuantity(int quantity) {this.quantity = quantity;}
}
