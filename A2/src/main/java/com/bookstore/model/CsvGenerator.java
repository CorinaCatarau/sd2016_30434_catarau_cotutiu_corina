package com.bookstore.model;

import com.itextpdf.text.DocumentException;
import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVPrinter;
import org.springframework.stereotype.Component;

import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Corina on 02.05.2016.
 */
@Component("CSV")
public class CsvGenerator implements  ReportGenerator {
    private static final String NEW_LINE_SEPARATOR = "\n";
    private static final Object [] FILE_HEADER = {"bookName","author","quantity"};
    private static String FILE = "D:/SD2016_30434_Catarau_Corina_Bookstore/src/main/resources/";


    @Override
    public void generateReport(int nr, String fileName, List<Book> books) throws FileNotFoundException, DocumentException {

        FileWriter fileWriter = null;
        CSVPrinter csvFilePrinter = null;
        CSVFormat csvFileFormat = CSVFormat.DEFAULT.withRecordSeparator(NEW_LINE_SEPARATOR);

        try {
            String myFile=FILE+fileName+".csv";
            fileWriter = new FileWriter(myFile);
            csvFilePrinter = new CSVPrinter(fileWriter, csvFileFormat);
            csvFilePrinter.printRecord(FILE_HEADER);

            for (Book book : books) {
                List studentDataRecord = new ArrayList();
                studentDataRecord.add(book.getTitle());
                studentDataRecord.add(book.getAuthor());
                studentDataRecord.add(book.getQuantity());
                csvFilePrinter.printRecord(studentDataRecord);
            }

            System.out.println("CSV file was created successfully !");

        } catch (Exception e) {
            System.out.println("Error in CsvFileWriter !");
            e.printStackTrace();
        } finally {
            try {
                fileWriter.flush();
                fileWriter.close();
                csvFilePrinter.close();
            } catch (IOException e) {
                System.out.println("Error while flushing/closing fileWriter/csvPrinter !");
                e.printStackTrace();
            }
        }
    }

}

