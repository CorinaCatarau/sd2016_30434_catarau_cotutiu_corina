package com.bookstore.model;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * Created by Corina on 28.04.2016.
 */
@XmlRootElement
public class SellReport {
    private String id;
    private String userId;
    private String bookName;
    private String quantity;

    public String getId() {return id;}
    @XmlElement(name="id")
    public void setId(String id) {this.id = id;}

    public String getUserId() {return userId;}
    @XmlElement(name="userId")
    public void setUserId(String userId) {this.userId = userId;}

    public String getBookName() {return bookName;}
    @XmlElement(name="bookName")
    public void setBookName(String bookName) {this.bookName = bookName;}

    public String getQuantity() {return quantity;}
    @XmlElement(name="quantity")
    public void setQuantity(String quantity) {this.quantity = quantity;}
}
