package com.bookstore.model;

import org.springframework.stereotype.Component;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Corina on 23.04.2016.
 */
@Component
@XmlRootElement
public class ListUser {
    List<User> userList=new ArrayList<User>();

    public List<User> getUserList() {return userList;}
    @XmlElementWrapper(name = "users")
    @XmlElement(name ="user")
    public void setUserList(List<User> userList) {this.userList = userList;}
}
