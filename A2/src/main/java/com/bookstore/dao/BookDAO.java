package com.bookstore.dao;

import com.bookstore.model.Book;
import com.bookstore.model.ListBook;
import org.springframework.stereotype.Component;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.List;

/**
 * Created by Corina on 24.04.2016.
 */
@Component
public interface BookDAO extends GenericDAO<ListBook,String>{
    public Book getBookById(String id) throws IOException;
    public List<Book> getBookByName(String name) throws IOException;
    public List<Book> getBooksByGenre(String genre) throws IOException;
    public List<Book> getBooksByPriceRange(int startPrice,int endPrice) throws IOException;
    public List<Book> getBooksByAuthor(String author) throws IOException;
    public List<Book> getBooksCheaperThan(int price) throws IOException;
    public List<Book> getBooksFrom(int price) throws IOException;
    public List<Book> intersection(List<Book> first,List<Book> second);

}
