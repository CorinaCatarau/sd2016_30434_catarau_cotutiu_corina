package com.bookstore.dao;

import com.bookstore.model.ListUser;
import com.bookstore.model.User;
import org.springframework.stereotype.Component;

import java.io.FileNotFoundException;
import java.io.IOException;

/**
 * Created by Corina on 23.04.2016.
 */
@Component
public interface UserDAO extends GenericDAO<ListUser,Integer>  {
    public User getUserById(String id) throws IOException;
    public User getUserByName(String name) throws IOException;
}
