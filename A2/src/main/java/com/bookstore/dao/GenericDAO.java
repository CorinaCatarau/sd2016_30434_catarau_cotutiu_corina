package com.bookstore.dao;

import org.springframework.stereotype.Component;

import javax.xml.bind.JAXBException;
import java.io.FileNotFoundException;
import java.io.IOException;

/**
 * Created by Corina on 24.04.2016.
 */
@Component
public interface GenericDAO<T,K> {
    public void create(T object,String xmlFile) throws IOException, JAXBException;
    public void delete(String id,String xmlFile) throws IOException;
    public T update(T object) throws FileNotFoundException;
    public T getAll(String xmlFile) throws IOException, JAXBException;
}
