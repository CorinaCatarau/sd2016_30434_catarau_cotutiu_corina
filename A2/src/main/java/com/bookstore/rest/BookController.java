package com.bookstore.rest;

/**
 * Created by Corina on 24.04.2016.
 */

import com.bookstore.dao.BookDAO;
import com.bookstore.model.Book;
import com.bookstore.model.ListBook;
import com.sun.org.apache.xerces.internal.impl.dv.util.Base64;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.oxm.UnmarshallingFailureException;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;

import javax.imageio.ImageIO;
import javax.xml.bind.JAXBException;
import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.rmi.UnmarshalException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.UUID;

@Controller
@EnableWebMvc
@RequestMapping("/rest")
public class BookController {
    @Autowired
    private BookDAO bookDAO;
    @Autowired
    private ListBook listBook;
    private static final String BOOK_XML="D:\\SD2016_30434_Catarau_Corina_Bookstore\\src\\books.xml";




    @RequestMapping(value = "/book", method = RequestMethod.POST,
            consumes ="application/JSON",
            produces = "application/JSON")
    @ResponseBody
    public ResponseEntity<Book> createBook(@RequestBody Book book) {
        UUID id=UUID.randomUUID();
        book.setId(id.toString());
        try {
            ListBook myBooks=bookDAO.getAll(BOOK_XML);
            myBooks.getBookList().add(book);
            bookDAO.create(myBooks,BOOK_XML);

        }catch (UnmarshallingFailureException ex){
            listBook.getBookList().add(book);
            try {
                bookDAO.create(listBook,BOOK_XML);
            } catch (Exception e) {
                return new ResponseEntity<Book>(HttpStatus.NOT_FOUND);
            }
        }catch (NullPointerException exp){
            listBook.getBookList().add(book);
            try {
                bookDAO.create(listBook,BOOK_XML);
            } catch (Exception e) {
                return new ResponseEntity<Book>(HttpStatus.NOT_FOUND);
            }
        }

        catch (Exception e) {
            return new ResponseEntity<Book>(HttpStatus.NOT_FOUND);
        }
        HttpHeaders header = new HttpHeaders();
        header.add("Location", "/rest/books/" + String.valueOf(book.getId()));
        return new ResponseEntity<Book>(book, header, HttpStatus.CREATED);
    }

    @RequestMapping(value = "/books/{idBook}", method = RequestMethod.PUT)
    @ResponseBody
    public ResponseEntity<Book> updateBook(@PathVariable("idBook") String idBook, @RequestBody  Book book) {
        try {
            bookDAO.delete(idBook,BOOK_XML);
            ListBook updated=bookDAO.getAll(BOOK_XML);
            updated.getBookList().add(book);
            bookDAO.create(updated,BOOK_XML);

        } catch (Exception e) {
            return new ResponseEntity<Book>(HttpStatus.BAD_REQUEST);
        }
        return new ResponseEntity<Book>(book, HttpStatus.OK);
    }

    @RequestMapping(value = "/books", method = RequestMethod.GET,produces = "application/JSON")
    @ResponseBody
    public  ResponseEntity<List<Book>> getAll(Model model) {
        List<Book> finalBooks;
        try {
            ListBook users=this.bookDAO.getAll(BOOK_XML);
            finalBooks=users.getBookList();

        } catch (Exception e) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity<>(finalBooks, HttpStatus.OK);
    }


    @RequestMapping(value = "/books/authors", method = RequestMethod.GET,produces = "application/JSON")
    @ResponseBody
    public  ResponseEntity<List<String >> getAuthors (Model model) {
        List<Book> finalBooks;
        List<String> authors=new ArrayList<String>();
        try {
            ListBook users=this.bookDAO.getAll(BOOK_XML);
            finalBooks=users.getBookList();
            for(int i=0;i<finalBooks.size();i++){
                if(authors.contains(finalBooks.get(i).getAuthor())==false) {
                    authors.add(finalBooks.get(i).getAuthor());
                }
            }

        } catch (Exception e) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity<>(authors, HttpStatus.OK);
    }

    @RequestMapping(value = "/books/genres", method = RequestMethod.GET,produces = "application/JSON")
    @ResponseBody
    public  ResponseEntity<List<String >> getGenres (Model model) {
        List<Book> finalBooks;
        List<String> genres=new ArrayList<String>();
        try {
            ListBook users=this.bookDAO.getAll(BOOK_XML);
            finalBooks=users.getBookList();
            for(int i=0;i<finalBooks.size();i++){
                if(genres.contains(finalBooks.get(i).getGenre())==false) {
                    genres.add(finalBooks.get(i).getGenre());
                }
            }

        } catch (Exception e) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity<>(genres, HttpStatus.OK);
    }

    @RequestMapping(value = "/books/titles", method = RequestMethod.GET,produces = "application/JSON")
    @ResponseBody
    public  ResponseEntity<List<String >> getTitles (Model model) {
        List<Book> finalBooks;
        List<String> titles=new ArrayList<String>();
        try {
            ListBook users=this.bookDAO.getAll(BOOK_XML);
            finalBooks=users.getBookList();
            for(int i=0;i<finalBooks.size();i++){
                if(titles.contains(finalBooks.get(i).getGenre())==false) {
                    titles.add(finalBooks.get(i).getTitle());
                }
            }

        } catch (Exception e) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity<>(titles, HttpStatus.OK);
    }
    @RequestMapping(value = "/books/{idBook}",method = RequestMethod.GET,produces = "application/JSON")
    @ResponseBody
    public ResponseEntity<Book> getBookById(@PathVariable String idBook ){
        try {
            Book book = bookDAO.getBookById(idBook);
            return new ResponseEntity<>(book, HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }


    @RequestMapping(value = "/book/{id}/image", method = RequestMethod.GET,produces = MediaType.IMAGE_JPEG_VALUE)
    @ResponseBody
    public ResponseEntity<String> getBookImage(@PathVariable("id") String id) {
        try{
            Book myBook = bookDAO.getBookById(id);
            BufferedImage img = ImageIO.read(new File(myBook.getImage()));
            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            ImageIO.write(img, "png", baos);
            baos.flush();
            byte[] bytes = baos.toByteArray();
            String encodedImage = Base64.encode(baos.toByteArray());
                HttpHeaders headers = new HttpHeaders();
                return new ResponseEntity<String>(encodedImage, headers, HttpStatus.OK);

        } catch (Exception e) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }



    @RequestMapping(value = "/books/name/{nameBook}",method = RequestMethod.GET,produces = "application/JSON")
    @ResponseBody
    public ResponseEntity<List<Book>> getBookByName(@PathVariable String nameBook ){
        try {
            List<Book> books = bookDAO.getBookByName(nameBook);
            return new ResponseEntity<>(books, HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    @RequestMapping(value = "/books/{idBook}",method = RequestMethod.DELETE)
    @ResponseBody
    public ResponseEntity<Book> deleteBook(@PathVariable String idBook){
        try {
            bookDAO.delete(idBook,BOOK_XML);
            return new ResponseEntity<>( HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    @RequestMapping(value = "/books/genre/{bookGenre}",method = RequestMethod.GET,produces = "application/JSON")
    @ResponseBody
    public ResponseEntity<List<Book>> getBookByGenre(@PathVariable String bookGenre ){
        try {
            List<Book> books = bookDAO.getBooksByGenre(bookGenre);
            return new ResponseEntity<>(books, HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    @RequestMapping(value = "/books/author/{bookAuthor}",method = RequestMethod.GET,produces = "application/JSON")
    @ResponseBody
    public ResponseEntity<List<Book>> getBookByAuthor(@PathVariable String bookAuthor ){
        try {
            List<Book> books = bookDAO.getBooksByAuthor(bookAuthor);
            return new ResponseEntity<>(books, HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    @RequestMapping(value = "/books/price",method = RequestMethod.GET,produces = "application/JSON")
    @ResponseBody
    public ResponseEntity<List<Book>> getBookByPrice(@RequestParam Map<String,String> requestParams ){
        try {
            int startPrice=Integer.parseInt( requestParams.get("start"));
            int endPrice=Integer.parseInt(requestParams.get("end"));
            List<Book> books = bookDAO.getBooksByPriceRange(startPrice,endPrice);
            return new ResponseEntity<>(books, HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }


    @RequestMapping(value = "/books/sell", method = RequestMethod.PUT)
    @ResponseBody
    public ResponseEntity<Book> sellBook(@RequestParam Map<String,String> requestParams) {
        try {
            int quantity=Integer.parseInt( requestParams.get("q"));
            String id=requestParams.get("id");
            Book soldBook=bookDAO.getBookById(id);
            if(quantity<=soldBook.getQuantity()) {
                soldBook.setQuantity(soldBook.getQuantity() - quantity);
                bookDAO.delete(id, BOOK_XML);
                ListBook updated = bookDAO.getAll(BOOK_XML);
                updated.getBookList().add(soldBook);
                bookDAO.create(updated, BOOK_XML);
            }else{
                return new ResponseEntity<Book>(HttpStatus.FORBIDDEN);

            }
        } catch (Exception e) {
            return new ResponseEntity<Book>(HttpStatus.BAD_REQUEST);
        }
        return new ResponseEntity<Book>(HttpStatus.OK);
    }

    @RequestMapping(value = "/books/filter",method = RequestMethod.GET,produces = "application/JSON")
    @ResponseBody
    public ResponseEntity<List<Book>> getBookByMultiFilter(@RequestParam Map<String,String> requestParams ){
        List<Book> result=new ArrayList<Book>();
        List<Book> partialRes=new ArrayList<Book>();
        List<Book> genreRes=new ArrayList<Book>();
        List<Book> authorRes=new ArrayList<Book>();
        List<Book> priceRes=new ArrayList<Book>();
               String genre = requestParams.get("genre");
        String author = requestParams.get("author");
        try {
            if(!requestParams.get("start").equals("")){
                if(!requestParams.get("end").equals("")){
                    int startPrice = Integer.parseInt(requestParams.get("start"));
                    int endPrice = Integer.parseInt(requestParams.get("end"));
                    priceRes=bookDAO.getBooksByPriceRange(startPrice,endPrice);
                }
                int startPrice = Integer.parseInt(requestParams.get("start"));

                priceRes=bookDAO.getBooksFrom(startPrice);
            }else if(!requestParams.get("end").equals("")){
                int endPrice = Integer.parseInt(requestParams.get("end"));

                priceRes=bookDAO.getBooksCheaperThan(endPrice);
            }
            genreRes=bookDAO.getBooksByGenre(genre);
            authorRes=bookDAO.getBooksByAuthor(author);
            if(genreRes.size()>0){
                if(authorRes.size()>0){
                    if(priceRes.size()>0){
                        partialRes=bookDAO.intersection(genreRes,authorRes);
                        result=bookDAO.intersection(partialRes,priceRes);
                        return  new ResponseEntity<List<Book>>(result,HttpStatus.OK);
                    }
                    partialRes=genreRes;
                    partialRes=bookDAO.intersection(genreRes,authorRes);
                    return  new ResponseEntity<List<Book>>(partialRes,HttpStatus.OK);
                }else  if(priceRes.size()>0){
                    partialRes=bookDAO.intersection(genreRes,priceRes);
                    return  new ResponseEntity<List<Book>>(partialRes,HttpStatus.OK);
                }else{
                    return  new ResponseEntity<List<Book>>(genreRes,HttpStatus.OK);

                }

            }else  if(authorRes.size()>0){
                     if(priceRes.size()>0){
                         result=bookDAO.intersection(authorRes,priceRes);
                        return  new ResponseEntity<List<Book>>(result,HttpStatus.OK);
                }
                return  new ResponseEntity<List<Book>>(authorRes,HttpStatus.OK);
            }else if(priceRes.size()>0){
                return  new ResponseEntity<List<Book>>(priceRes,HttpStatus.OK);
            }
            return  new ResponseEntity<List<Book>>(HttpStatus.NOT_FOUND);




        } catch (Exception e) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }



}
