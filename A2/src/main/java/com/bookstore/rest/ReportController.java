package com.bookstore.rest;

import com.bookstore.dao.BookDAO;
import com.bookstore.model.Book;
import com.bookstore.model.ListBook;
import com.bookstore.model.ReportGenerator;
import com.bookstore.model.ReportGeneratorFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;
import java.util.UUID;

/**
 * Created by Corina on 02.05.2016.
 */
@Controller
@EnableWebMvc
@RequestMapping("/rest")
public class ReportController {
    @Autowired
    private BookDAO bookDAO;
    @Autowired
    private ListBook listBook;
    private static final String BOOK_XML="D:\\SD2016_30434_Catarau_Corina_Bookstore\\src\\books.xml";

    @RequestMapping(value = "/generate/{type}", method = RequestMethod.GET)
    ResponseEntity<ReportGenerator> generatePdf(@PathVariable("type") String type) throws Exception {
        UUID id=UUID.randomUUID();
        List<Book> finalBooks;
        ListBook users=this.bookDAO.getAll(BOOK_XML);
        finalBooks=users.getBookList();

        ReportGenerator rep=ReportGeneratorFactory.getReport(type);
        rep.generateReport(finalBooks.size(),id.toString(),finalBooks);



        return new ResponseEntity<ReportGenerator>(HttpStatus.CREATED);
    }

}
