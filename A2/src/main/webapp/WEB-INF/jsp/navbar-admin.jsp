<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<md-toolbar ng-app="app">
    <div class="md-toolbar-tools myNav" >
        <md-button ng-click="switchView('admin-activities')">
            Hello
        </md-button>
        <md-button aria-label="comment" class="md-icon-button" ng-click="switchView('admin-users')">
            <i class="material-icons" ng-click="switchView('admin-users')">face</i>
        </md-button>
        <md-button aria-label="label" class="md-icon-button" ng-click="switchView('books-crud')">
            <i class="material-icons" ng-click="switchView('books-crud')">playlist_add_check</i>
        </md-button>
        <a href="<c:url value="/logout" />">Logout</a>


    </div>
</md-toolbar>
