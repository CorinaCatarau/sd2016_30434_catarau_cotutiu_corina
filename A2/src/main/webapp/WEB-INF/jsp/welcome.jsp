<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html ng-app="app">
<head>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- Angular Material style sheet -->
    <link rel="stylesheet" href="http://ajax.googleapis.com/ajax/libs/angular_material/1.0.0/angular-material.min.css">
    <!-- load bootstrap and fontawesome via CDN -->
    <link rel="stylesheet" href="//netdna.bootstrapcdn.com/bootstrap/3.0.0/css/bootstrap.min.css" />
    <link rel="stylesheet" href="//netdna.bootstrapcdn.com/font-awesome/4.0.0/css/font-awesome.css" />
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <link rel="stylesheet" href="css/custom.css"/>


    <!-- SPELLS -->
    <!-- load angular and angular route via CDN -->
    <script src="http://ajax.googleapis.com/ajax/libs/angularjs/1.4.8/angular.min.js"></script>
    <script src="http://ajax.googleapis.com/ajax/libs/angularjs/1.4.8/angular-animate.min.js"></script>
    <script src="http://ajax.googleapis.com/ajax/libs/angularjs/1.4.8/angular-aria.min.js"></script>
    <script src="http://ajax.googleapis.com/ajax/libs/angularjs/1.4.8/angular-messages.min.js"></script>


    <!-- Angular Material Library -->
    <script src="http://ajax.googleapis.com/ajax/libs/angular_material/1.0.0/angular-material.min.js"></script>
    <script type="text/javascript" src="js/app.js">var myVar = "${username}";</script>


</head>

<!-- HEADER AND NAVBAR -->
<body  ng-controller="AppCtrl">
<toolbar ></toolbar>


<!-- MAIN CONTENT AND INJECTED VIEWS -->
<div id="main"  >
    <%--<activities ng-if="activitiesView"></activities>
    <clients ng-if="clientsView"></clients>
    <transfers ng-if="transferView"></transfers>
    <bills ng-if="billsView"></bills>--%>

        <div class="container">
            <div class="row">
                <button class="btn btn-primary" type="button" ng-click="hideSearch()">
                    Advanced Search
                </button>
                <button class="btn btn-primary" type="button" ng-click="clearSearch()">
                    Reset Search
                </button>
                <md-input-container class="col-md-3 md-block" flex-gt-sm="">
                    <label>Filter by</label>
                    <md-select ng-model="selectedFilter">
                        <md-option >Author</md-option>
                        <md-option >Genre</md-option>
                        <md-option >Price</md-option>
                    </md-select>
                </md-input-container>
            </div>

            <div ng-switch="selectedFilter">
                <div ng-switch-when="Author">
                    <md-input-container class=" md-block" flex-gt-sm="">
                        <label>Author</label>
                        <md-select ng-model="selectedAuthor">
                            <md-option ng-repeat="author in authors" value="{{author}}">
                                {{author}}
                            </md-option>
                        </md-select>
                    </md-input-container>
                    <button class="btn btn-primary" ng-click="byAuthor(selectedAuthor)">Go</button>
                </div>

                <div ng-switch-when="Genre">
                    <md-input-container class=" md-block" flex-gt-sm="">
                        <label>Genre</label>
                        <md-select ng-model="selectedGenre">
                            <md-option ng-repeat="genre in genres" value="{{genre}}">
                                {{genre}}
                            </md-option>
                        </md-select>
                    </md-input-container>
                    <button class="btn btn-primary" ng-click="byGenre(selectedGenre)">Go</button>
                </div>
                <div ng-switch-when="Price">
                    <md-input-container class=" md-icon-float md-icon-right md-block">
                        <label>Start Price</label>
                        <input ng-model="startPrice" type="number" step="1">
                    </md-input-container>
                    <md-input-container class=" md-icon-float md-icon-right md-block">
                        <label>End Price</label>
                        <input ng-model="endPrice" type="number" step="1">
                    </md-input-container>
                    <button class="btn btn-primary" ng-click="byPrice(startPrice,endPrice)">Go</button>
                </div>
            <div id="demo" ng-if=advanced>
            <div class="row">
                    <md-input-container class="col-md-3 md-block" flex-gt-sm="">
                        <label>Author</label>
                        <md-select ng-model="selectedAuthor">
                            <md-option ng-repeat="author in authors" value="{{author}}">
                                {{author}}
                            </md-option>
                        </md-select>
                    </md-input-container>
                <md-input-container class="col-md-3 md-block" flex-gt-sm="">
                    <label>Genre</label>
                    <md-select ng-model="selectedGenre">
                        <md-option ng-repeat="genre in genres" value="{{genre}}">
                            {{genre}}
                        </md-option>
                    </md-select>
                </md-input-container>
                </div>
            <div class="row">
                <md-input-container class="md-block" flex-gt-sm="">
                    <label>Title</label>
                    <md-select ng-model="selectedTitle">
                        <md-option ng-repeat="title in titles" value="{{title}}">
                            {{title}}
                        </md-option>
                    </md-select>
                </md-input-container>
                </div>
                <div class="row">
                    <md-input-container class=" md-icon-float md-icon-right md-block">
                        <label>Start Price</label>
                        <input ng-model="startPrice" type="number" step="1">
                    </md-input-container>
                    <md-input-container class=" md-icon-float md-icon-right md-block">
                        <label>End Price</label>
                        <input ng-model="endPrice" type="number" step="1">
                    </md-input-container>
                <button class="btn-default" ng-click="multiSearch(selectedAuthor,selectedGenre,selectedTitle,startPrice,endPrice)">Search</button>
                </div>
                </div>
            <div class="row">
                <div  ng-repeat="book in books" class="col-xs-4 col-md-2" class="media">
                    <div class="media-left">
                        <a href="#">
                            <a href=""> <img data-ng-src="data:image/JPEG;base64,{{book.image}}" class="img-responsive book"></a>
                        </a>
                    </div>
                    <div class="media-body">
                        <ul class="list-group">
                            <li class="list-group-item">Title:{{book.title}}</li>
                            <li class="list-group-item">Genre:{{book.genre}}</li>
                            <li class="list-group-item">Price:{{book.price}}</li>
                            <li class="list-group-item">
                                <md-input-container class="md-icon-float md-icon-right md-block">
                                    <label>Quantity</label>
                                    <input max="{{book.quantity}}" min="0" type="number" ng-model="amountOfInterval" >
                                </md-input-container>

                            </li>
                            <li class="list-group-item">
                                <button class="btn btn-primary" ng-click="sellBook(book,amountOfInterval,$event)">SELL</button>
                            </li>
                        </ul>
                    </div>
                </div>


            </div>



            </div>
        </div>

</div>

</body>
</html>