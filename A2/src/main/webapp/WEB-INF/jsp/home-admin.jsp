<%--
  Created by IntelliJ IDEA.
  User: Corina
  Date: 28.04.2016
  Time: 14:17
  To change this template use File | Settings | File Templates.
--%>
<html ng-app="app">
<head>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- Angular Material style sheet -->
    <link rel="stylesheet" href="http://ajax.googleapis.com/ajax/libs/angular_material/1.0.0/angular-material.min.css">
    <!-- load bootstrap and fontawesome via CDN -->
    <link rel="stylesheet" href="//netdna.bootstrapcdn.com/bootstrap/3.0.0/css/bootstrap.min.css" />
    <link rel="stylesheet" href="//netdna.bootstrapcdn.com/font-awesome/4.0.0/css/font-awesome.css" />
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <link rel="stylesheet" href="../css/custom.css"/>


    <!-- SPELLS -->
    <!-- load angular and angular route via CDN -->
    <script src="http://ajax.googleapis.com/ajax/libs/angularjs/1.4.8/angular.min.js"></script>
    <script src="http://ajax.googleapis.com/ajax/libs/angularjs/1.4.8/angular-animate.min.js"></script>
    <script src="http://ajax.googleapis.com/ajax/libs/angularjs/1.4.8/angular-aria.min.js"></script>
    <script src="http://ajax.googleapis.com/ajax/libs/angularjs/1.4.8/angular-messages.min.js"></script>


    <!-- Angular Material Library -->
    <script src="http://ajax.googleapis.com/ajax/libs/angular_material/1.0.0/angular-material.min.js"></script>
    <script type="text/javascript" src="../js/app.js">var myVar = "${username}";</script>
    <script type="text/javascript" src="../js/admin.js"></script>



</head>

<!-- HEADER AND NAVBAR -->
<body  ng-controller="AdminCtrl" ng-init="getBooks()">
<toolbar-admin ></toolbar-admin>
<books-crud ng-if="booksView"></books-crud>


<!-- MAIN CONTENT AND INJECTED VIEWS -->
<div id="main" ng-if="userView" ng-init="getUsers()">
    <md-button class="lavender" ng-model="collapsed" ng-click="collapsed=!collapsed">ADD USER</md-button>
    <div ng-show="collapsed">
        <form name="userFormCreate" ng-submit="createUser(newUser,$event)" class="myForm">
            <md-input-container class="md-block">
                <label>PNC</label>
                <input  required="" md-no-asterisk="" name="newUserPNC" ng-model="newUser.id" ng-pattern="/^[1-2][0-9][0-9][0-1][0-9][0-3][0-9]{7}$/">
                <div ng-messages="userFormCreate.newUserPNC.$error">
                    <div ng-message="required">This is required.</div>
                    <div ng-message="pattern">Invalid PNC format</div>

                </div>
            </md-input-container>

            <md-input-container class="md-block">
                <label>Username</label>
                <input  required="" md-no-asterisk="" name="newUserName" ng-model="newUser.name">
                <div ng-messages="userFormCreate.newUserName.$error">
                    <div ng-message="required">This is required.</div>
                </div>
            </md-input-container>

            <md-input-container class="md-block">
                <label>Password</label>
                <input required="" name="newUserPassword" ng-model="newUser.password" ng-pattern="/^(?=.*[A-Za-z])(?=.*\d)[A-Za-z\d]{6,}$/">
                <div ng-messages="userFormCreate.newUserPassword.$error"  ng-show="userFormCreate.newUserPassword.$dirty">
                    <div ng-message="required">This is required.</div>
                    <div ng-message="pattern">The password must contain at least 6 characters ,1 numeric and 1 letter</div>
                </div>
            </md-input-container>

            <md-input-container>
                <label>Type</label>
                <md-select placeholder="User type" ng-model="newUser.role" >
                    <md-option  >USER_ROLE</md-option>
                    <md-option  >ADMIN_ROLE</md-option>
                </md-select>
            </md-input-container>
            <md-button type="submit" id="submit" class="lavender" ng-disabled="userFormCreate.$invalid">Submit</md-button>

        </form>
    </div>
    <div class="container" ng-repeat="user in users"  >
        <div class="row"  ng-class-odd="'alt'">
            <div class="col-xs-1 text-center"><img  src="/img/img.png" class="img-thumbnail img-responsive img-circle"></div>
            <div class="col-xs-3 cap">{{user.name}}</div>
        </div>
        <div class="row"  ng-class-odd="'alt'">

            <div class="col-md-3 col-xs-6" ng-if="!editModeUser($index)">
                <label>PNC</label>{{user.id}}<br>
                <label>Name</label> {{user.name}}<br>
                <label>Password</label> {{user.password}}<br>
                <label>Role</label> {{user.role}}
                <md-button >
                    <i class="material-icons"  class="md-secondary md-hue-3" ng-click="setEditModeUser($index)">mode_edit</i>
                </md-button>
                <md-button >
                    <i class="material-icons"  class="md-secondary" ng-click="deleteUser(user,users)">delete_forever</i>
                </md-button>
            </div>

            <div class="col-md-3 col-xs-6"  ng-if="editModeUser($index)">
                <md-input-container class="md-block">
                    <label>Username</label>
                    <input  required="" md-no-asterisk="" name="username" ng-model="user.name">
                    <div ng-messages="username.$error">
                        <div ng-message="required">This is required.</div>
                    </div>
                </md-input-container>
                <md-input-container class="md-block">
                    <label>Password</label>
                    <input  required="" md-no-asterisk="" name="password" ng-model="user.password" >
                    <div ng-messages="password.$error">
                        <div ng-message="required">This is required.</div>
                    </div>
                </md-input-container>
                <md-input-container>
                    <label>Type</label>
                    <md-select placeholder="user.role" ng-model="user.role" >
                        <md-option  >USER_ROLE</md-option>
                        <md-option  >ADMIN_ROLE</md-option>
                    </md-select>
                </md-input-container>

                <md-button >
                    <i class="material-icons" ng-click="updateUser(user,$event)"  >arrow_forward</i>
                </md-button>
            </div>




            <div class="col-xs-12"><hr></div>

        </div>

    </div>



</div>

</body>
</html>